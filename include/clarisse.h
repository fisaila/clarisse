#ifndef CLARISSE_H
#define CLARISSE_H

#include "mpi.h"

#define CLARISSE_MAX_SERVER_PATH 256
#define CLARISSE_MAX_PARAM_SIZE 16
#define CLARISSE_SERVER_PARAMS_CNT 12 // Includes the NULL as the last param 
#define CLARISSE_MAX_VIEW_SIZE 10240 // Size of the binary representation of view
#define CLARISSE_MAX_TARGET_NAME_SIZE 256 // Max size of a target (file) name

#define CLARISSE_MAX_NR_ACTIVE_CLIENTS 16 // Max number of concurrent active clients for CLARISSE_DYN_PROCESS

typedef struct clarisse_params{
  //char server_path[CLARISSE_MAX_SERVER_PATH]; // if coupleness == CLARISSE_DYN_PROCESS 
  char port_file[CLARISSE_MAX_SERVER_PATH]; // if coupleness == CLARISSE_DYN_PROCESS   
  int buffer_size; // global
  int bufpool_size; // global
  int max_bufpool_size; //global
  int max_dcache_size; // per open file : needs to be max allocated buffers per file at each server (should be <= bufpool_size)
  int net_buf_size; // per open file
  int net_buf_data_size; // size of the data portion only
  int net_datatype_count;
  int net_datatype_incr;
  unsigned int metadata_transfer_size;
  int nr_servers;
  short int io_sched_policy; // per open file
  short int lazy_eager_view; // per open file
  short int prefetch; // per open file
  short int write_type;  // per open file
  short int coupleness; 
  short int server_map; // Various mapping of servers on real nodes
  int aggregator_selection_step; // BGQ architecture: step for selecting the aggregators on each node
  int bg_nodes_pset; // BGQ architecture: number of aggreg per virtual
  short int file_partitioning; // CLARISSE_STATIC_FILE_PARTITIONING and CLARISSE_DYNAMIC_FILE_PARTITIONING
  short int collective; // CLARISSE_ROMIO_COLLECTIVE, CLARISSE_VB_COLLECTIVE, CLARISSE_LISTIO_COLLECTIVE
  short int aggr_thread; // CLARISSE_AGGR_THREAD and CLARISSE_AGGR_PROCESS
  short int control; //CLARISSE_CONTROL_ENABLED CLARISSE_CONTROL_DISABLED
  short int global_scheduling; //CLARISSE_GLOBAL_SCHEDULING_NULL CLARISSE_GLOBAL_SCHEDULING_FCFS
  short int elasticity; //CLARISSE_ELASTICITY_DISABLED CLARISSE_ELASTICITY_ENABLED 
  short int elasticity_policy; 
  short int read_future; //CLARISSE_READ_FUTURE_ENABLED CLARISSE_READ_FUTURE_DISABLED 
  short int external_buffering; //CLARISSE_EXTERNAL_BUFFERING_ENABLED CLARISSE_EXTERNAL_BUFFERING_DISABLED 
  short int external_monitoring;//CLARISSE_EXTERNAL_MONITORING_ENABLED CLARISSE_EXTERNAL_MONITORING_DISABLED
} clarisse_params_t;

#define CLARISSE_NO_PREFETCH     0
#define CLARISSE_PREFETCH        1
#define CLARISSE_WRITE_ON_CLOSE  0
#define CLARISSE_WRITE_BACK      1
#define CLARISSE_WRITE_ON_DEMAND 2
#define CLARISSE_EAGER_VIEW      0
#define CLARISSE_LAZY_VIEW       1
#define CLARISSE_COUPLED         0  // Compute nodes and aggregtators same process
#define CLARISSE_DYN_PROCESS     1  // Launch aggregators as separate processes
#define CLARISSE_INTERCOMM       2  // Aggregators processes of the same app but different ranks than the producer/consumers

#define CLARISSE_SERVER_MEMBERSHIP_KEY  0 
#define CLARISSE_CLIENT_MEMBERSHIP_KEY  1
enum {
  CLARISSE_SERVER_MAP_ROMIO=0,
  CLARISSE_SERVER_MAP_UNIFORM, //DEFAULT
  CLARISSE_SERVER_MAP_TOPOLOGY_EXCLUDE_BRIDGE,
  CLARISSE_SERVER_MAP_TOPOLOGY_INCLUDE_BRIDGE,
  CLARISSE_SERVER_MAP_RANDOM,
  CLARISSE_SERVER_MAP_CUSTOM,
  CLARISSE_SERVER_MAP_COUNT
};
#define CLARISSE_STATIC_FILE_PARTITIONING 0 //DEFAULT
#define CLARISSE_DYNAMIC_FILE_PARTITIONING 1
#define CLARISSE_WORKFLOW_FILE_PARTITIONING 2


#define CLARISSE_VB_COLLECTIVE 0 
#define CLARISSE_ROMIO_COLLECTIVE 1
#define CLARISSE_LISTIO_COLLECTIVE 2
#define CLARISSE_AGGR_PROCESS 0
#define CLARISSE_AGGR_THREAD 1
#define CLARISSE_CONTROL_DISABLED 0
#define CLARISSE_CONTROL_ENABLED 1
#define CLARISSE_GLOBAL_SCHEDULING_NULL 0 
#define CLARISSE_GLOBAL_SCHEDULING_FCFS 1
#define CLARISSE_ELASTICITY_DISABLED 0
#define CLARISSE_ELASTICITY_ENABLED 1
#define CLARISSE_READ_FUTURE_DISABLED 0
#define CLARISSE_READ_FUTURE_ENABLED 1
#define CLARISSE_EXTERNAL_BUFFERING_DISABLED 0 
#define CLARISSE_EXTERNAL_BUFFERING_ENABLED 1
#define CLARISSE_EXTERNAL_MONITORING_DISABLED 0 
#define CLARISSE_EXTERNAL_MONITORING_ENABLED 1


#define CLARISSE_DEFAULT_COLLECTIVE CLARISSE_LISTIO_COLLECTIVE
#define CLARISSE_DEFAULT_BUFFER_SIZE  (16*1024*1024)
#define CLARISSE_DEFAULT_BUFPOOL_SIZE 2
#define CLARISSE_DEFAULT_MAX_BUFPOOL_SIZE 64
#define CLARISSE_DEFAULT_MAX_DCACHE_SIZE 2 //MAX_DCACHE_SIZE in hash.h
#define CLARISSE_DEFAULT_NET_BUF_DATA_SIZE (16*1024*1024)
//Number of elements in the network datatype count
#define CLARISSE_DEFAULT_NET_DATATYPE_COUNT  256
//Increment of network datatype count
#define CLARISSE_DEFAULT_NET_DATATYPE_INCR 256
#define CLARISSE_DEFAULT_METADATA_TRANSFER_SIZE 128
#define CLARISSE_DEFAULT_NR_SERVERS 1
#define CLARISSE_DEFAULT_IO_SCHED_POLICY  HASH_POLICY 
#define CLARISSE_DEFAULT_LAZY_EAGER_VIEW  CLARISSE_LAZY_VIEW
#define CLARISSE_DEFAULT_WRITE_TYPE CLARISSE_WRITE_ON_DEMAND
#define CLARISSE_DEFAULT_PREFETCH CLARISSE_NO_PREFETCH
#define CLARISSE_DEFAULT_SERVER_MAP CLARISSE_SERVER_MAP_UNIFORM
#define CLARISSE_DEFAULT_FILE_PARTITIONING CLARISSE_STATIC_FILE_PARTITIONING 
#define CLARISSE_DEFAULT_SERVER_WORKER_THREADS 1
#define CLARISSE_DEFAULT_SERVER_TASK_QUEUE_SIZE (64*1024)
#define CLARISSE_DEFAULT_AGGR_THREAD CLARISSE_AGGR_PROCESS
#define CLARISSE_DEFAULT_CONTROL CLARISSE_CONTROL_DISABLED
#define CLARISSE_DEFAULT_GLOBAL_SCHEDULING CLARISSE_GLOBAL_SCHEDULING_NULL
#define CLARISSE_DEFAULT_ELASTICITY CLARISSE_ELASTICITY_DISABLED
#define CLARISSE_DEFAULT_READ_FUTURE CLARISSE_READ_FUTURE_DISABLED
#define CLARISSE_DEFAULT_EXTERNAL_BUFFERING CLARISSE_EXTERNAL_BUFFERING_DISABLED
#define CLARISSE_DEFAULT_EXTERNAL_MONITORING CLARISSE_EXTERNAL_MONITORING_DISABLED



#define CLS_SERVER_INACTIVE -1
int cls_ph_server_down(int ph_ios_rank);
int cls_ph_server_up(int ph_ios_rank);
int cls_log_server_down(int ph_ios_rank);
int cls_log_server_up(int ph_ios_rank);
int cls_active_server_down(int active_log_ios_rank);
int cls_active_server_down_publish(int active_log_ios_rank);
int cls_ph_server_add(int ph_ios_rank);
int cls_ph_server_remove(int ph_ios_rank);

void cls_set_load_injection_coll_seq(int coll_seq, int active_log_ios_rank, double load);
void cls_set_load_detection_coll_seq(int coll_seq);
void cls_active_server_slow(int active_log_ios_rank, double slow);

void clarisse_param_print(clarisse_params_t *clarisse_pars);

typedef long long int clarisse_off_t;

int clarisse_hash(unsigned char *str);
int cls_assign_servers(int map_type, int nr_servers, int *server_map, int *client_leader, int *server_leader);
int cls_reassign_servers(int map_type, int nr_servers, int *server_map);
void cls_print_servers();
int cls_get_nr_active_servers();
int cls_get_active_log_ios_rank(int log_ios_rank);
int cls_get_buffer_size();

MPI_Comm cls_get_client_intracomm();
void cls_set_client_intracomm(MPI_Comm);
MPI_Comm cls_get_allclients_intracomm();
int cls_is_client();
int cls_is_server();
int cls_set_clients(int nr_cli, int *cli_nprocs);
int cls_get_client_id();

int cls_server_connect() ;
int cls_server_disconnect(); 

int cls_init();
int cls_finalize(); 


#define CLARISSE_MPI_INIT 0
#define CLARISSE_MPI_INIT_THREAD 1

// type: CLARISSE_MPI_INIT or  CLARISSE_MPI_INIT_THREAD
int MPI_Init_clarisse(int *argc, char ***argv, int required, int *provided, int type);
int MPI_Finalize_clarisse ();
int MPI_File_open_clarisse(MPI_Comm comm, const char *filename, int amode, MPI_Info info, MPI_File *fh);
int MPI_File_close_clarisse(MPI_File *fh);
int MPI_File_delete_clarisse(const char *filename, MPI_Info info);

int MPI_File_read_at_clarisse(MPI_File fh, MPI_Offset offset, void * buf, int count, MPI_Datatype datatype, MPI_Status *status);
int MPI_File_read_clarisse(MPI_File fh, void *buf, int count, MPI_Datatype datatype, MPI_Status *status);
int MPI_File_read_at_all_clarisse(MPI_File fh, MPI_Offset offset, void * buf, int count, MPI_Datatype datatype, MPI_Status *status) ;
int MPI_File_read_all_clarisse(MPI_File fh, void *buf, int count, MPI_Datatype datatype, MPI_Status *status);

int MPI_File_write_at_clarisse(MPI_File fh, MPI_Offset offset, const void *buf, int count, MPI_Datatype datatype, MPI_Status *status);
int MPI_File_write_clarisse(MPI_File fh, const void *buf, int count, MPI_Datatype datatype, MPI_Status *status);
int MPI_File_write_at_all_clarisse(MPI_File fh, MPI_Offset offset, const void *buf, int count, MPI_Datatype datatype, MPI_Status *status);
int MPI_File_write_all_clarisse(MPI_File fh, const void *buf, int count, MPI_Datatype datatype, MPI_Status *status);

int MPI_File_set_view_clarisse(MPI_File fh, MPI_Offset disp, MPI_Datatype etype, MPI_Datatype filetype, const char *datarep, MPI_Info info);
int MPI_File_seek_clarisse(MPI_File fh, MPI_Offset offset, int whence);

int MPI_File_sync_clarisse(MPI_File fh);
int MPI_File_get_size_clarisse(MPI_File fh, MPI_Offset *size);
int MPI_File_set_size_clarisse(MPI_File fh, MPI_Offset size);
#endif
