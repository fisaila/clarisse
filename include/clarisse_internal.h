#ifndef CLARISSE_INTERNAL_H
#define CLARISSE_INTERNAL_H
#include "mpi.h"

typedef struct {
  int aggr_count;
  int *aggr_ranks; 
  MPI_Comm comm;  
} clarisse_file_t;

#define CLARISSE_CTRL_TAG 29092014
#define CLARISSE_CTRL_MSG_DONE 0

#define CLARISSE_NOT_LAST_MSG 0
#define CLARISSE_LAST_MSG 1 
#define CLARISSE_EOF      2 

#define CLARISSE_WRITE 0
#define CLARISSE_READ  1

#ifdef CLARISSE_EXPORT_ACCESS_PATTERN
typedef struct {
  int log_ios_rank;
  int global_descr;
  long long int global_offset;
  int off_len_list_size;
} clarisse_listio_log_t;
#endif


#endif
