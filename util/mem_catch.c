#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "error.h"
#include "util.h"
#include "mpi.h" 

#if defined(CLARISSE_MEM_COUNT) || defined(CLARISSE_MEM_DEBUG) 
//clarisse_timing_t local_timing;
extern clarisse_mem_count_t mem_count;
// This table does not include the memory needed for the internal elements
extern alocated_mem_t *allocated_mem_table;// = NULL;
extern pthread_mutex_t allocated_mem_lock;// = PTHREAD_MUTEX_INITIALIZER;;
extern int multithreaded;// = 0; // Can be changed by cls_init

inline int mem_lock() {
  if (multithreaded)
    return pthread_mutex_lock(&allocated_mem_lock);
  else
    return 0;
}
inline int mem_unlock() {
  if (multithreaded)
    return pthread_mutex_unlock(&allocated_mem_lock);
  else
    return 0;
}
#endif

#ifndef CLARISSE_MEM_DEBUG
extern void *__libc_malloc(size_t size);
extern void *__libc_calloc(size_t nmemb, size_t size);
extern void __libc_free(void *ptr);
extern void *__libc_realloc(void *ptr, size_t size); 

void *malloc(size_t size){
  void *ptr;
  if ((ptr = __libc_malloc(size)) == NULL) { 
    fprintf(stderr, "malloc: Out of memory");
    MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
  }
  
#ifdef CLARISSE_MEM_COUNT
  alocated_mem_t *allocated_mem;
  mem_count.crt_allocated_memory += (double) size;
  if (mem_count.crt_allocated_memory > mem_count.max_allocated_memory)
    mem_count.max_allocated_memory = mem_count.crt_allocated_memory;
  allocated_mem = (alocated_mem_t *)__libc_malloc(sizeof(alocated_mem_t));
  allocated_mem->addr = ptr;
  allocated_mem->size = size;
  mem_lock();
  HASH_ADD_PTR(allocated_mem_table, addr,  allocated_mem);
  mem_unlock();
#endif

#ifdef TRACE_MEMALLOC  
  printf("Malloc %d bytes address = %p\n", (int) size, ptr);
#endif
  return ptr;
}

void *calloc(size_t nmemb, size_t size){
  void *ptr;
  if ((ptr = __libc_calloc(nmemb, size)) == NULL)  { 
    fprintf(stderr, "Calloc: Out of memory");
    MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
  }
  
#ifdef CLARISSE_MEM_COUNT
  alocated_mem_t *allocated_mem;
  mem_count.crt_allocated_memory += (double) nmemb * size;
  if (mem_count.crt_allocated_memory > mem_count.max_allocated_memory)
    mem_count.max_allocated_memory = mem_count.crt_allocated_memory;
  allocated_mem = (alocated_mem_t *)__libc_malloc(sizeof(alocated_mem_t));
  allocated_mem->addr = ptr;
  allocated_mem->size = nmemb * size;
  mem_lock();
  HASH_ADD_PTR(allocated_mem_table, addr,  allocated_mem);
  mem_unlock();
#endif

#ifdef TRACE_MEMALLOC  
  printf("Calloc nmemb =%d and %d bytes address = %p\n", (int) nmemb, (int) size, ptr);
#endif
  return ptr;
}


void free(void *ptr) {
#ifdef TRACE_MEMALLOC  
  printf("Free address %p\n", ptr);
#endif
#ifdef CLARISSE_MEM_COUNT
  alocated_mem_t *allocated_mem;
  mem_lock();
  HASH_FIND_PTR(allocated_mem_table, &ptr, allocated_mem);
  if (allocated_mem){
    HASH_DEL(allocated_mem_table, allocated_mem);
    mem_count.crt_allocated_memory -= (double) allocated_mem->size;
    __libc_free(allocated_mem);
  }
  mem_unlock();
#endif 
  __libc_free(ptr);
}


//void *calloc(size_t nmemb, size_t size);

void *realloc(void *ptr, size_t size) {

#ifdef CLARISSE_MEM_COUNT
  alocated_mem_t *allocated_mem;
  if (ptr) {
    mem_lock();
    HASH_FIND_PTR(allocated_mem_table, &ptr, allocated_mem);
    HASH_DEL(allocated_mem_table, allocated_mem);
    mem_unlock();
  }
  else
    allocated_mem = NULL;
#endif 
  ptr = __libc_realloc(ptr, size);
#ifdef TRACE_MEMALLOC  
  printf("Realloc %d bytes address = %p\n", (int) size, ptr);
#endif
#ifdef CLARISSE_MEM_COUNT
  if (allocated_mem)
    mem_count.crt_allocated_memory = mem_count.crt_allocated_memory - allocated_mem->size + (double) size;  
  else
    mem_count.crt_allocated_memory = mem_count.crt_allocated_memory + (double) size; 
  if (mem_count.crt_allocated_memory > mem_count.max_allocated_memory)
    mem_count.max_allocated_memory = mem_count.crt_allocated_memory;

  if (!allocated_mem)
    allocated_mem = (alocated_mem_t *)__libc_malloc(sizeof(alocated_mem_t));
  allocated_mem->addr = ptr; // ptr could have changed
  allocated_mem->size = size;
  mem_lock();
  HASH_ADD_PTR(allocated_mem_table, addr,  allocated_mem);
  mem_unlock();
#endif  
  return ptr;
}

#else

void clarisse_print_leaks(){
  alocated_mem_t *allocated_mem;
  printf("FOLLOWING LEAKS WERE IDENTIFIED:\n");
  if (allocated_mem_table) {
    alocated_mem_t *tmp;    
    HASH_ITER(hh, allocated_mem_table, allocated_mem, tmp) {
      printf("\t Function:line %s:%d addr=%p\n", allocated_mem->function, allocated_mem->line, allocated_mem->addr);
    }
  }
  else
    printf("\t NO LEAKS!\n");
}

#endif


