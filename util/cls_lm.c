#include "cls_lm.h"
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"

static double mean(double *a, int n) {
  int i;
  double sum = 0;
  for (i = 0; i < n; i++)
    sum += a[i];
  return sum / n;
}

int cls_lm_predict(cls_linear_model_t *lm, double x, double *y) {
  if (lm->trained) {
    *y = lm->alfa + lm->beta * x;
    return 0;
  }
  else 
    return -1;
}

double cls_lm_prediction_error(double predicted, double real) {
  return abs(predicted - real);
}

void cls_lm_init(cls_linear_model_t *lm) {
  lm->trained = 0;
  lm->nr_training_points = 0;
}

int cls_lm_add_training_point(cls_linear_model_t *lm, double x, double y){
  if (lm->nr_training_points >= CLS_MAX_TRAINING_POINTS)
    return -1;
  else {
    lm->trained = 1;
    lm->x[lm->nr_training_points] = x;
    lm->y[lm->nr_training_points] = y;
    lm->nr_training_points++;
    return 0;
  }
}

int cls_lm(cls_linear_model_t *lm){

  if (lm->nr_training_points < 2) 
    return -1; 
  else {
    double *x_delta, *y_delta, x_sd, y_sd, x_mean, y_mean, xy_sum = 0;
    int i, n;
    
    n = lm->nr_training_points;
    lm->rse = 0;
    x_delta = (double *) clarisse_malloc(n * sizeof(double));
    y_delta = (double *) clarisse_malloc(n * sizeof(double));
    
    x_mean = mean(lm->x, n);
    y_mean = mean(lm->y, n);
    
    xy_sum = 0;
    x_sd = 0;
    for (i = 0; i < n; i++) {
      x_delta[i] = lm->x[i] - x_mean;
      y_delta[i] = lm->y[i] - y_mean;
      xy_sum = xy_sum + x_delta[i] * y_delta[i];
      x_sd = x_sd + x_delta[i] * x_delta[i];
      y_sd = y_sd + y_delta[i] * y_delta[i];
    }
    lm->beta = xy_sum / x_sd; 
    lm->alfa = y_mean - x_mean * lm->beta;
    lm->rsquared = lm->beta * sqrt(x_sd) / sqrt(y_sd);
    lm->rsquared *= lm->rsquared;
    
    for (i = 0; i < n; i++)
      lm->rse += (lm->y[i] - lm->alfa - lm->beta * lm->x[i]) *  (lm->y[i] - lm->alfa - lm->beta * lm->x[i]);
    lm->rse = sqrt(lm->rse / (n - 2));
    
    
    printf("y = %lf + %lf * x, where Rsquared = %lf and rse (resid std err) = %lf\n", lm->alfa, lm->beta, lm->rsquared, lm->rse);
    
    clarisse_free(x_delta);
    clarisse_free(y_delta);
    return 0;
  }
}


 
int _main_test() {

  double x[20], y[20];
  int n, i;
  cls_linear_model_t lm;

  cls_lm_init(&lm);
  printf("Enter the value of n: ");
  scanf("%d", &n);
  if (n > CLS_MAX_TRAINING_POINTS) {
    printf("n > %d, set n as CLS_MAX_TRAINING_POINTS", CLS_MAX_TRAINING_POINTS);
    n = CLS_MAX_TRAINING_POINTS;
  }
  printf("Enter the values of x and y:\n");
  for (i = 0; i <  n; i++) {
    scanf("%lf%lf", &x[i], &y[i]);
    cls_lm_add_training_point(&lm, x[i], y[i]);
  }

  cls_lm(&lm);

  return 0;
}
 
