#include "interval_list.h"
#include <stdio.h>
#include <assert.h>
#include "util.h"

void interval_list_insert(struct dllist *il, long long int l, long long int r){

  assert(l <= r);
  if (dllist_is_empty(il)) {
    interval_t *i = (interval_t *) clarisse_malloc(sizeof(interval_t));
    i->l = l;
    i->r = r;
    dllist_iat(il, &i->link);
  }
  else {
    struct dllist_link *aux;
    interval_t *iprev = NULL;
    interval_t *iupdated = NULL;
    for(aux = il->head; aux != NULL; aux = aux->next) {
      interval_t *icrt = DLLIST_ELEMENT(aux, interval_t, link);	
      if (!iupdated) {
	if (!iprev) {// first element
	  if (r  + 1 < icrt->l ){
	    interval_t *i = (interval_t *) clarisse_malloc(sizeof(interval_t));
	    i->l = l;
	    i->r = r;
	    dllist_iah(il, &i->link);
	    return;
	  }
	  /*	  else
	    if (icrt->r + 1 < l)  {
	      interval_t *i = (interval_t *) clarisse_malloc(sizeof(interval_t));
	      i->l = l;
	      i->r = r;
	      dllist_iat(il, &i->link);
	      return;
	      }*/
	}
	if (aux->next == NULL) {
	    if (l > icrt->r + 1){
	      interval_t *i = (interval_t *) clarisse_malloc(sizeof(interval_t));
	      i->l = l;
	      i->r = r;
	      dllist_iat(il, &i->link);
	      return;
	    }
	  }
	
	if ((iprev) &&
	    (l > iprev->r + 1) &&
	    (r + 1 < icrt->l)){
	  interval_t *i = (interval_t *) clarisse_malloc(sizeof(interval_t));
	  i->l = l;
	  i->r = r;
	  dllist_ibefore(il, &icrt->link, &i->link);
	  return;
	}
     
	// here I am sure that (l,r) either overlaps with icrt OR it must be inserted later  
      
	if ((r + 1 == icrt->l) || 
	    (l == icrt->r + 1) ||
	    ((l <= icrt->l) && (r  + 1 >= icrt->l)) || 
	    ((l >= icrt->l) && (l <= icrt->r + 1))){
	  icrt->l = MIN(l, icrt->l);
	  icrt->r = MAX(r, icrt->r);
	  iupdated = icrt;
	}	 
      	iprev = icrt;
      } // if (!iupdated)
      else {
	if ((iupdated->r + 1 == icrt->l) ||
	    (iupdated->l == icrt->r + 1) ||
	    ((iupdated->l <= icrt->l) && (iupdated->r  + 1 >= icrt->l)) || 
	    ((iupdated->l >= icrt->l) && (iupdated->l <= icrt->r + 1))){
	  icrt->l = MIN(iupdated->l, icrt->l);
	  icrt->r = MAX(iupdated->r, icrt->r);
	  dllist_rem(il, &iupdated->link);
	  clarisse_free(iupdated);
	  iupdated = icrt;
	}
	else 
	  return;
      }
    }
  }
}
void interval_list_free(struct dllist *il) {
  while (!dllist_is_empty(il)) {
    clarisse_free(DLLIST_ELEMENT(dllist_rem_head(il), interval_t, link));
  }
}

int interval_list_covers(struct dllist *il, long long int l, long long int r){
  struct dllist_link *aux; 
  for(aux = il->head; aux != NULL; aux = aux->next) {
    interval_t *icrt = DLLIST_ELEMENT(aux, interval_t, link);
    if ((l >= icrt->l) && (r <= icrt->r))
      return 1;
    if (l > icrt->r) 
      return 0;
  }
  return 0;
}

void interval_list_print(struct dllist *il){
  struct dllist_link *aux;
  printf("INTERVAL LIST: ");
  for(aux = il->head; aux != NULL; aux = aux->next) {
    interval_t *icrt = DLLIST_ELEMENT(aux, interval_t, link);
    printf("(%lld, %lld) ", icrt->l, icrt->r);
  }
  printf("\n");
}
