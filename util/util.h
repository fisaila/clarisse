#ifndef UTIL_H
#define UTIL_H
#include <stdlib.h>
#include "uthash.h"

#define CLARISSE_UNUSED(x) (void)(x)

typedef struct {
  void *addr;
  size_t size;
#ifdef CLARISSE_MEM_DEBUG
  char *function;
  int line;
#endif
  UT_hash_handle hh;
} alocated_mem_t;

#ifdef CLARISSE_TIMING
#define CLARISSE_MAX_CNT_TIMED_OPS 1024
typedef struct {
  int cnt_open;
  int cnt_close;
  int cnt_setview;
  int cnt_write_all;
  int cnt_read_all;
  
  double time_open[CLARISSE_MAX_CNT_TIMED_OPS][2];
  double time_close[CLARISSE_MAX_CNT_TIMED_OPS][2];
  double time_setview[CLARISSE_MAX_CNT_TIMED_OPS][2];
  double size_write_all[CLARISSE_MAX_CNT_TIMED_OPS];
  double time_write_all[CLARISSE_MAX_CNT_TIMED_OPS][2];
  double time_write_all_wait[CLARISSE_MAX_CNT_TIMED_OPS][2]; // waiting for scheduling
  double size_read_all[CLARISSE_MAX_CNT_TIMED_OPS];
  double time_read_all[CLARISSE_MAX_CNT_TIMED_OPS][2];
#ifdef CLARISSE_EXTERNAL_BUFFERING
  double time_write_all_domain[CLARISSE_MAX_CNT_TIMED_OPS][2];
  double time_write_all_blockmask[CLARISSE_MAX_CNT_TIMED_OPS][2];
  double time_write_all_data_transfer[CLARISSE_MAX_CNT_TIMED_OPS][2];
  double time_read_all_domain[CLARISSE_MAX_CNT_TIMED_OPS][2];
  double time_read_all_blockmask[CLARISSE_MAX_CNT_TIMED_OPS][2];
  double time_read_all_data_transfer[CLARISSE_MAX_CNT_TIMED_OPS][2];  

#endif
  int cnt_file_write;
  int cnt_file_read;
  double time_file_write;
  double time_file_read;
  double size_file_write;
  double size_file_read;
  
  double initial_time;
} clarisse_timing_t;


void clarisse_timing_init(clarisse_timing_t *timing);
void clarisse_timing_finalize(clarisse_timing_t *timing);
#endif

#if defined(CLARISSE_MEM_COUNT) || defined(CLARISSE_MEM_DEBUG) 
typedef struct {
  double max_allocated_memory;
  double crt_allocated_memory;
} clarisse_mem_count_t;

void clarisse_mem_count_init(clarisse_mem_count_t *timing);
void clarisse_mem_count_finalize(clarisse_mem_count_t *timing);
#endif


void *clarisse_malloc(size_t size);
void clarisse_free(void *ptr);
//void *clarisse_calloc(size_t nmemb, size_t size);
void *clarisse_realloc(void *ptr, size_t size); 
void clarisse_print_leaks();

#if defined(CLARISSE_MEM_COUNT) && defined(CLARISSE_MEM_DEBUG)
extern alocated_mem_t *allocated_mem_table;
#define clarisse_malloc(toallocatesize) ({	\
      void *ptr;				\
      alocated_mem_t *allocated_mem;		\
      if ((ptr = malloc(toallocatesize)) == NULL)	\
	printf("Out of memory\n");					\
      allocated_mem = (alocated_mem_t *)malloc(sizeof(alocated_mem_t));	\
      allocated_mem->addr = ptr;					\
      allocated_mem->size = toallocatesize;				\
      allocated_mem->function = malloc(strlen(__FUNCTION__)+1);		\
      strcpy(allocated_mem->function, __FUNCTION__);			\
      allocated_mem->line = __LINE__;					\
      HASH_ADD_PTR(allocated_mem_table, addr,  allocated_mem);		\
      ptr;								\
    })

#define clarisse_free(ptr) do{						\
    alocated_mem_t *allocated_mem;					\
    HASH_FIND_PTR(allocated_mem_table, &(ptr), allocated_mem);		\
    if (allocated_mem){							\
      HASH_DEL(allocated_mem_table, allocated_mem);			\
      free(allocated_mem->function);					\
      free(allocated_mem);						\
    }									\
    free(ptr); 								\
    } while(0)

#define clarisse_realloc(ptr, toreallocatesize) ({			\
      alocated_mem_t *allocated_mem;					\
      if (ptr) {							\
	HASH_FIND_PTR(allocated_mem_table, &ptr, allocated_mem);	\
	HASH_DEL(allocated_mem_table, allocated_mem);			\
      }									\
      else								\
	allocated_mem = NULL;						\
      ptr = realloc(ptr, toreallocatesize);				\
      if (!allocated_mem)						\
	allocated_mem = (alocated_mem_t *)malloc(sizeof(alocated_mem_t)); \
      allocated_mem->addr = ptr;					\
      allocated_mem->size = toreallocatesize;				\
      allocated_mem->function = malloc(strlen(__FUNCTION__)+1);		\
      strcpy(allocated_mem->function, __FUNCTION__);			\
      allocated_mem->line = __LINE__;					\
      HASH_ADD_PTR(allocated_mem_table, addr,  allocated_mem);		\
      ptr;								\
    })

#endif

#define SET_NTH_BIT(x, n) ((x) |= (1 << (n)))
#define CLEAR_NTH_BIT(x, n) ((x) &= (~(1 << (n))))
#define TOGGLE_NTH_BIT(x, n) ((x) ^= (1 << (n)))
#define CHECK_NTH_BIT(x, n) (((x) >> (n)) & 1)


#endif
