#include "adio.h"
#include "adio_extern.h"

ios_task_p find_ios_task(int global_fd, int proc_rank, dllist *req_list){
    struct dllist_link *aux;
    for (aux = req_list->head;aux != NULL;aux = aux->next) {
	ios_task_p r = (ios_task_p)aux;
	if ((global_fd == r->o->global_fd) && 
	    (proc_rank == r->proc_rank)) 
	    return r;
    }
    return NULL;
}

ios_task_p alloc_init_ios_task(ofile_info_p o, int proc_rank, ADIO_Offset l_f, ADIO_Offset r_f) {
    ios_task_p iot =  (ios_task_p) ADIOI_clarisse_malloc(sizeof(ios_task));
    iot->o = o;
    iot->proc_rank = proc_rank;
    iot->l_f = l_f;
    iot->r_f = r_f;
    iot->confirmed = 0;
    iot->errn = 0;
    return iot;
}


int create_io_tasks(ADIO_File fd, void *buf, int bufsize,
		    distrib_p buf_distrib, ADIO_Offset l_v, ADIO_Offset l_f, ADIO_Offset r_f) {
		    	
    ADIO_Offset r_v, crt_l_v, crt_l_f, crt_r_v, crt_r_f;
    int  block_size, log_ios_rank;
    int num_tasks = 0;
    struct file_info *file_info = (struct file_info *)fd->adio_ptr;
    int ph_ios,i;
    int *sends;
     
    file_info->pending_ios = 0;
    for (i=0;i<fd->hints->cb_nodes;i++) {
        file_info->pending_ios_ret[i] = 0;
    }

    block_size = file_info->block_size; 
    sends =(int *) ADIOI_clarisse_malloc(fd->hints->cb_nodes * sizeof(int));
    for (i=0;i<fd->hints->cb_nodes;i++) sends[i] = 0;    

    crt_l_v = l_v;
    r_v = l_v + bufsize - 1;
    crt_l_f = l_f;
    log_ios_rank = (l_f / block_size) % fd->hints->cb_nodes ;
    while (crt_l_f <= r_f) {
        //log_ios_rank = fd->hints->ranklist[log_ios_rank_orig]; 
	crt_r_f = ADIOI_MIN(r_f, END_BLOCK(crt_l_f,block_size));
	crt_r_v = func_inf(crt_r_f, &file_info->distrib); 
	if (crt_l_v <= crt_r_v) {
	    ios_task_p iot = (ios_task_p) ADIOI_clarisse_malloc(sizeof(ios_task));
	    iot->count = crt_r_v - crt_l_v + 1;
	    iot->l_f = crt_l_f;
	    iot->r_f = crt_r_f;
	    iot->l_dt = func_1(crt_l_v - l_v, buf_distrib); 
	    iot->r_dt = func_1(crt_r_v - l_v, buf_distrib);
	    iot->buf_distrib = buf_distrib;
	    iot->buf = ((char *)buf); // + iot->l_dt ; //crt_l_v - l_v;
	    iot->log_ios_rank = log_ios_rank;
            
            if (file_info->view_send[log_ios_rank] == 0) {
                  file_info->view_send[log_ios_rank]++;
                  iot->first = 1;
            } else
                  iot->first = 0;

            if (!file_info->pending_ios_ret[log_ios_rank]) {
                  file_info->pending_ios_ret[log_ios_rank] = 1;
                  file_info->pending_ios++;
            }
	    circ_hash_insert(&file_info->task_hash, fd->hints->ranklist[log_ios_rank] , (void*)iot);
	    num_tasks++;
	}
	
	crt_l_f = BEGIN_NEXT_BLOCK(crt_l_f ,block_size);
	crt_l_v = func_sup(crt_l_f, &file_info->distrib);
	log_ios_rank = (log_ios_rank + 1) % fd->hints->cb_nodes ;
    }    
    ADIOI_clarisse_free(sends);
    return num_tasks;
}
