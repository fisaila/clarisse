#ifndef INFO_INCLUDE

#define INFO_INCLUDE

#define ADIO_BLOCK_SIZE     64 * 1024
#define ADIO_NET_BUF_SIZE   64 * 1024

#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <fcntl.h>
#include "dllist.h"
#include "buffer_pool.h"
#include "map.h"
#include "hash.h"
#include "ofile.h"

// This structures is used for incrementally building a hindexed datatyped, subsequently used for transfers
struct net_dt {
    int count;
    int max_count;
    int incr;
    MPI_Aint base_address;
    int *block_lens;
    MPI_Aint *displs;
} ;

struct state{
  struct dllist open_files;
  struct dllist write_req_list;
  circ_hash read_req_list;
  struct buffer_pool buf_pool;
  struct hash ds_hash;
} ;


struct file_info {
    int block_size;
    distrib distrib; // current view
    circ_hash task_hash; // hash containing a bucket of reqs for each phys ios 
    short *pending_ios_ret; // 0: no request pending // 1 :o/w
    int pending_ios;
    short *view_send;
    int net_buf_size;
    int buffer_pool_size;
    char *net_buf;
    struct net_dt net_dt;
    struct state state;
    ofile_info_p o;
    int pool_prefetch;
    int stop_thread; 
#ifndef BlueGeneL  
    pthread_t thread;
    pthread_cond_t flush_not_full; 
    pthread_cond_t flush_not_empty;
    pthread_mutex_t mutex;
#endif
    MPI_Comm comm;
    int lazyview;
    int prefetch;
    int flush;
    int lzo;

};



void alloc_init_file_info(ADIO_File fd);
void free_file_info(ADIO_File fd);

/* Functions for manipulating the hindexed datatype used for transfers for
avoiding memcpy

*/

static void alloc_net_dt(struct net_dt *net_dt);
static void init_net_dt (struct net_dt *net_dt, int max_count, int incr);
void init_alloc_net_dt (struct net_dt *net_dt, int max_count, int incr);
void free_net_dt(struct net_dt *net_dt);
void update_net_dt(struct net_dt *net_dt, char *buf, int len);
MPI_Datatype commit_net_dt(struct net_dt *net_dt);



#endif




