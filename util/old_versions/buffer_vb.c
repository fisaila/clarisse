#include "adio.h"
#include "adio_extern.h"

char * my_get_buffer(ADIO_File fd, ofile_info_p o) {
    struct file_info *file_info = (struct file_info *)fd->adio_ptr;
    if (file_info->flush) {
#ifndef BlueGeneL
         pthread_mutex_lock(&file_info->mutex);
         while (POOL_FULL(file_info->state.buf_pool ))
             pthread_cond_wait(&file_info->flush_not_full, &file_info->mutex);
         char *buf= get_buffer(&file_info->state.buf_pool);
         pthread_cond_signal(&file_info->flush_not_empty);
         pthread_mutex_unlock(&file_info->mutex);
         if (buf) 
         	return buf;
#endif
    } else 
    {
         struct file_info *file_info = (struct file_info *)fd->adio_ptr;
         char *buf= get_buffer(&file_info->state.buf_pool);
         if (buf)
                return buf;
         else {
                ADIO_Offset file_disp;
                page_info_p p = hash_rm_lru(&file_info->state.ds_hash);
                file_disp =p->idx/o->ios_per_file * file_info->block_size +
                   (o->ios_per_file -1) * file_info->block_size +
                   o->log_ios_rank * file_info->block_size;
                file_disp =(p->idx/o->ios_per_file)  * file_info->block_size +
                 (p->idx/o->ios_per_file *  (o->ios_per_file -1)) * file_info->block_size +
                 (o->log_ios_rank) * file_info->block_size;
                //printf ("p idx %lld  file_disp %lld\n",p->idx,file_disp);
                write_dirty_page(fd, o->local_fd, file_disp, p);
                //o->frsize = o->fsize;
                buf = p->page;
                ADIOI_clarisse_free(p);
                return buf;
        }
    }
}

void flush_buffer(ADIO_File fd, ofile_info_p o, ADIO_Offset offset) {
  struct file_info *file_info = (struct file_info *)fd->adio_ptr;
  int b = file_info->block_size;
  page_info_p p = find_page(&file_info->state.ds_hash, o->global_fd, offset / b);
  if (p) {
    //printf ("buffer: put page %d\n",p->idx);
    if (p->dirty) {
       write_dirty_page(fd, o->local_fd, offset, p);
    }
    
    put_buffer(p->page, &file_info->state.buf_pool);
    hash_rm(&file_info->state.ds_hash, p); 
    ADIOI_clarisse_free(p);
  }
}

// drops the buffer (when truncating or deleting the file)
void drop_buffer(ADIO_File fd, ofile_info_p o, ADIO_Offset offset) {
  struct file_info *file_info = (struct file_info *)fd->adio_ptr;
  int b = file_info->block_size;
  page_info_p p = find_page(&file_info->state.ds_hash, o->global_fd, offset / b);
  if (p) {
    put_buffer(p->page, &file_info->state.buf_pool);
    hash_rm(&file_info->state.ds_hash, p);
    ADIOI_clarisse_free(p);
  }
}


// starts at b * o->log_ios_rank (first block presented on this server)
void flush_buffers(ADIO_File fd, ofile_info_p o) {
  int err;
  struct file_info *file_info = (struct file_info *)fd->adio_ptr;
  if (o->fsize) {
    int b = file_info->block_size;
    ADIO_Offset i, final_r = func_1(o->fsize - 1, &o->subf_distrib);
    for (i = b * o->log_ios_rank; i <= final_r; i+= b * o->ios_per_file) 
      flush_buffer(fd, o, i); 
      drop_buffer(fd, o, i); 
    o->fsize = 0;
  }
}


// offset MUST be present on the current buffer BECAUSE  i += b * o->ios_per_file
void drop_buffers_from(ADIO_File fd, ofile_info_p o, ADIO_Offset offset) {
  struct file_info *file_info = (struct file_info *)fd->adio_ptr;
  if (o->fsize == 0) 
    return;
  else {
    int b = file_info->block_size;
    ADIO_Offset i; 
    o->fsize = ADIOI_MIN(o->fsize, offset);
    int final_r = func_1(o->fsize -1,&o->subf_distrib);
    if (offset % b) {
      page_info_p p = find_page(&file_info->state.ds_hash, o->global_fd, offset / b);
      if (p) {
	 	bzero(p->page + offset % b, END_BLOCK(offset, b) - offset + 1); 
	 	p->r_dirty = offset % b - 1;
      }
      offset = BEGIN_BLOCK(offset, b) + b * o->ios_per_file;
    }

    // offset is begining of a block
    for (i = offset; i <= final_r; i += b * o->ios_per_file) 
      drop_buffer(fd, o, i);
    
  }
}
