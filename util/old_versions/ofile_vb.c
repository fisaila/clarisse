#include "adio.h"
#include "adio_extern.h"

ofile_info_p find_ofile(ADIO_File fd, int global_fd) {
    struct file_info *file_info = (struct file_info *)fd->adio_ptr;
    struct dllist_link *aux;
    for (aux = file_info->state.open_files.head; (aux!= NULL) && (((ofile_info_p)aux)->global_fd != global_fd); aux = aux->next); 
    return (ofile_info_p)aux;
}

ofile_info_p find_insert_ofile(ADIO_File fd, int global_fd, int log_ios_rank, int ios_per_file, int nprocs) {
	struct file_info *file_info = (struct file_info *)fd->adio_ptr;
	MPI_Comm dupcommself;
        int error_code, myrank;
        ADIO_File local_fd;
        ADIO_Fcntl_t *fcntl_struct;
	MPI_Comm_rank(fd->comm, &myrank);
        int i;
	 

    ofile_info_p o  = find_ofile(fd,global_fd);
    if (!o) {
		int i;
		
		int block_lengths[3] = {1, file_info->block_size, 1}; 
		MPI_Aint displacements[3] = {0, log_ios_rank * file_info->block_size, ios_per_file * file_info->block_size};
		MPI_Datatype t, typelist[3] = {MPI_LB, MPI_CHAR, MPI_UB};

        
		o = (ofile_info_p) malloc(sizeof(ofile_info));
		o->global_fd = global_fd;
//                for (i=0; i< fd->hints->cb_nodes;i++)
//                    if(fd->hints->ranklist[i] == log_ios_rank) 
//		          o->log_ios_rank = i;
		o->log_ios_rank = log_ios_rank;
		o->ios_per_file = ios_per_file;
		MPI_Type_struct(3, block_lengths, displacements, typelist, &t);
		init_distrib(&o->subf_distrib, 0, t);

		o->nprocs = nprocs;
		o->d = (distrib *) ADIOI_clarisse_malloc(nprocs * sizeof(distrib));
                if (o->d == NULL)
  	                handle_err(MPI_ERR_NO_MEM, "ofile: malloc error \n");
		o->last_p = 0;
		fcntl_struct = (ADIO_Fcntl_t *) ADIOI_clarisse_malloc(sizeof(ADIO_Fcntl_t));
                if (fcntl_struct == NULL)
  	                handle_err(MPI_ERR_NO_MEM, "ofile: malloc error \n");
	        ADIO_Fcntl(fd, ADIO_FCNTL_GET_FSIZE, fcntl_struct, &error_code);
        	/* --BEGIN ERROR HANDLING-- */
	        if (error_code != MPI_SUCCESS)
	                error_code = MPIO_Err_return_file(fd, error_code);
	        /* --END ERROR HANDLING-- */
	        o->fsize = o->frsize = fcntl_struct->fsize;

	        ADIOI_clarisse_free(fcntl_struct);        

		for (i = 0; i < nprocs; i++) 
	    	init_distrib(&o->d[i], 0, MPI_CHAR);
		dllist_iat(&file_info->state.open_files,(struct dllist_link *)o); 
                //my_fprintf(stderr,"find_insert_ofile:o->log_ios_rank=%d\n",o->log_ios_rank);	
    }
 
    return o;
}

void rm_ofile(ADIO_File fd, ofile_info_p o) {
  struct file_info *file_info = (struct file_info *)fd->adio_ptr;
  dllist_rem(&file_info->state.open_files,(struct dllist_link *)o); 
  ADIOI_clarisse_free(o->d);
  ADIOI_clarisse_free(o);
}
