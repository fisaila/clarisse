#ifndef TASKS_INCLUDE
#define TASKS_INCLUDE

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/mman.h>
#include "map.h"
#include "dllist.h"

typedef struct ios_task {
  struct dllist_link link;
  int proc_rank;
  int count;
  ofile_info_p o;
  ADIO_Offset l_f;
  ADIO_Offset  r_f;
  ADIO_Offset l_dt;
  ADIO_Offset r_dt;
  distrib_p buf_distrib;
  int confirmed;
  int errn;
  int log_ios_rank;
  char *buf;
  int first;
} ios_task, *ios_task_p, *iotask_p;




ios_task_p alloc_init_ios_task(ofile_info_p o, int proc_rank, ADIO_Offset l_f, ADIO_Offset r_f);
ios_task_p find_ios_task(int global_fd, int proc_rank, dllist *req_list);
int create_io_tasks(ADIO_File fd, void *buf, int bufsize,
		    distrib_p buf_distrib, ADIO_Offset l_v, ADIO_Offset l_f, ADIO_Offset r_f);

#endif /*TASKS_H_*/
