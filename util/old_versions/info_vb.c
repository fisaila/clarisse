#include "adio.h"
#include "adio_extern.h"

void alloc_init_file_info(ADIO_File fd) {
    struct file_info *file_info = (struct file_info *)ADIOI_clarisse_malloc(sizeof(struct file_info));
    int i, nprocs;
    //my_fprintf(stderr, "alloc_init_file_info\n"); 
    MPI_Comm_size(fd->comm, &nprocs);
    
    file_info->pending_ios_ret  = (short *)ADIOI_clarisse_malloc(nprocs*sizeof(short));
    file_info->view_send  = (short *)ADIOI_clarisse_malloc(nprocs*sizeof(short));
    for (i=0;i<nprocs;i++) {
	file_info->pending_ios_ret[i] = 0;
        file_info->view_send[i] = 0;
    }

    file_info->pending_ios  = 0;
#ifndef BlueGene
    file_info->stop_thread  = 0;  
#endif
    file_info->comm         = fd->comm;
    file_info->lazyview     = 1;   
    file_info->prefetch     = 1;   
    file_info->flush        = 0;   
    file_info->lzo          = 0;   
    
    init_alloc_net_dt(&file_info->net_dt, 256, 256);
    circ_hash_init(&file_info->task_hash,nprocs,HASH_POLICY);
    
    dllist_init(&file_info->state.open_files);
    dllist_init(&file_info->state.write_req_list);

    hash_init(&file_info->state.ds_hash, NR_DS_BUCKETS, MAX_DS_HASH_SIZE);
    fd->adio_ptr = (void*)  file_info;
}

void free_file_info(ADIO_File fd) {
    int myrank;
    struct file_info *file_info = ( struct file_info *)fd->adio_ptr;
    //my_fprintf(stderr, "free_file_info\n");
    MPI_Comm_rank(fd->comm, &myrank);
    
    ADIOI_clarisse_free(file_info->pending_ios_ret);
    ADIOI_clarisse_free(file_info->view_send);
    free_net_dt(&file_info->net_dt);
    circ_hash_free(&file_info->task_hash);
    file_info->pending_ios = 0;
    circ_hash_free(&file_info->state.read_req_list);
    free_hash(&file_info->state.ds_hash);
    //circ_hash_free(&file_info->state.write_req_list);

    if (is_aggregator(myrank, fd)) {
         buffer_pool_free(&file_info->state.buf_pool);
    }
    ADIOI_clarisse_free(file_info->net_buf);
    ADIOI_clarisse_free(file_info);
    fd->adio_ptr = NULL;
}

/* Functions for manipulating the hindexed datatype used for transfers for
avoiding memcpy

*/

static void alloc_net_dt(struct net_dt *net_dt){
    if ((net_dt->block_lens = (int *) ADIOI_clarisse_malloc(net_dt->max_count*sizeof(int))) == NULL)
        handle_error(MPI_ERR_NO_MEM, "Out of memory");;
    if ((net_dt->displs = (MPI_Aint *) ADIOI_clarisse_malloc(net_dt->max_count*sizeof(MPI_Aint))) == NULL)
        handle_error(MPI_ERR_NO_MEM, "Out of memory");;
}

static void init_net_dt (struct net_dt *net_dt, int max_count, int incr) {
    net_dt->count = 0;
    net_dt->max_count = max_count;
    net_dt->incr = incr;
}

void init_alloc_net_dt (struct net_dt *net_dt, int max_count, int incr) {
    init_net_dt (net_dt, max_count, incr);
    alloc_net_dt(net_dt);
}

void free_net_dt(struct net_dt *net_dt){
    ADIOI_clarisse_free(net_dt->block_lens);
    ADIOI_clarisse_free(net_dt->displs);
}

void update_net_dt(struct net_dt *net_dt, char *buf, int len) {
    MPI_Aint a;
    if (net_dt->count == net_dt->max_count) {
        net_dt->max_count += net_dt->incr;
        if ((net_dt->block_lens = (int *) ADIOI_Realloc(net_dt->block_lens, net_dt->max_count*sizeof(int))) == NULL)
            handle_error(MPI_ERR_NO_MEM, "Out of memory");
        if ((net_dt->displs = (MPI_Aint *) ADIOI_Realloc(net_dt->displs, net_dt->max_count*sizeof(MPI_Aint))) == NULL)
            handle_error(MPI_ERR_NO_MEM, "Out of memory");
    }
    net_dt->block_lens[net_dt->count] = len;
    MPI_Address(buf, &a);
    if (net_dt->count == 0) {
        net_dt->base_address = a;
        net_dt->displs[net_dt->count] = 0;
    }
    else
        net_dt->displs[net_dt->count] = a -  net_dt->base_address;
    net_dt->count++;
}

MPI_Datatype commit_net_dt(struct net_dt *net_dt) {
    MPI_Datatype d;
    int err = MPI_Type_hindexed(net_dt->count, net_dt->block_lens, net_dt->displs, MPI_BYTE, &d);
    if (err != MPI_SUCCESS)
        handle_error(err, "Error at creating the hindexed\n");
    MPI_Type_commit(&d);
    return d;
}




