#ifndef OFILE_INCLUDE
#define OFILE_INCLUDE

#include <stdio.h>
#include "map.h"
#include "dllist.h"

typedef struct ofile_info{
  struct dllist_link link;
  int global_fd;
  int log_ios_rank;
  int ios_per_file;
  distrib subf_distrib;
  int nprocs;
  distrib *d;
  ADIO_File local_fd;
  ADIO_Offset fsize; // MAX fsize: used only for DS (?!)
  ADIO_Offset frsize; // MAX fsize: used only for DS (?!)
  int last_p;
} ofile_info, *ofile_info_p;



ofile_info_p find_ofile(ADIO_File fd, int global_fd);
ofile_info_p find_insert_ofile(ADIO_File fd, int global_fd, int log_ios_rank, int ios_per_file, int nprocs);
void rm_ofile(ADIO_File fd, ofile_info_p o);

#endif /*OFILE_H_*/
