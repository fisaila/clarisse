#ifndef _CLS_LM_INCLUDE
#define _CLS_LM_INCLUDE


#define CLS_MAX_TRAINING_POINTS 10

typedef struct {
  int trained;
  int nr_training_points; //number of training points
  double x[CLS_MAX_TRAINING_POINTS], y[CLS_MAX_TRAINING_POINTS];
  double alfa, beta, rsquared, rse;
} cls_linear_model_t;


void cls_lm_init(cls_linear_model_t *lm);
int cls_lm_add_training_point(cls_linear_model_t *lm, double x, double y);
int cls_lm(cls_linear_model_t *lm);
int cls_lm_predict(cls_linear_model_t *lm, double x, double *y);
double cls_lm_prediction_error(double predicted, double real);

#endif
