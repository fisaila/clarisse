#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include "error.h"
#include "mpi.h"
//#include "adio.h"
//#include "adio_extern.h"
#include "util.h"

#if defined(CLARISSE_MEM_COUNT) || defined(CLARISSE_MEM_DEBUG) 
//clarisse_timing_t local_timing;
clarisse_mem_count_t mem_count = {0, 0};
// This table does not include the memory needed for the internal elements
alocated_mem_t *allocated_mem_table = NULL;
pthread_mutex_t allocated_mem_lock = PTHREAD_MUTEX_INITIALIZER;;
int multithreaded = 0; // Can be changed by cls_init
#endif 

#ifndef CLARISSE_MEM_DEBUG
void *clarisse_malloc(size_t size){
  //  return ADIOI_Malloc(size);
  return malloc(size);
}

void clarisse_free(void *ptr) {
  //ADIOI_Free(ptr);
  free(ptr);
}

//void *clarisse_calloc(size_t nmemb, size_t size);

void *clarisse_realloc(void *ptr, size_t size) {
  //return ADIOI_Realloc(ptr, size);
  return realloc(ptr, size);
}
#endif


//#if 0

#if defined(CLARISSE_MEM_DEBUG) && !defined(CLARISSE_MEM_COUNT)

inline int mem_lock() {
  if (multithreaded)
    return pthread_mutex_lock(&allocated_mem_lock);
  else
    return 0;
}
inline int mem_unlock() {
  if (multithreaded)
    return pthread_mutex_unlock(&allocated_mem_lock);
  else
    return 0;
}


void *clarisse_malloc(size_t size){
  void *ptr;
  //if ((ptr = ADIOI_Malloc(size)) == NULL) 
  if ((ptr = malloc(size)) == NULL) 
    handle_err(MPI_ERR_NO_MEM, "Out of memory");
  
  alocated_mem_t *allocated_mem;
  mem_count.crt_allocated_memory += (double) size;
  if (mem_count.crt_allocated_memory > mem_count.max_allocated_memory)
    mem_count.max_allocated_memory = mem_count.crt_allocated_memory;
  allocated_mem = (alocated_mem_t *)malloc(sizeof(alocated_mem_t));
  allocated_mem->addr = ptr;
  allocated_mem->size = size;
  mem_lock();
  HASH_ADD_PTR(allocated_mem_table, addr,  allocated_mem);
  mem_unlock();

#ifdef TRACE_MEMALLOC  
  printf("Malloc %d bytes address = %p\n", (int) size, ptr);
#endif
  return ptr;
}


void clarisse_free(void *ptr) {
#ifdef TRACE_MEMALLOC  
  printf("Free address %p\n", ptr);
#endif
  alocated_mem_t *allocated_mem;
  mem_lock();
  HASH_FIND_PTR(allocated_mem_table, &ptr, allocated_mem);
  if (allocated_mem){
    HASH_DEL(allocated_mem_table, allocated_mem);
    mem_count.crt_allocated_memory -= (double) allocated_mem->size;
    free(allocated_mem);
  }
  mem_unlock();
  free(ptr);
}


//void *clarisse_calloc(size_t nmemb, size_t size);

void *clarisse_realloc(void *ptr, size_t size) {

  alocated_mem_t *allocated_mem;
  if (ptr) {
    mem_lock();
    HASH_FIND_PTR(allocated_mem_table, &ptr, allocated_mem);
    HASH_DEL(allocated_mem_table, allocated_mem);
    mem_unlock();
  }
  else
    allocated_mem = NULL;
  //ptr = ADIOI_Realloc(ptr, size);
  ptr = realloc(ptr, size);
#ifdef TRACE_MEMALLOC  
  printf("Realloc %d bytes address = %p\n", (int) size, ptr);
#endif
  mem_count.crt_allocated_memory = mem_count.crt_allocated_memory - allocated_mem->size + (double) size;  
  if (mem_count.crt_allocated_memory > mem_count.max_allocated_memory)
    mem_count.max_allocated_memory = mem_count.crt_allocated_memory;

  if (!allocated_mem)
    allocated_mem = (alocated_mem_t *)malloc(sizeof(alocated_mem_t));
  allocated_mem->addr = ptr; // ptr could have changed
  allocated_mem->size = size;
  mem_lock();
  HASH_ADD_PTR(allocated_mem_table, addr,  allocated_mem);
  mem_unlock();
  return ptr;
}


void clarisse_print_leaks(){
  alocated_mem_t *allocated_mem;
  if (allocated_mem_table) {
    alocated_mem_t *tmp;
    printf("FOLLOWING LEAKS WERE IDENTIFIED:\n");
    HASH_ITER(hh, allocated_mem_table, allocated_mem, tmp) {
      printf("\t Function:line %s:%d addr=%p\n", allocated_mem->function, allocated_mem->line, allocated_mem->addr);
    }
  }
}

#endif //#ifdef CLARISSE_MEM_COUNT

//#endif // #if 0


//#define clarisse_malloc ADIOI_Malloc
//#define clarisse_free ADIOI_Free
//#define clarisse_realloc ADIOI_Realloc


