#include <assert.h>
#include <stdio.h>
#include <strings.h>
#include "buffer_pool.h"
#include "error.h"
#include "util.h"
#include "mpi.h"


static buffer_unit_t * alloc_init_buffer(int buffer_size) {
  buffer_unit_t * bu = (buffer_unit_t *) clarisse_malloc(sizeof(buffer_unit_t));
  if (bu == NULL)
    handle_err(MPI_ERR_NO_MEM, "buffer_pool: clarisse_malloc error \n");
  bu->buf = (char*)clarisse_malloc(buffer_size);
  if (bu->buf == NULL)
    handle_err(MPI_ERR_NO_MEM, "buffer_pool: clarisse_malloc error \n");
  bzero(bu->buf,buffer_size);
  return bu;
}

static buffer_unit_t *  alloc_init_insert_buffer(buffer_pool_t * b) {
  buffer_unit_t * bu = alloc_init_buffer(b->buffer_size);
  dllist_iat(&b->free_buf_list,&bu->link);
  return bu;
}


buffer_pool_t * bufpool_init_static(int concurrency, int buffer_size, int nr_buffers) {
  int i, ret;
  buffer_pool_t * b = (buffer_pool_t *)clarisse_malloc(sizeof(buffer_pool_t));
  if (b == NULL)
    handle_err(MPI_ERR_NO_MEM, "buffer_pool: clarisse_malloc error\n");
  b->concurrency = concurrency;
  b->buffer_size = buffer_size;
  b->min_nr_buffers = b->nr_free_buffers 
    = b->total_nr_buffers = b->max_nr_buffers = nr_buffers;  
  b->incr_decr = 0;
  b->dyn_stat = STATIC_POOL;
  dllist_init(&b->free_buf_list);
  //    printf("buffer_pool_init: b->nr_free_buffers=%d b->buffer_size=%d\n", b->nr_free_buffers,b->buffer_size);
  for (i = 0;i<b->nr_free_buffers;i++)
    alloc_init_insert_buffer(b);
  
  if ((ret = pthread_mutex_init(&(b->bufpool_lock), NULL)) != 0)	
    fprintf(stderr,"pthread_mutex_init %s",strerror(ret)), exit(-1);	
  if ((ret = pthread_cond_init(&(b->bufpool_not_empty), NULL)) != 0)	
    fprintf(stderr,"pthread_cond_init %s",strerror(ret)), exit(-1);
  b->cnt_waiting = 0;
  return b;
}


// starts with min_nr_buffers 
// incr when nr_free_buffers == 0, and total_nr_buffers < max_nr_buffers
// decr when nr_free_buffers > min_nr_buffers 
buffer_pool_t * bufpool_init_dynamic(int concurrency, int buffer_size, int min_nr_buffers, 
			   int max_nr_buffers, int incr_decr) {
  if (min_nr_buffers > max_nr_buffers) {
    //    handle_err(MPI_ERR_OTHER, "alloc_init_buffer_pool:min_nr_buffers>max_nr_buffers");
    return NULL;
  }
  else {
    int i, ret;
    buffer_pool_t * b = (buffer_pool_t *)clarisse_malloc(sizeof(buffer_pool_t));
    if (b == NULL)
      handle_err(MPI_ERR_NO_MEM, "buffer_pool: clarisse_malloc error\n");
    b->concurrency = concurrency;
    b->buffer_size = buffer_size;
    b->min_nr_buffers = b->nr_free_buffers 
      = b->total_nr_buffers = min_nr_buffers;
    b->max_nr_buffers = max_nr_buffers;    
    b->incr_decr = incr_decr;
    b->dyn_stat = DYNAMIC_POOL;
    dllist_init(&b->free_buf_list);
    //    printf("buffer_pool_init: b->nr_free_buffers=%d b->buffer_size=%d\n", b->nr_free_buffers,b->buffer_size);
    for (i = 0;i<b->nr_free_buffers;i++)
      alloc_init_insert_buffer(b);
    if ((ret = pthread_mutex_init(&(b->bufpool_lock), NULL)) != 0)	
      fprintf(stderr,"pthread_mutex_init %s",strerror(ret)), exit(-1);	
    if ((ret = pthread_cond_init(&(b->bufpool_not_empty), NULL)) != 0)	
      fprintf(stderr,"pthread_cond_init %s",strerror(ret)), exit(-1);
    b->cnt_waiting = 0;
    return b;
  } 
}

void bufpool_free(buffer_pool_t * b){
   while (!dllist_is_empty(&b->free_buf_list)) {
     buffer_unit_t * bu = (buffer_unit_t *) dllist_rem_head(&b->free_buf_list);
     clarisse_free(bu->buf);
     clarisse_free(bu);
   }
   clarisse_free(b);
}

static int bufpool_empty_nonconcurrent(buffer_pool_t * b){
  if (b->dyn_stat == STATIC_POOL)
    return (b->nr_free_buffers == 0);
  else
    return ((b->nr_free_buffers == 0) && (b->total_nr_buffers == b->max_nr_buffers));
}

int bufpool_empty(buffer_pool_t * b){
  int empty;
  if (b->concurrency == BPOOL_CONCURRENT_ACCESS) 
    pthread_mutex_lock(&b->bufpool_lock);
  if (b->dyn_stat == STATIC_POOL)
    empty = (b->nr_free_buffers == 0);
  else
    empty = ((b->nr_free_buffers == 0) && (b->total_nr_buffers == b->max_nr_buffers));
  if (b->concurrency == BPOOL_CONCURRENT_ACCESS) 
    pthread_mutex_unlock(&b->bufpool_lock);
  return empty;
}



static buffer_unit_t *  bufpool_get_nonempty(buffer_pool_t * b) {
  buffer_unit_t * bu;
  if (b->nr_free_buffers > 0) {
    bu = (buffer_unit_t *) dllist_rem_head(&b->free_buf_list);
    b->nr_free_buffers--;
  }
  else {
    int i;
    int incr = MIN(b->incr_decr, 
		   b->max_nr_buffers - b->total_nr_buffers);
    assert(incr >= 1);
    for (i = 0; i < incr - 1; i++) 
      alloc_init_insert_buffer(b);
    b->total_nr_buffers += incr;
    b->nr_free_buffers += (incr - 1); 
    bu = alloc_init_buffer(b->buffer_size);
  }
  assert(b->total_nr_buffers >= b->min_nr_buffers);
  assert(b->total_nr_buffers <= b->max_nr_buffers);
  return bu;
}
/*
buffer_unit_t * bufpool_get(buffer_pool_t * b) {
  if (b->concurrency == BUFPOOL_CONCURRENT_ACCESS)
    
  
  if (bufpool_empty(b))
    return NULL;
  else
    return bufpool_get_nonempty(b);
}
*/
//concurrent get
buffer_unit_t * bufpool_get(buffer_pool_t * b) {
  buffer_unit_t * bu;

  if (b->concurrency == BPOOL_CONCURRENT_ACCESS) {
    pthread_mutex_lock(&b->bufpool_lock);
    b->cnt_waiting++; 
    while (bufpool_empty_nonconcurrent(b)){
      pthread_cond_wait(&(b->bufpool_not_empty),	
			&(b->bufpool_lock)); 
    }
    b->cnt_waiting--;
  }
  else
    if (bufpool_empty_nonconcurrent(b))
      return NULL;
  bu = bufpool_get_nonempty(b);
  if (b->concurrency == BPOOL_CONCURRENT_ACCESS) 
    pthread_mutex_unlock(&b->bufpool_lock);
  return bu;
}

    
void bufpool_put(buffer_pool_t * b, buffer_unit_t * bu) {
  if (b->concurrency == BPOOL_CONCURRENT_ACCESS) 
    pthread_mutex_lock(&b->bufpool_lock);

  if (b->nr_free_buffers < b->min_nr_buffers) {
    b->nr_free_buffers++;
    bzero(bu->buf, b->buffer_size);
    dllist_iat(&b->free_buf_list,&bu->link);
  }
  else {
    if (b->dyn_stat == DYNAMIC_POOL) {
      int i;
      int decr = MIN(b->incr_decr, 
		     b->total_nr_buffers - b->min_nr_buffers);
      clarisse_free(bu->buf);
      clarisse_free(bu);
      for (i = 0; i < decr - 1; i++) {
	buffer_unit_t * aux = (buffer_unit_t *) dllist_rem_head(&b->free_buf_list);
	clarisse_free(aux->buf);
	clarisse_free(aux);
      }
      b->total_nr_buffers -= decr;
      b->nr_free_buffers -= (decr - 1);
    }
  } 
  assert(b->total_nr_buffers >= b->min_nr_buffers);
  assert(b->total_nr_buffers <= b->max_nr_buffers);

  if (b->concurrency == BPOOL_CONCURRENT_ACCESS) {
    if (b->cnt_waiting > 0)
      pthread_cond_broadcast(&b->bufpool_not_empty);
    pthread_mutex_unlock(&b->bufpool_lock);
  }
}

/*
//concurrent put
buffer_unit_t * bufpool_conc_put(buffer_pool_t * b, buffer_unit_t * bu) {
  pthread_mutex_lock(&b->bufpool_lock);
  bufpool_put(b, bu);
  if (b->cnt_waiting > 0)
    pthread_cond_broadcast(&b->bufpool_not_empty);
  pthread_mutex_unlock(&b->bufpool_lock);
  return bu;
}
*/



void bu_print(buffer_unit_t * bu){
  //int i;

  printf("\n\t\tgd=%d off=%lld l_dirty=%d r_dirty=%d dirty=%hd addr=%p",bu->global_descr, bu->offset, bu->l_dirty, bu->r_dirty, bu->dirty, bu);
  //printf(" DIRTY RANGE: ");
  //for (i = bu->l_dirty; i <= bu->r_dirty; i++)
  // printf("%c",bu->buf[i]);
  //printf(" BYTES: ");
  //for (i = 0; i < server_global_state.clarisse_pars.buffer_size; i++)
    //printf("%d:%c ",i, bu->buf[i]);
    //printf("%c", bu->buf[i]);
}


static void print_free_buf_list(struct dllist *dll)
{
  struct dllist_link *aux;
  printf("Buffer list:\t");
  for(aux = dll->head; aux != NULL; aux = aux->next) {
    printf("%016llX\t", (unsigned long long int)(DLLIST_ELEMENT(aux, buffer_unit_t, link))->buf);
  }
  printf("\n");
}





void bufpool_print(buffer_pool_t * b) {
    printf("\nBUFF POOL STATE: total=%d free=%d bufsize=%d min=%d max=%d incr=%d\n",
	   b->total_nr_buffers, b->nr_free_buffers,b->buffer_size,
	   b->min_nr_buffers,b->max_nr_buffers, b->incr_decr);
    print_free_buf_list(&b->free_buf_list);
}

