#ifndef _INTERVAL_LIST_INCLUDE
#define _INTERVAL_LIST_INCLUDE
#include "list.h"

typedef struct {
  struct dllist_link link;
  long long int l;
  long long int r;
} interval_t;

void interval_list_insert(struct dllist *il, long long int l, long long int r);
void interval_list_free(struct dllist *il);
int interval_list_covers(struct dllist *il, long long int l, long long int r);
void interval_list_print(struct dllist *il);

#ifndef MAX
#define MAX(a,b) \
  ({ __typeof__ (a) _a = (a);	 \
    __typeof__ (b) _b = (b);	 \
    _a > _b ? _a : _b; })
#endif

#ifndef MIN
#define MIN(a,b)	       \
  ({ __typeof__ (a) _a = (a);	\
    __typeof__ (b) _b = (b);	\
    _a < _b ? _a : _b; })

#endif
#endif
