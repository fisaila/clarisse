#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "map.h"

/* find_bin_search test */

MPI_Datatype get_contig();
MPI_Datatype get_lb_contig(); 
MPI_Datatype get_contig_ub();
MPI_Datatype get_simple_vec();
MPI_Datatype get_displ_simple_vec();
MPI_Datatype get_simple_struct();
MPI_Datatype get_displ_displ_struct();
MPI_Datatype get_vect_struct();
MPI_Datatype get_darray();

#define MAX_CASES_PER_TEST 1024

int test_find_bin_search(char *testname, int test_cnt, MPI_Datatype *dt, ADIO_Offset *offset_input, int map_shift, int *expected_output) {
  int i, ret = 0;
  ADIOI_Flatlist_node *flat_buf = get_flatlist(*dt);
  
  assert(test_cnt <= MAX_CASES_PER_TEST);

  for (i=0; i<test_cnt; i++) {
    int output;
    if ((output = find_bin_search(offset_input[i],flat_buf, map_shift)) != expected_output[i]){
      printf("find_bin_search test %s failed on input %d with output %d (expected %d)!!!\n", testname, (int)offset_input[i], output, expected_output[i] );
      print_flatlist(*dt);
      draw_flatlist(*dt,0,1);
      ret = -1;
      break;
    }
  }
  ADIOI_Delete_flattened(*dt);
  MPI_Type_free(dt);
  return ret; 
}

int test1_1a() {
  MPI_Datatype dt = get_lb_contig();
  ADIO_Offset offset_input[]={0,1,2,3,4,5,6,7};
  int  expected_output[]=    {0,0,0,0,0,0,0,0};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search("LB CONTIG MAP_CURRENT_BLOCK",3, &dt, offset_input, map_shift, expected_output);
}

int test1_1b() {
  MPI_Datatype dt = get_lb_contig();
  ADIO_Offset offset_input[]={0,1,2,3,4,5,6,7};
  int  expected_output[]=    {0,0,0,0,1,1,1,1};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search("LB CONTIG MAP_CURRENT_BLOCK",3, &dt, offset_input, map_shift, expected_output);
}

int test1_2a() {
  MPI_Datatype dt = get_contig_ub();
  ADIO_Offset offset_input[]={0,1,2,3,4,5,6,7};
  int  expected_output[]=    {0,0,0,0,1,1,1,1};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search("LB CONTIG MAP_CURRENT_BLOCK",3, &dt, offset_input, map_shift, expected_output);
}

int test1_2b() {
  MPI_Datatype dt = get_contig_ub();
  ADIO_Offset offset_input[]={0,1,2,3,4,5,6,7};
  int  expected_output[]=    {1,1,1,1,1,1,1,1};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search("LB CONTIG MAP_NEXT_BLOCK",3, &dt, offset_input, map_shift, expected_output);
}


int test1_3a() {
  MPI_Datatype dt = get_simple_vec();
  ADIO_Offset offset_input[]={0,1,2};
  int  expected_output[]={0,1,1};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search("SIMPLE VECTOR MAP_CURRENT_BLOCK",3, &dt, offset_input, map_shift, expected_output);
}

int test1_3b() {
  MPI_Datatype dt = get_simple_vec();
  ADIO_Offset offset_input[]={0,1,2};
  int  expected_output[]={1,1,2};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search("SIMPLE VECTOR MAP_NEXT_BLOCK",3, &dt, offset_input, map_shift, expected_output);

}


// 0         1         
// 012345678901234
// ___[_*_**_*____]
//              1
//I:  012345678901 
//Oa: 000111223333
//Ob: 001122233333

int test1_4a() {
  MPI_Datatype dt = get_displ_simple_vec();
  ADIO_Offset offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11};
  int  expected_output[] =   {0,0,0,1,1,1,2,2,3,3, 3, 3};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search("DISPL SIMPLE VECT MAP_CURRENT_BLOCK",12, &dt, offset_input, map_shift, expected_output);
}

int test1_4b() {
  MPI_Datatype dt = get_displ_simple_vec();
  ADIO_Offset offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11};
  int  expected_output[] =   {0,0,1,1,2,2,2,2,3,3, 3, 3};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search("DISPL SIMPLE VECT MAP_NEXT_BLOCK",12, &dt, offset_input, map_shift, expected_output);
}

// 0         1         
// 01234567890123456789
// _______[___****____]
//                  1
//I:      012345678901 
//Oa:     000000001111
//Ob:     000011111111
int test1_5a() {
  MPI_Datatype dt = get_simple_struct();
  ADIO_Offset offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11};
  int  expected_output[] =   {0,0,0,0,0,0,0,0,1,1, 1, 1};
  int map_shift = MAP_CURRENT_BLOCK;
  return test_find_bin_search("SIMPLE STRUCT MAP_CURRENT_BLOCK",12, &dt, offset_input, map_shift, expected_output);
}

int test1_5b() {
  MPI_Datatype dt = get_simple_struct();
  ADIO_Offset offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11};
  int  expected_output[] =   {0,0,0,0,1,1,1,1,1,1, 1, 1};
  int map_shift = MAP_NEXT_BLOCK;
  return test_find_bin_search("SIMPLE STRUCT MAP_NEXT_BLOCK",12, &dt, offset_input, map_shift, expected_output);
}




int main(int argc, char* argv[]) {
    MPI_Info info;
    int ret;
    MPI_Init(&argc, &argv);

    //Fake deletion to initialize ADIO
    MPI_Info_create(&info);
    MPI_File_delete("/tmp/a", info);

    ret = test1_1a();
    if (ret >= 0) ret = test1_1b();
    if (ret >= 0) ret = test1_2a();
    if (ret >= 0) ret = test1_2b();
    if (ret >= 0) ret = test1_3a();
    if (ret >= 0) ret = test1_3b();
    if (ret >= 0) ret = test1_4a();
    if (ret >= 0) ret = test1_4b();
    if (ret >= 0) ret = test1_5a();
    if (ret >= 0) ret = test1_5b();


    /*
    printf("\nDRAW SIMPLE STRUCT:\n");
    draw_type(get_simple_struct(),10,2);
    printf("\nDRAW DISPL DISPL STRUCT:\n");
    draw_type(get_displ_displ_struct(),0,1);
    printf("\nDRAW STRUCT OF VECTOR OF STRUCT:\n");
    draw_type(get_vect_struct(),0,1);
			 */

    /*
    init_distrib(&d, 10, column_mpi_t);
      
    for(offs=0;offs<40;offs++)
      printf("Map_1(offs=%lld) = %lld %lld\n",offs,func_1(offs,dt, get_flatlist(dt), 10), find_size_bin_search(offs, get_flatlist(dt)));

    */
    MPI_Info_free(&info);
    MPI_Finalize();
    return ret;
}  // main 
