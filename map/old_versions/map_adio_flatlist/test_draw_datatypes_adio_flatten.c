#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "map.h"

MPI_Datatype get_contig();
MPI_Datatype get_simple_vec();
MPI_Datatype get_displ_simple_vec();
MPI_Datatype get_simple_struct();
MPI_Datatype get_displ_displ_struct();
MPI_Datatype get_vect_struct();
MPI_Datatype get_darray();

void draw_type(MPI_Datatype dt, int displ, int repet) {
  MPI_Aint extent, lb;
  MPI_Type_get_extent(dt, &lb,&extent);
  printf("LB=%d EXTENT=%d\n", (int)lb, (int)extent);
  print_flatlist(dt);
  draw_flatlist(dt,0,1);
  ADIOI_Delete_flattened(dt);
  MPI_Type_free(&dt);
}

int main(int argc, char* argv[]) {
    MPI_Info info;

    MPI_Init(&argc, &argv);

    //Fake deletion to initialize ADIO
    MPI_Info_create(&info);
    MPI_File_delete("/tmp/a", info);

    printf("\nDRAW CONTIG:\n");
    draw_type(get_contig(),0,1);

    printf("\nDRAW SIMPLE VECTOR:\n");
    draw_type(get_simple_vec(),0,1);

    printf("\nDRAW DISPL SIMPLE VECTOR (between lb and ub):\n");
    draw_type(get_displ_simple_vec(),0,1);

    printf("\nDRAW SIMPLE STRUCT:\n");
    draw_type(get_simple_struct(),10,2);

    printf("\nDRAW DISPL DISPL STRUCT:\n");
    draw_type(get_displ_displ_struct(),0,1);

    printf("\nDRAW STRUCT OF VECTOR OF STRUCT:\n");
    draw_type(get_vect_struct(),0,1);
    
    MPI_Info_free(&info);
    MPI_Finalize();
    return 0;
}  // main 
