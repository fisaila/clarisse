
// File model for mapping:
// The asumption is that the file data type repetition is between lb and ub, 
// i.e the period is the extent for the exemple from below the repetition 
// starts at displ + lb = 1 + 4 = 5 
//
//        extent=6 
//disp=1 lb=4 up=11
//       ------
//   |   |    |
//   v   v    v
//  _____***___***___***___
//  0123456790123456790123456790
//
// Assumption: the first element of flat list contains always the indices = lb+displ


#include <assert.h>
#include "adio.h"
#include "adio_extern.h"
#include "map_adio_flatlist.h"

extern ADIOI_Flatlist_node *ADIOI_Flatlist;

void handle_error(int errcode, char *str) 
{
    char msg[MPI_MAX_ERROR_STRING];
    char   processor_name[MPI_MAX_PROCESSOR_NAME];
    int namelen,resultlen,my_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Get_processor_name(processor_name,&namelen);
    MPI_Error_string(errcode, msg, &resultlen);
    fprintf(stderr,"Process %d on %s %s: %s\n", my_rank, processor_name,str, msg);
    MPI_Abort(MPI_COMM_WORLD, 1);
}


ADIOI_Flatlist_node * get_flatlist(MPI_Datatype dt){
  int is_contig;
  
  ADIOI_Datatype_iscontig(dt,&is_contig);
  if (is_contig)
    return NULL;
  else {
    ADIOI_Flatlist_node *flat_buf;
    ADIOI_Flatten_datatype(dt);
    flat_buf = ADIOI_Flatlist;
    while ((flat_buf)&&(flat_buf->type != dt)) flat_buf = flat_buf->next;    
    if (flat_buf) {
      if (flat_buf->incr_size[0] == -1) {
	int i;
	flat_buf->incr_size[0] = 0;
	for (i=1;i<flat_buf->count;i++)
	  flat_buf->incr_size[i] = flat_buf->incr_size[i-1] + flat_buf->blocklens[i-1]; 
      }
    }
    else
      handle_error(MPI_ERR_OTHER,"data type not flatten\n");
    return flat_buf;
  }
}

void init_distrib(distrib_p d, ADIO_Offset disp, MPI_Datatype dt) {
  MPI_Aint lb;
  MPI_Type_lb(dt,&lb);
  d->disp = disp + lb;
  d->filetype = dt;
  d->flat_buf = get_flatlist(dt);
}

distrib_p alloc_init_distrib(ADIO_Offset disp, MPI_Datatype dt) {
  distrib_p d = (distrib_p)clarisse_malloc(sizeof(distrib));
  init_distrib(d,disp,dt );
  return d;
}

void update_distrib(distrib_p d, ADIO_Offset disp, MPI_Datatype dt) {
  // ?? should the old flat_buf be freed?
  init_distrib(d, disp, dt );
}

void free_distrib(distrib_p d ) {

  ADIOI_Delete_flattened(d->filetype);
  clarisse_free(d);
}

void print_flatlist(MPI_Datatype dt) {
  int b_index;
  ADIOI_Flatlist_node *flat_buf = get_flatlist(dt);
  if (flat_buf) {
    //printf("FLATLIST:\n");
    for (b_index=0; b_index < flat_buf->count; b_index++) {
      printf ("%3d-th: offset=%8lld len=%8lld incr_size=%8d\n",b_index, flat_buf->indices[b_index],(long long int)flat_buf->blocklens[b_index],flat_buf->incr_size[b_index] ); 
    }
  }
  else
    printf ("NULL flat_buf\n");

}


/* New function in ../test/flatten/datatypes.c */
void draw_flatlist(MPI_Datatype dt,  ADIO_Offset disp, int n) {
  int i,k,j;
  ADIOI_Flatlist_node *flat_buf;
  int line=100, idx=0, maxdraw=5000;

  printf("\n");
  for (i=0;i<line;i++)
    if (!(i%10))
      printf("%d",i/10);
    else
      printf(" ");
  printf("\n");
  for (i=0;i< line ;i++)
    printf("%d",i%10);
  printf("\n");

  flat_buf = get_flatlist(dt);

  if (!flat_buf) {
      printf("Contiguous type: not drawn \n");
      return;
  }
  for (i=0; i < disp + flat_buf->indices[0]; i++) { 
      printf("_");
      if (!((++idx)%line)) { printf("\n"); }
      if (idx == maxdraw) return;
      //      if ((++idx)==line) { printf("\n"); idx=0; }
  }
  for (j=0;j<n;j++)
    for (i=0; i < flat_buf->count; i++) {
	for (k=0;k < flat_buf->blocklens[i]; k++) {
	    printf("*");
	    if (!((++idx)%line)) { printf("\n"); }
	    if (idx == maxdraw) return;
	}
	if (i < flat_buf->count - 1)
	    for (k=0;k < flat_buf->indices[i+1] - flat_buf->indices[i] - flat_buf->blocklens[i] ; k++)  {
		printf("_");
		//		if ((++idx)==line) { printf("\n"); idx=0; }
		if (!((++idx)%line)) { printf("\n"); }
		if (idx == maxdraw) return;
	    }
    }
   
    //}
  printf("\n");
  //  l = n* (flat_buf->indices[flat_buf->count-1]+flat_buf->blocklens[flat_buf->count-1] - flat_buf->indices[0]) + disp +flat_buf->indices[0];
  //  printf("l =%d \n",line);

}



int cmp_flatlists(MPI_Datatype dt1,MPI_Datatype dt2) {
  int b_index;
  ADIOI_Flatlist_node *flat_buf1,*flat_buf2;
  flat_buf1 = ADIOI_Flatlist;
  while ((flat_buf1)&&(flat_buf1->type != dt1)&&(flat_buf1->type != dt2))
    flat_buf1 = flat_buf1->next;

  if (flat_buf1 == NULL)
    handle_error(MPI_ERR_LASTCODE,"None of 2 datatypes was flattened\n");
  flat_buf2 = flat_buf1;
  if (flat_buf2->type == dt1) {
    while ((flat_buf2)&&(flat_buf2->type != dt2)) flat_buf2 = flat_buf2->next;
    if (flat_buf2 == NULL)
      handle_error(MPI_ERR_LASTCODE,"2nd datatype was not flattened\n");
  }
  else {
    while ((flat_buf2)&&(flat_buf2->type != dt1)) flat_buf2 = flat_buf2->next;
    if (flat_buf2 == NULL)
      handle_error(MPI_ERR_LASTCODE,"1st datatypes was not flattened\n");
  }
  if (flat_buf1->count != flat_buf2->count)
     handle_error(MPI_ERR_LASTCODE,"cmp_flatlists:Different number of flattened elements\n");
  for (b_index=0; b_index < flat_buf1->count; b_index++) 
    if ((flat_buf1->indices[b_index] != flat_buf2->indices[b_index])||
	(flat_buf1->blocklens[b_index] != flat_buf2->blocklens[b_index]))
      handle_error(MPI_ERR_LASTCODE,"cmp_flatlists:Different number of indices or blocklens\n");
  return 0;
}


void print_dt(MPI_Datatype type) {
  static int nest=0;
  int ni, na, nd, combiner, *i,k; 
  MPI_Aint *a; 
  MPI_Datatype *d; 

  MPI_Type_get_envelope(type, &ni, &na, &nd, &combiner); 
  if (combiner != MPI_COMBINER_NAMED){
    if (ni > 0) i = (int *) clarisse_malloc(ni*sizeof(int));
    if (na > 0) a = (MPI_Aint *) clarisse_malloc(na*sizeof(MPI_Aint));
    if (nd > 0) d = (MPI_Datatype *) clarisse_malloc(nd*sizeof(MPI_Datatype));
    MPI_Type_get_contents(type, ni, na, nd, i, a, d); 
  }
  
  for (k=0;k<nest;k++) printf("  ");

  switch (combiner) {
  case  MPI_COMBINER_NAMED:
    printf("NAMED DTYPE=%x\n",type);
    break;
  case MPI_COMBINER_DUP:
    printf("DUP DTYPE\n");
    break;
  case MPI_COMBINER_CONTIGUOUS: 
    printf("CONTIGUOUS DTYPE count=%d\n",i[0]);
    break;
  case MPI_COMBINER_VECTOR:
    printf("VECTOR DTYPE count=%d blocklen=%d stride=%d \n",i[0],i[1],i[2]);
    break;
  case MPI_COMBINER_HVECTOR_INTEGER:
    printf("HVECTOR_INTEGER DTYPE count=%d blocklen=%d stride=%lld \n",i[0],i[1],(long long int)a[0]);
    break;
  case MPI_COMBINER_HVECTOR: 
    printf("HVECTOR DTYPE count=%d blocklen=%d stride=%lld \n",i[0],i[1],(long long int)a[0]);
    break;
  case MPI_COMBINER_INDEXED:
    printf("INDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%d ",k,i[1+k],i[1+i[0]+k]); 
    printf("\n");
    break;
  case MPI_COMBINER_HINDEXED_INTEGER:
    printf("HINDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%lld ",k,i[1+k],(long long int)a[k]); 
    printf("\n");
    break;
    
  case MPI_COMBINER_HINDEXED:
    printf("HINDEXED DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%lld ",k,i[1+k],(long long int)a[k]); 
    printf("\n");
    break;
  case MPI_COMBINER_INDEXED_BLOCK:
    printf("HINDEXED_BLOCK DTYPE count=%d blocklen=%d ",i[0],i[1]);
    for (k=0;k<i[0];k++) 
      printf("[%d]disp=%d ",k,i[2+k]); 
    printf("\n");
    break;
  case MPI_COMBINER_STRUCT_INTEGER:
  case MPI_COMBINER_STRUCT:
    printf("STRUCT DTYPE count=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]blocklen=%d disp=%lld ",k,i[1+k],(long long int)a[k]); 
    printf("\n");
    break;
  case MPI_COMBINER_SUBARRAY:
    printf("SUBARRAY DTYPE ndims=%d ",i[0]);
    for (k=0;k<i[0];k++) 
      printf("[%d]size=%d subsize=%d start=%d",k,i[1+k],i[1+i[0]+k],i[1+2*i[0]+k]); 
 
    printf("order=%d\n",i[1+3*i[0]]);
    break;
  case MPI_COMBINER_DARRAY:
    printf("DARRAY DTYPE size=%d rank=%d ndims=%d ",i[0],i[1],i[2]);
    for (k=0;k<i[0];k++) 
      printf("[%d]gsize=%d distrib=%d darg=%d psize=%d",k,i[3+k],i[3+i[0]+k],i[3+2*i[0]+k],i[3+3*i[0]+k] ); 
 
    printf("order=%d\n",i[3+4*i[0]]);
    break;
  case MPI_COMBINER_RESIZED:
     printf("RESIZED DTYPE lb=%d, extent=%d\n",i[0],i[1]);
     break;
  default:
    handle_error(MPI_ERR_LASTCODE,"Error: unkonwn combiner\n");
  }
  nest++;
  for (k=0;k<nd;k++)
    print_dt(d[k]);
  nest--;
  //  printf("DTYPE=%x\n",type);

  if (ni > 0) clarisse_free(i);  
  if (na > 0) clarisse_free(a);  
  if (nd > 0) clarisse_free(d);
  
}

// assumes : x >= 0 &&  x < flat_buf->indices[m] - flat_buf->indices[0] + flat_buf->blocklens[m]
// i.e. mapping is always between lb and ub, relatively to lb (from 0 to ub-lb)
// result:  between 0 and flat_buf->count inclusively
// returns the index of the flat_buf where x is located
//  MAP_CURRENT_BLOCK : when mapping on a block return the index of current block, o/w of the next block 
//  ____***____***____****__
//  000000011111112222222233
//  MAP_NEXT_BLOCK :  always return the index of next block
//  000011111112222222333333 
int find_bin_search(ADIO_Offset x, ADIOI_Flatlist_node *flat_buf, int map_shift){

    assert(x >= 0);
    assert(x < flat_buf->indices[flat_buf->count - 1] - flat_buf->indices[0] + flat_buf->blocklens[flat_buf->count - 1]);

    int l = 0 ,r = flat_buf->count - 1 ,found = -1;

    assert(l<=r);
    while (l <= r) {
	int m = (l + r) / 2;
	if (x < flat_buf->indices[m] - flat_buf->indices[0] + flat_buf->blocklens[m]) {
	    if (l == m) {
	      found = m;
	      if (flat_buf->blocklens[0] == 0) printf("l==m and flat_buf->blocklens[0] == 0\n");
	      break;
	    }
	    if (x >= flat_buf->indices[m - 1] - flat_buf->indices[0] + flat_buf->blocklens[m - 1]) {
	      // If the first element is lb substract one 
	      if (flat_buf->blocklens[0] == 0)
		found = m - 1;
	      else
		found = m ;
	      break;
	    }
	    r = m - 1 ;
	}
	else {
	    if (m == r) {
	      found = m +1;
	      if (flat_buf->blocklens[0] == 0) printf("r==m and flat_buf->blocklens[0] == 0\n");
	      break;
	    }
	    if (x < flat_buf->indices[m + 1] - flat_buf->indices[0] + flat_buf->blocklens[m + 1]) {
	      // If the first element is lb substract one 
	      if (flat_buf->blocklens[0] == 0)
		found = m;
	      else
		found = m + 1 ;
	      break;
	    }
	    l = m +1;
	}
    }
    if ((map_shift == MAP_NEXT_BLOCK) 
	&& (found < flat_buf->count)
	&& (x < flat_buf->indices[found] - flat_buf->indices[0] + flat_buf->blocklens[found]) 
	&& (x >= flat_buf->indices[found] - flat_buf->indices[0])) found++;
    return found;
}

// Finds the file offset mapping from a view offset x) within the extent
// returns file offset
//    
// assumes : x >= 0 && x < flat_buf->incr_size[flat_buf->count - 1] 
int find_size_bin_search(ADIO_Offset x, ADIOI_Flatlist_node *flat_buf){
  
  assert (x >= 0);
  assert (x < flat_buf->incr_size[flat_buf->count - 1]);
  int l = 0, r = flat_buf->count - 1;
  int size;

  assert(l<=r);

  while (l <= r) {
    int m = (l + r) / 2;
    //printf("\n*** l=%d r=%d\n",l,r);
    if (x < flat_buf->incr_size[m] + flat_buf->blocklens[m]) {
	if ((l == m) || (x >= flat_buf->incr_size[m])) {
	size = flat_buf->indices[m] + x - flat_buf->incr_size[m];
	break;
      }
      r = m - 1 ;
    }
    else {
	 // ???
	if (m == r) {
	    size = flat_buf->indices[m] + x - flat_buf->incr_size[m];
	    break;
	}
	if (x < flat_buf->incr_size[m + 1] + flat_buf->blocklens[m + 1]) {
	    size = flat_buf->indices[m + 1] + x - flat_buf->incr_size[m + 1];
	    break;
	}
	l = m +1;
    }
  }
  return size;
}

/*  Maps a file offset onto a view offset within a period (i.e. an extent)
    Assumes x >= 0 && x < flat_buf->indices[m] - flat_buf->indices[0] + flat_buf->blocklens[m]
 */
// lb is included in flat_buf, even though is not considered 
// in the repetitive pattern of the view. that is why it has to be substracted
// we add lb==flat_buf->indices[0] to the displacement in the distrib struct
ADIO_Offset func_sup_per( ADIO_Offset x, ADIOI_Flatlist_node *flat_buf){
  assert(x >= 0);
  assert(x < flat_buf->indices[flat_buf->count - 1] - flat_buf->indices[0] + flat_buf->blocklens[flat_buf->count - 1]);
  int i = find_bin_search(x, flat_buf, MAP_CURRENT_BLOCK);

  if (i == flat_buf->count)
    return flat_buf->incr_size[i-1] +  flat_buf->blocklens[i-1];
  
  // substract flat_buf->indices[0] because all indices include lb and displ 
  // I got i, but because mapped on a hole and shifted to the right i can get the 
  // whole incremental size
  if ( x <= flat_buf->indices[i] - flat_buf->indices[0])
    return flat_buf->incr_size[i];
  else
    return flat_buf->incr_size[i] + x - flat_buf->indices[i] + flat_buf->indices[0];
} 

/* Maps a file offset onto a view offset (in bytes)  
   if the mapping falls on a hole, moves to the write
*/
ADIO_Offset func_sup( ADIO_Offset x,distrib_p d){
  assert(x >= 0);
  if (x < d->disp) return 0;
  else {
    int is_contig;
    ADIOI_Datatype_iscontig(d->filetype, &is_contig);
    if (is_contig) return x - d->disp;
    else {
      MPI_Aint extent, lb;
      int sz;
      MPI_Type_get_extent(d->filetype, &lb, &extent);
      assert(extent == d->flat_buf->indices[d->flat_buf->count-1] - d->flat_buf->indices[0] + d->flat_buf->blocklens[d->flat_buf->count-1]);
      MPI_Type_size(d->filetype, &sz);
      assert(sz == d->flat_buf->incr_size[d->flat_buf->count -1]);
      return (x - d->disp) / extent * sz + func_sup_per((x - d->disp) % extent, d->flat_buf);
    }
  }
}

/*  Maps a file offset onto a view offset within a period (i.e. an extent)
    Assumes x >= 0 && x < flat_buf->indices[m] - flat_buf->indices[0] + flat_buf->blocklens[m]
 */
ADIO_Offset func_inf_per( ADIO_Offset x,ADIOI_Flatlist_node *flat_buf){
  assert(x >= 0);
  assert(x < flat_buf->indices[flat_buf->count - 1] - flat_buf->indices[0] + flat_buf->blocklens[flat_buf->count - 1]);
  int i = find_bin_search(x,flat_buf, MAP_CURRENT_BLOCK);
  if (i == flat_buf->count)
    return flat_buf->incr_size[i-1] +  flat_buf->blocklens[i-1]  - 1;
  if ( x < flat_buf->indices[i] - flat_buf->indices[0])
    return flat_buf->incr_size[i] - 1;
  else
    return flat_buf->incr_size[i] + x - flat_buf->indices[i] + flat_buf->indices[0];
} 

/* Maps a file offset onto a view offset (in bytes)  
   if the mapping falls on a hole, moves to the left
   Returns -1 if it falls to the left of first byte in the view.
*/
ADIO_Offset func_inf( ADIO_Offset x,distrib_p d){
  assert(x >= 0);
  if (x < d->disp) return -1;
  else {
    int is_contig;
    ADIOI_Datatype_iscontig(d->filetype, &is_contig);
    if (is_contig) return x - d->disp;
    else {
      MPI_Aint extent, lb;
      int sz;
      MPI_Type_get_extent(d->filetype, &lb, &extent);
      assert(extent == d->flat_buf->indices[d->flat_buf->count-1] - d->flat_buf->indices[0] + d->flat_buf->blocklens[d->flat_buf->count-1]);
      MPI_Type_size(d->filetype, &sz);
      return (x - d->disp) / extent * sz + func_inf_per((x - d->disp) % extent, d->flat_buf);
    }
  }
}

/*  Maps a view offset onto a file offset within a period (i.e. an extent)
    Assumes x >= 0 && x < flat_buf->incr_size[flat_buf->count - 1]
 */

ADIO_Offset func_1_per( ADIO_Offset x, ADIOI_Flatlist_node *flat_buf){
  assert (x >= 0);
  assert (x < flat_buf->incr_size[flat_buf->count -1]);
  
  return  find_size_bin_search(x, flat_buf) - flat_buf->indices[0];
}

/* Maps a view offset on a file offset 
x in in bytes  
*/

ADIO_Offset func_1( ADIO_Offset x,distrib_p d){
  int is_contig;
  ADIOI_Datatype_iscontig(d->filetype, &is_contig);
  if (is_contig) 
    return x + d->disp;
  else {
    MPI_Aint extent, lb;
    int sz;
    
    MPI_Type_get_extent(d->filetype, &lb, &extent);
    assert(extent == d->flat_buf->indices[d->flat_buf->count-1] - d->flat_buf->indices[0] + d->flat_buf->blocklens[d->flat_buf->count-1]);

    MPI_Type_size(d->filetype, &sz);
    assert(sz == d->flat_buf->incr_size[d->flat_buf->count -1]);

    return d->disp + x / sz * extent + func_1_per(x % sz, d->flat_buf);
  }
}


ADIO_Offset get_count(ADIO_Offset l, ADIO_Offset r, distrib_p d) {
  if (l > r) return 0;
  if (d->flat_buf) 
    return func_inf(r, d) - func_sup(l,d) + 1;
  else 
    return r - l + 1;
}


/* This version compacts the neighboring contiguous blocks */ 
int count_contiguous_blocks_memory(MPI_Datatype datatype, int count) {
    ADIOI_Flatlist_node *flat_buf;
    flat_buf = get_flatlist(datatype);
    return (flat_buf)?(flat_buf->count*count):1;
}

#define BEGIN_BLOCK(x,block_size) (((x)/(block_size))*(block_size))
#define END_BLOCK(x,block_size) (((x)/(block_size)+1)*(block_size)-1)
#define BEGIN_NEXT_BLOCK(x,block_size) (((x)/(block_size)+1)*(block_size))


void free_datatype(MPI_Datatype type) {
    int ni, na, nd, combiner; 
    MPI_Type_get_envelope(type, &ni, &na, &nd, &combiner); 
    if (combiner != MPI_COMBINER_NAMED)
	MPI_Type_free(&type);
}


int count_contiguous_blocks_file(MPI_File fh, MPI_Offset foff1, MPI_Offset foff2) {
    MPI_Offset disp;
    MPI_Datatype etype, filetype;
    char datarep[MPI_MAX_DATAREP_STRING];
    int is_contig;
    int ret;

    assert(foff1 <= foff2);
     
    MPI_File_get_view(fh, &disp, &etype, &filetype, datarep);
    //my_get_view(fh, &disp, &etype, &filetype);
    ADIOI_Datatype_iscontig(filetype, &is_contig);
    if (is_contig)
	ret=1;
    else { 
	MPI_Aint extent, lb;
	ADIOI_Flatlist_node *flat_buf;
	flat_buf = get_flatlist(filetype);
	MPI_Type_get_extent(filetype, &lb, &extent);
	assert( extent == flat_buf->indices[flat_buf->count-1] - flat_buf->indices[0] + flat_buf->blocklens[flat_buf->count-1] );

	if (disp+lb > foff1) {
		printf("disp=%lld lb=%lld foff1=%lld foff2=%lld\n", disp, (long long) lb, foff1, foff2);
		print_flatlist(filetype);		
	}	
	assert(disp+lb <= foff1);
	assert(lb == flat_buf->indices[0]);

	if (foff2 < ((flat_buf->blocklens[0] == 0)?flat_buf->indices[1]:flat_buf->indices[0]))
	    ret=0;
	else{
	    int ind1, ind2;
	    ind1 = find_bin_search((foff1 - disp - lb) % extent, flat_buf, MAP_CURRENT_BLOCK);
	    ind2 = find_bin_search((foff2 - disp - lb) % extent, flat_buf, MAP_NEXT_BLOCK);
	    
	    if ((foff2 - disp - lb) < BEGIN_NEXT_BLOCK(foff1 - disp - lb, extent)) 
		ret = ind2 - ind1;
	    else 
		ret = flat_buf->count - ind1 - ((flat_buf->blocklens[flat_buf->count-1] == 0)?1:0)+
		    (BEGIN_BLOCK(foff2 - disp - lb, extent)-
		     BEGIN_NEXT_BLOCK(foff1 - disp - lb, extent)) 
		    / extent * (flat_buf->count-
				((flat_buf->blocklens[0] == 0)?1:0) -
				((flat_buf->blocklens[flat_buf->count-1] == 0)?1:0)-
				(((flat_buf->blocklens[flat_buf->count-1])&&(flat_buf->blocklens[flat_buf->count-1] + flat_buf->indices[flat_buf->count-1] - flat_buf->indices[0] == extent ))?1:0))
		    + ind2 - (((ind2>0)&&(flat_buf->blocklens[0] == 0))?1:0) -  
		    (((ind2>0)&&(flat_buf->indices[0]==0))?1:0); 
	}
    }
    free_datatype(etype);
    free_datatype(filetype);
    return ret;
}
