#include "adio.h"
#include "adio_extern.h"

static int sg_extent_mem_old (char *buf, ADIO_Offset l, ADIO_Offset r,  ADIOI_Flatlist_node *flat_buf, char*out_buf, int type) {
  
  int i = (l) ? find_bin_search(l, flat_buf) : 0;
  int cnt = 0;
  if (i < flat_buf->count ) {
      l = ADIOI_MAX (l, flat_buf->indices[i] - flat_buf->indices[0]);

      while (l <= r) {
	  ADIO_Offset crt_r = ADIOI_MIN (r, flat_buf->indices[i] + flat_buf->blocklens[i] - flat_buf->indices[0] - 1);
	  switch (type) {
	      case SCATTER:
		  memcpy(buf + l, out_buf + cnt,  crt_r - l + 1);
		  break;
	      case GATHER:
		  memcpy(out_buf + cnt, buf + l, crt_r - l + 1);
		  break;
	      case SCATTER_NOCOPY:
	      case GATHER_NOCOPY:
		  update_net_dt((struct net_dt *) out_buf, buf+l, crt_r - l + 1);
		  break;
	  }

	  cnt += (crt_r - l + 1);
	  if ((++i) < flat_buf->count )
	      l =  flat_buf->indices[i]  - flat_buf->indices[0];
	  else
	      break;
      }
  }
  return cnt;
}

// type 0: scatter 1: gather
// l,r : absolute positions in the buffer buf
// has to be called with the buffer aligned to l
// 1)in client where the view is formed from n MPI dtypes aligned to the start of buffer :  buf + l / extent * extent
// 2) in IOS where the buffer represents a position in the file (the filetype is not aligned to the buffer: page - l % extent

int sg_mem_old (char *buf, ADIO_Offset l, ADIO_Offset r, distrib_p d, char*out_buf, int type) {
  int cnt = 0;
  if (d->flat_buf) {
    MPI_Aint extent;
    ADIOI_Flatlist_node *flat_buf = d->flat_buf;
    int buf_disp = 0;

    if (l > r) return 0;
    if (r < d->disp) return 0;
    MPI_Type_extent(d->filetype, &extent);
    
    if (l < d->disp) {
      l = 0;
      buf_disp = flat_buf->indices[0] - l;
    }
    else {
      l -= d->disp;
      //buf_disp = - l % extent;
      //buf_disp = l ; /// extent * extent; // added for making l, r absolute 
    }
    r -= d->disp;
    while (l <= r) {
      ADIO_Offset crt_r = (r < END_BLOCK(l,extent))? (r % extent) : extent - 1; 
      if ((type == SCATTER) || (type == GATHER))
	  cnt += sg_extent_mem_old(buf + buf_disp, l % extent, crt_r, flat_buf, out_buf + cnt, type);
      else  // in this case the buffer contains the net_dt
	  cnt += sg_extent_mem_old(buf + buf_disp, l % extent, crt_r, flat_buf, out_buf, type);
 
      //buf_disp += (crt_r - l % extent  + 1);
      buf_disp += extent;
      l = BEGIN_NEXT_BLOCK(l,extent);
    }
  }
  else {
    cnt =  r - l + 1;
    switch (type) {
	case SCATTER:
	    memcpy(buf , out_buf, cnt);
	    break;
	case GATHER:
	    memcpy(out_buf, buf , cnt);
	    break;
	case SCATTER_NOCOPY:
	case GATHER_NOCOPY:
	    update_net_dt((struct net_dt *)out_buf,buf, cnt);
	    break;
    }
  }
  return cnt;
}

// type 0: scatter 1: gather
// l,r : absolute positions from the beginning of buffer buf

int sg_mem (char *buf, ADIO_Offset l, ADIO_Offset r, distrib_p d, char*out_buf, int type) {
  int cnt = 0;
  if (d->flat_buf) {
      MPI_Aint extent;
      ADIOI_Flatlist_node *flat_buf = d->flat_buf;

      if (l > r) return 0;
      if (r < d->disp) return 0;
      l = (l < d->disp) ? 0 : l - d->disp;
      r -= d->disp;

      MPI_Type_extent(d->filetype, &extent);
      while (l <= r) {
	  int i;
	  i = (l % extent) ? find_bin_search(l % extent, flat_buf) : 0;
	  if (i < flat_buf->count ) {
	      ADIO_Offset rel_l = ADIOI_MAX (l % extent, flat_buf->indices[i] - flat_buf->indices[0]); 
	      ADIO_Offset rel_r = ADIOI_MIN(r , END_BLOCK(l,extent)) % extent;

	      while (rel_l <= rel_r) {
		  ADIO_Offset crt_r = ADIOI_MIN (rel_r, flat_buf->indices[i] + flat_buf->blocklens[i] - flat_buf->indices[0] - 1); //
		  switch (type) {
		      case SCATTER:
                          //printf ("d->disp %lld + BEGIN_BLOCK(l,extent) %lld + rel_l %lld  l %lld\n",d->disp,  BEGIN_BLOCK(l,extent), rel_l,l);
			  memcpy(buf + d->disp + BEGIN_BLOCK(l,extent) + rel_l, out_buf + cnt,  crt_r - rel_l + 1);
			  break;
		      case GATHER:
			  memcpy(out_buf + cnt, buf + d->disp + BEGIN_BLOCK(l,extent) + rel_l, crt_r - rel_l + 1);
			  break;
		      case SCATTER_NOCOPY:
		      case GATHER_NOCOPY:
			  update_net_dt((struct net_dt *) out_buf, buf + d->disp + BEGIN_BLOCK(l,extent) + rel_l, crt_r - rel_l + 1);
			  break;
		  }

		  cnt += (crt_r - rel_l + 1);
		  if ((++i) < flat_buf->count )
		      rel_l =  flat_buf->indices[i] - flat_buf->indices[0];
		  else
		      break;
	      }
	  }
	  l = BEGIN_NEXT_BLOCK(l,extent);
      }
  }
  else {
    cnt =  r - l + 1;
    //printf (" d->disp %lld d->flat_buf %d l %lld  r %lld cnt %d\n", d->disp,d->flat_buf,l,r,cnt);
    switch (type) {
	case SCATTER:
	    //memcpy(buf+l, out_buf, cnt);
	    memcpy(buf, out_buf, cnt);
	    break;
	case GATHER:
	    memcpy(out_buf, buf+l, cnt);
	    break;
	case SCATTER_NOCOPY:
	case GATHER_NOCOPY:
	    update_net_dt((struct net_dt *)out_buf,buf+l, cnt);
	    break;
    }
  }
  return cnt;
}



static int sg_extent_file (ADIO_File fd, ADIO_Offset file_disp, ADIO_Offset l, ADIO_Offset r,  ADIOI_Flatlist_node *flat_buf, char*out_buf, int type) {
  
  int i = (l) ? find_bin_search(l, flat_buf) : 0;
  int cnt = 0;

  l = ADIOI_MAX (l, flat_buf->indices[i] - flat_buf->indices[0]);

  while ((i < flat_buf->count ) && (l <= r)) {
    ADIO_Offset crt_r = ADIOI_MIN (r, flat_buf->indices[i] + flat_buf->blocklens[i] - flat_buf->indices[0] - 1);
    
    if (type) {
 
      if (readn_at(fd, out_buf + cnt, crt_r - l + 1, l + file_disp) != crt_r - l + 1)
	handle_error(MPI_ERR_OTHER, "read failed in sg_extent_file\n");
    }
    else

      if (writen_at(fd, out_buf + cnt, crt_r - l + 1, l + file_disp) != crt_r - l + 1)
    	   handle_error(MPI_ERR_OTHER, "read failed in sg_extent_file\n");
      cnt += (crt_r - l + 1);
      l =  flat_buf->indices[++i]  - flat_buf->indices[0];
    }
    return cnt;
}


int writen(ADIO_File fd, void *vptr, int n)
{
  size_t		nleft;
  ssize_t		nwritten;
  const char	*ptr;
  ADIO_Status *status;
  int err;
    
  ptr = vptr;
  nleft = n;
  //printf ("writen:  nelem %d\n",n); 
  while (nleft > 0) {
    ADIO_WriteContig(fd,(char *)ptr,nleft,MPI_CHAR,ADIO_EXPLICIT_OFFSET,0,status,&err);
    MPI_Get_count( status, MPI_CHAR, &nwritten);
	if (err == EINTR) {
	    fprintf(stderr, "writen EINTR\n");
	    nwritten = 0;		/* and call write() again */
	}
      else
		return(-1);			/* error */
    
    nleft -= nwritten;
    ptr   += nwritten;
  }
//    printf ("write n %d\n",n);
  return n;
}

/* end writen */
int writen_at(ADIO_File fd, void *vptr, int n, ADIO_Offset offset)
{
    int m, a ,b ,c;
    char err_msg[128];
    ADIO_Status status;
    int err;
   
    //    printf ("writen_at: n %d offset %lld \n",n,offset); 
    ADIO_WriteContig(fd,vptr,n,MPI_CHAR, ADIO_EXPLICIT_OFFSET,offset,&status,&err);
    MPI_Get_count( &status,MPI_CHAR, &m);
    if (m < n) 
		sprintf(err_msg, "Writen_at: Error at writing to file write %d instead of %d", m,n);
    return n;
}

int readn(ADIO_File fd, void *vptr, size_t n)
{
    size_t	nleft;
    size_t	nread;
    char	*ptr;
    ADIO_Status *status;
    int err;
    
    ptr = vptr;
    nleft = n;
    //printf ("readn: n %d\n",n);
    while (nleft > 0) {
    	
    	ADIO_ReadContig(fd,ptr,nleft,fd->etype,ADIO_EXPLICIT_OFFSET,0,status,&err);
    	MPI_Get_count( status, fd->filetype, &nread);
		if (err != MPI_SUCCESS) {
	    	if (errno == EINTR){
				fprintf(stderr, "READn EINTR\n");
				nread = 0;		/* and call read() again */
	    	}
	    	else
				return(-1);
		} else if (nread == 0)
	    	break;				/* EOF */
	
		nleft -= nread;
		ptr   += nread;
    }
    return(n - nleft);		/* return >= 0 */
}
/* end readn */

int readn_at(ADIO_File fd, void *vptr, int n, ADIO_Offset offset)
{
    int m = 0, k = 0;
    char err_msg[128];
    ADIO_Status status; 
    int err;
    if (n > 0) {
        ADIO_ReadContig(fd,vptr,n,MPI_CHAR,ADIO_EXPLICIT_OFFSET,offset,&status,&err);
    	MPI_Get_count( &status, MPI_CHAR,(int *) &m);
	//	printf ("readn_at: n %d offset %lld m %d\n",n,offset,m);
        
    	if (m < n) {
      	sprintf(err_msg, "Readn_at: Error at reading file read %d instead of %d", m,n);
      	//handle_error(MPI_ERR_OTHER, err_msg);
    	}
    }
    return m;
}

