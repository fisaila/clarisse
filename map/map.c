
// File model for mapping:
// The asumption is that the file data type repetition is between lb and ub, 
// i.e the period is the extent for the exemple from below the repetition 
// starts at displ + lb = 1 + 4 = 5 
//
//        extent=7 
//disp=1 lb=4 up=12
//       -------------------
//   |   |     |     |     |
//   v   v     v     v     v
//  ______***____***____***___
//  0123456790123456790123456790
//
// Assumption: the first element of flat list contains always the indices = lb+displ


#include <assert.h>
#include "flatten.h"
#include "map.h"

// Assumes : 
// 1)x >= 0 &&  x < flat->extent
//   i.e. mapping is always between lb and ub, relatively to lb (from 0 to ub-lb)
// 2) flat indices are within the extent (from 0 to extent-1) 
// Returns the index of the flat_buf where x is located
// Result:  between 0 and flat_buf->count inclusively
//  MAP_CURRENT_BLOCK : when mapping on a block return the index of current block, o/w of the next block 
//  ____***____***____****__
//  000000011111112222222233
//  MAP_NEXT_BLOCK :  always return the index of next block
//  000011111112222222333333 
int find_bin_search(long long int x, distrib_t *distr, int map_shift){
  int l, r, found;
  flatlist_node_t *flat = distr->flat;
  l = 0; r = flat->count - 1; found = -1;
  assert(x >= 0);
  assert(x < flat->extent);
  assert(l <= r);

  while (l <= r) {
    int m = (l + r) / 2;
    if (x < flat->indices[m] + flat->blocklens[m]) {
	    if (l == m) {
	      found = m;
	      break;
	    }
	    if (x >= flat->indices[m - 1] + flat->blocklens[m - 1]) {
	      found = m ;
	      break;
	    }
	    r = m - 1 ;
	}
    else {
      if (m == r) {
	found = m +1;
	break;
      }
      if (x < flat->indices[m + 1] + flat->blocklens[m + 1]) {
	found = m + 1 ;
	      break;
      }
      l = m +1;
    }
  }
  if ((map_shift == MAP_NEXT_BLOCK) 
      && (found < flat->count)
      && (x < flat->indices[found] + flat->blocklens[found]) 
      && (x >= flat->indices[found])) found++;
  return found;
}

// Finds the file offset mapping from a view offset x) within the extent
// returns file offset
//    
// assumes : x >= 0 && 
// x < flat->incr_size[flat->count - 1] + flat->blocklens[flat->count - 1]
//
//  __d__[_***___***_*_]
//I:       012   345 6  
//                 1    
//O:-----012345678901234
long long int find_size_bin_search(long long int x, distrib_t *distr){
  flatlist_node_t *flat = distr->flat;
  int l = 0, r = flat->count - 1;
  long long int size;

  assert (x >= 0);
  assert (x < flat->incr_size[flat->count - 1] + 
	  flat->blocklens[flat->count - 1]);
  assert(l<=r);
  while (l <= r) {
    int m = (l + r) / 2;
    //printf("\n*** l=%d r=%d\n",l,r);
    if (x < flat->incr_size[m] + flat->blocklens[m]) {
	if ((l == m) || (x >= flat->incr_size[m])) {
	  size = flat->indices[m] + x - flat->incr_size[m];
	  break;
	}
      r = m - 1 ;
    }
    else {
	if (m == r) {
	    size = flat->indices[m] + x - flat->incr_size[m];
	    break;
	}
	if (x < flat->incr_size[m + 1] + flat->blocklens[m + 1]) {
	    size = flat->indices[m + 1] + x - flat->incr_size[m + 1];
	    break;
	}
	l = m +1;
    }
  }
  return size;
}


//  Maps a file offset onto a view offset within a period (i.e. an extent)
//  Assumes x >= 0 && x < flat->extent
//  __d__[_***___***_*_]
//                 1 
//I:     01234567890123  
//                     
//O:-----00012333344556
long long int func_sup_per(long long int x, distrib_t *distr){
  flatlist_node_t *flat = distr->flat;
  int i;

  assert(x >= 0);
  //if (x >= flat->extent)
  //  fprintf(stderr, "x=%lld flat->extent=%lld\n",x, flat->extent); 
  assert(x < flat->extent);

  i = find_bin_search(x, distr, MAP_CURRENT_BLOCK);
  if (i == flat->count)
    return flat->incr_size[i-1] +  flat->blocklens[i-1];
  // whole incremental size
  if ( x <= flat->indices[i])
    return flat->incr_size[i];
  else
    return x - flat->indices[i] + flat->incr_size[i];
} 


/* Maps a file offset onto a view offset (in bytes)  
   if the mapping falls on a hole, moves to the write
*/

long long int func_sup(long long int x, distrib_t *distr){

  flatlist_node_t *flat = distr->flat;
  assert(x >= 0);
  if (x < distr->displ) return 0;
  else {
    long long int sz = flat->incr_size[flat->count - 1] +
      flat->blocklens[flat->count - 1];
    return (x - distr->displ) / flat->extent * sz + 
      func_sup_per((x - distr->displ) % flat->extent, distr);
  }
}

//  Maps a file offset onto a view offset within a period (i.e. an extent)
//  Assumes x >= 0 && x < flat->extent
//  __d__[_***___***_*_]
//                 1 
//I:     01234567890123  
//                 1    
//O:-----00012222344455
long long int func_inf_per(long long int x, distrib_t *distr){
  flatlist_node_t *flat = distr->flat;
  int i;

  assert(x >= 0);
  assert(x < flat->extent);

  i = find_bin_search(x, distr, MAP_CURRENT_BLOCK);
  if (i == flat->count)
    return flat->incr_size[i-1] +  flat->blocklens[i-1]  - 1;
  if (x < flat->indices[i])
    return flat->incr_size[i] - 1;
  else
    return flat->incr_size[i] + x - flat->indices[i];
} 

/* Maps a file offset onto a view offset (in bytes)  
   if the mapping falls on a hole, moves to the left
   Returns -1 if it falls to the left of first byte in the view.
*/
long long int func_inf(long long int x, distrib_t *distr){
  flatlist_node_t *flat = distr->flat;
 
  assert(x >= 0);
  if (x < distr->displ) return -1;
  else {
    long long int sz = flat->incr_size[flat->count - 1] +
      flat->blocklens[flat->count - 1];
    return (x - distr->displ) / flat->extent * sz + 
      func_inf_per((x - distr->displ) % flat->extent, distr);
  }
}


/*  Maps a view offset onto a file offset within a period (i.e. an extent)
    Assumes x >= 0 && x < flat_buf->incr_size[flat_buf->count - 1]
    Maps exactly on find_size_bin_search 
 */


long long int func_1_per(long long int x, distrib_t *distr){
  assert (x >= 0);
  assert (x < distr->flat->incr_size[distr->flat->count - 1] + distr->flat->blocklens[distr->flat->count - 1]);  
  return  find_size_bin_search(x, distr);
}


// Maps a view offset on a file offset x in bytes  
// Assumes x >= 0
long long int func_1(long long int x, distrib_t *distr){
  flatlist_node_t *flat = distr->flat;
  long long int sz;
  assert(x >= 0);
  sz = flat->incr_size[flat->count - 1] +
    flat->blocklens[flat->count - 1];
  return distr->displ + x / sz * flat->extent + func_1_per(x % sz, distr);
}


//Get the number of bytes between l and r
long long int get_count(long long int l, long long int r, distrib_t *distr) {
  if (l > r) return 0;
  return func_inf(r, distr) - func_sup(l, distr) + 1;
}



/* This version compacts the neighboring contiguous blocks */ 

/*
int count_contiguous_blocks_memory(MPI_Datatype datatype, int count) {
    ADIOI_flatlist_node_t *flat_buf;
    flat_buf = get_flatlist(datatype);
    return (flat_buf)?(flat_buf->count*count):1;
}

void free_datatype(MPI_Datatype type) {
    int ni, na, nd, combiner; 
    MPI_Type_get_envelope(type, &ni, &na, &nd, &combiner); 
    if (combiner != MPI_COMBINER_NAMED)
	MPI_Type_free(&type);
}


int count_contiguous_blocks_file(MPI_File fh, MPI_Offset foff1, MPI_Offset foff2) {
    MPI_Offset disp;
    MPI_Datatype etype, filetype;
    char datarep[MPI_MAX_DATAREP_STRING];
    int is_contig;
    int ret;

    assert(foff1 <= foff2);
     
    MPI_File_get_view(fh, &disp, &etype, &filetype, datarep);
    //my_get_view(fh, &disp, &etype, &filetype);
    ADIOI_Datatype_iscontig(filetype, &is_contig);
    if (is_contig)
	ret=1;
    else { 
	MPI_Aint extent, lb;
	ADIOI_flatlist_node_t *flat_buf;
	flat_buf = get_flatlist(filetype);
	MPI_Type_get_extent(filetype, &lb, &extent);
	assert( extent == flat_buf->indices[flat_buf->count-1] - flat_buf->indices[0] + flat_buf->blocklens[flat_buf->count-1] );

	if (disp+lb > foff1) {
		printf("disp=%lld lb=%lld foff1=%lld foff2=%lld\n", disp, (long long) lb, foff1, foff2);
		print_flatlist(filetype);		
	}	
	assert(disp+lb <= foff1);
	assert(lb == flat_buf->indices[0]);

	if (foff2 < ((flat_buf->blocklens[0] == 0)?flat_buf->indices[1]:flat_buf->indices[0]))
	    ret=0;
	else{
	    int ind1, ind2;
	    ind1 = find_bin_search((foff1 - disp - lb) % extent, flat_buf, MAP_CURRENT_BLOCK);
	    ind2 = find_bin_search((foff2 - disp - lb) % extent, flat_buf, MAP_NEXT_BLOCK);
	    
	    if ((foff2 - disp - lb) < BEGIN_NEXT_BLOCK(foff1 - disp - lb, extent)) 
		ret = ind2 - ind1;
	    else 
		ret = flat_buf->count - ind1 - ((flat_buf->blocklens[flat_buf->count-1] == 0)?1:0)+
		    (BEGIN_BLOCK(foff2 - disp - lb, extent)-
		     BEGIN_NEXT_BLOCK(foff1 - disp - lb, extent)) 
		    / extent * (flat_buf->count-
				((flat_buf->blocklens[0] == 0)?1:0) -
				((flat_buf->blocklens[flat_buf->count-1] == 0)?1:0)-
				(((flat_buf->blocklens[flat_buf->count-1])&&(flat_buf->blocklens[flat_buf->count-1] + flat_buf->indices[flat_buf->count-1] - flat_buf->indices[0] == extent ))?1:0))
		    + ind2 - (((ind2>0)&&(flat_buf->blocklens[0] == 0))?1:0) -  
		    (((ind2>0)&&(flat_buf->indices[0]==0))?1:0); 
	}
    }
    free_datatype(etype);
    free_datatype(filetype);
    return ret;
}
*/

