#ifndef _FLATTEN_INCLUDE
#define _FLATTEN_INCLUDE

#include "list.h"
#include "mpi.h"

typedef struct fl_node { 
  struct dllist_link link;
  long long int extent;      // Extent of the region
  long long int count;       // no. of contiguous blocks 
  long long int *blocklens;  // array of contiguous block lengths (bytes)
  long long int *indices;    // array of byte offsets of each block 
  long long int *incr_size;  // incremental size
  
  //long long int displ;     // Displacement where the repetitions start (==displ+lb for MPI views)
  long long int lb;          // Datatype lb (for MPI views)
  //int rep; // repetitions of the flatten data type
  int id;                    // Datatype id 
  int refcount;              // Number of references
  //MPI_Datatype type;  
} flatlist_node_t;


typedef struct distrib{
  long long int displ;
  flatlist_node_t *flat;
} distrib_t;

//void free_flatlist(flatlist_node_t *flat);
void free_datatype(MPI_Datatype type);

flatlist_node_t *  get_flatlist(MPI_Datatype d);
// Removes from the flatlist and frees it
void delete_flatlist(int id);
// Removes from the flatlist and returns 
flatlist_node_t * remove_flatlist(int id);
// Frees the flatlist
void free_flatlist(flatlist_node_t *flat);
void print_flatlist(flatlist_node_t *flat); 
void print_flatlist_list();
void draw_flatlist(flatlist_node_t *flat, int rep, long long int displ);
int cmp_flatlists(flatlist_node_t *flat_buf1, flatlist_node_t *flat_buf2);
int contig_flatlist(flatlist_node_t *flat);

flatlist_node_t *  flat_vector(long long int count, long long int blocklength, long long int stride);

void distrib_init(distrib_t *distr, MPI_Datatype dt, long long int displ);
distrib_t *distrib_get(MPI_Datatype dt, long long int displ);
distrib_t *distrib_get2(flatlist_node_t *flat, long long int displ);
void distrib_free(distrib_t *distr);
void distrib_free_content(distrib_t *distr);
void distrib_update(distrib_t *distr, MPI_Datatype dt, long long int displ);
void distrib_print(distrib_t *distr); 
void distrib_draw(distrib_t *distr, int rep);
int distrib_contig(distrib_t *distr);

flatlist_node_t * ROMIO_Flatten_datatype(MPI_Datatype datatype);
void ROMIO_Datatype_iscontig(MPI_Datatype datatype, int *flag);
int alloc_flat_arrays(flatlist_node_t * flat);

#endif
