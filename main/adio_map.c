#include "adio.h"
#include "adioi.h"
#include "adio_map.h"
#include "error.h"
#include "util.h"
#include "hdr.h"

void ADIOI_CLS_WriteStridedColl(ADIO_File fd, const void *buf, int count,
				   MPI_Datatype datatype, int file_ptr_type,
				   ADIO_Offset offset, ADIO_Status *status, int *error_code);
void ADIOI_CLS_ReadStridedColl(ADIO_File fd, void *buf, int count,
				  MPI_Datatype datatype, int file_ptr_type,
				  ADIO_Offset offset, ADIO_Status *status, int *error_code);

cls_adio_c_ctxt_map_t *cls_adio_c_ctxt_map = NULL;

extern client_global_state_t client_global_state;

void cls_adio_set_generic_functions(MPI_File fh){
  ADIO_File fd; 

  fd = MPIO_File_resolve(fh);
  // On BGQ: GPFS driver
#ifndef BGQ
  fd->fns->ADIOI_xxx_WriteStridedColl = ADIOI_GEN_WriteStridedColl;
  fd->fns->ADIOI_xxx_ReadStridedColl = ADIOI_GEN_ReadStridedColl;
#endif
  // I io_buf was freed restore it
  if (fd->io_buf == NULL)
	fd->io_buf = ADIOI_Malloc(fd->hints->cb_buffer_size);
}

void MPIR_Ext_cs_enter_allfunc(void);
void MPIR_Ext_cs_exit_allfunc(void);


void cls_adio_open(client_iocontext_t *c_ctxt) {
   ADIO_File fd; 
   cls_adio_c_ctxt_map_t * cls_adio_c_ctxt_map_elem;

   fd = MPIO_File_resolve(c_ctxt->fh);
   if (fd->io_buf) {
     ADIOI_Free(fd->io_buf);
     fd->io_buf = NULL;
   }
   c_ctxt->filename = fd->filename;
   c_ctxt->fp_ind = (MPI_Offset) fd->fp_ind;
   c_ctxt->fp_sys_posn = (MPI_Offset) fd->fp_sys_posn;
   c_ctxt->disp = fd->disp;
   c_ctxt->etype = fd->etype;
   c_ctxt->filetype = fd->filetype;
   c_ctxt->etype_size = fd->etype_size;
   c_ctxt->fd_sys = fd->fd_sys;
   
   fd->fns->ADIOI_xxx_WriteStridedColl = ADIOI_CLS_WriteStridedColl;
   fd->fns->ADIOI_xxx_ReadStridedColl = ADIOI_CLS_ReadStridedColl;
   if (client_global_state.clarisse_pars.server_map == CLARISSE_SERVER_MAP_ROMIO) {
     client_global_state.clarisse_pars.nr_servers = fd->hints->cb_nodes;
     cls_assign_servers(client_global_state.clarisse_pars.server_map, client_global_state.clarisse_pars.nr_servers, fd->hints->ranklist);
   }
   cls_adio_c_ctxt_map_elem = (cls_adio_c_ctxt_map_t *)clarisse_malloc(sizeof(cls_adio_c_ctxt_map_t));
   cls_adio_c_ctxt_map_elem->key = (void *)fd;
   cls_adio_c_ctxt_map_elem->c_ctxt = c_ctxt;
   HASH_ADD_PTR(cls_adio_c_ctxt_map, key, cls_adio_c_ctxt_map_elem);
}

  

void cls_adio_close(client_iocontext_t *c_ctxt) {
   ADIO_File fd; 
   cls_adio_c_ctxt_map_t * cls_adio_c_ctxt_map_elem;
   void *key;
     
   fd = MPIO_File_resolve(c_ctxt->fh);
   // Alloc for correctness (close deallocated io_buf)
   if (fd->io_buf == NULL)
     fd->io_buf = ADIOI_Malloc(1);
   key = (void *)fd;
   HASH_FIND_PTR(cls_adio_c_ctxt_map, &key, cls_adio_c_ctxt_map_elem);
   HASH_DEL(cls_adio_c_ctxt_map, cls_adio_c_ctxt_map_elem); 
   clarisse_free(cls_adio_c_ctxt_map_elem);
}

void ADIOI_CLS_WriteStridedColl(ADIO_File fd, const void *buf, int count,
				MPI_Datatype datatype, int file_ptr_type,
				ADIO_Offset offset, ADIO_Status *status, int
				*error_code)
{
  void *key;
  cls_adio_c_ctxt_map_t *cls_adio_c_ctxt_map_elem;
  client_iocontext_t *c_ctxt;
  int bytes_xfered;

  key = (void *)fd;
  HASH_FIND_PTR(cls_adio_c_ctxt_map, &key, cls_adio_c_ctxt_map_elem);
  
  if (cls_adio_c_ctxt_map_elem == NULL)
    handle_err(MPI_ERR_OTHER, "ADIOI_CLS_WriteStridedColl: c_ctxt not found in cls_adio_c_ctxt_map ");
  
  c_ctxt = cls_adio_c_ctxt_map_elem->c_ctxt;
  MPIR_Ext_cs_exit_allfunc();
  
  switch (file_ptr_type){
  case ADIO_EXPLICIT_OFFSET: file_ptr_type = CLARISSE_EXPLICIT_OFFSET; break;
  case ADIO_INDIVIDUAL: file_ptr_type = CLARISSE_INDIVIDUAL; break;
  case ADIO_SHARED: file_ptr_type = CLARISSE_SHARED; break;
  default: handle_err(MPI_ERR_OTHER, "Not suported file_ptr_type"); 
  }
  
  bytes_xfered = cls_write_coll(c_ctxt, (char *) buf, count,
				datatype, file_ptr_type, (MPI_Offset)offset);

  if (status) 
    MPIR_Status_set_bytes(status, datatype, bytes_xfered);
  *error_code = MPI_SUCCESS;  
  MPIR_Ext_cs_enter_allfunc();
}


void ADIOI_CLS_ReadStridedColl(ADIO_File fd, void *buf, int count,
			       MPI_Datatype datatype, int file_ptr_type,
			       ADIO_Offset offset, ADIO_Status *status, int
			       *error_code)
{
  void *key;
  cls_adio_c_ctxt_map_t *cls_adio_c_ctxt_map_elem;
  client_iocontext_t *c_ctxt;
  int bytes_xfered;

  key = (void *)fd;
  HASH_FIND_PTR(cls_adio_c_ctxt_map, &key, cls_adio_c_ctxt_map_elem);
  
  if (cls_adio_c_ctxt_map_elem == NULL)
    handle_err(MPI_ERR_OTHER, "ADIOI_CLS_WriteStridedColl: c_ctxt not found in cls_adio_c_ctxt_map ");
  
  c_ctxt = cls_adio_c_ctxt_map_elem->c_ctxt;
  MPIR_Ext_cs_exit_allfunc();
  
  switch (file_ptr_type){
  case ADIO_EXPLICIT_OFFSET: file_ptr_type = CLARISSE_EXPLICIT_OFFSET; break;
  case ADIO_INDIVIDUAL: file_ptr_type = CLARISSE_INDIVIDUAL; break;
  case ADIO_SHARED: file_ptr_type = CLARISSE_SHARED; break;
  default: handle_err(MPI_ERR_OTHER, "Not suported file_ptr_type"); 
  }
  
  bytes_xfered = cls_read_coll(c_ctxt, buf, count,
				datatype, file_ptr_type, (MPI_Offset)offset);

  if (status) 
    MPIR_Status_set_bytes(status, datatype, bytes_xfered);
  *error_code = MPI_SUCCESS;  
  MPIR_Ext_cs_enter_allfunc();
}


/*
long long int ADIO_get_fsize(clarisse_target_t *tgt) {
  int error_code;
  ADIO_Fcntl_t fcntl_struct; 

  ADIO_Fcntl(((ADIO_File)tgt->local_handle.adio_fd), ADIO_FCNTL_GET_FSIZE, &fcntl_struct, &error_code);
  if (error_code != MPI_SUCCESS)
    error_code = MPIO_Err_return_file(((ADIO_File)tgt->local_handle.adio_fd), error_code);
  return fcntl_struct.fsize;
}

long long int bcast_ADIO_get_fsize(ADIO_File fd) {
  if (fd->access_mode & ADIO_CREATE & ADIO_EXCL) 
    return 0;
  else {
    int myrank;
    long long int fsize_on_disk;
    
    MPI_Comm_rank(fd->comm, &myrank);
    
    if (myrank == 0) {
      int error_code;
      ADIO_Fcntl_t fcntl_struct; 
      
      ADIO_Fcntl(fd, ADIO_FCNTL_GET_FSIZE, &fcntl_struct, &error_code);
      if (error_code != MPI_SUCCESS)
	error_code = MPIO_Err_return_file(fd, error_code);
      fsize_on_disk = fcntl_struct.fsize;
    }
  
    MPI_Bcast(&fsize_on_disk, 1, MPI_LONG_LONG_INT, 0, fd->comm);
    return fsize_on_disk;
  }
}


 */
