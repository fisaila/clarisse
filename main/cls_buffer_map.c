#ifdef CLARISSE_EXTERNAL_BUFFERING
#include "client_iocontext.h"
#include "hdr.h"
#include "util.h"
#include "error.h"
#include <assert.h>

extern client_global_state_t client_global_state;


int client_buffer_map(int op, client_iocontext_t *c_ctxt, clarisse_off_t l_d, clarisse_off_t r_d) {
  struct rq_file_buffer_map rq;
  int ret = MPI_SUCCESS, cnt = 0;
  clarisse_off_t log_ios_rank, i = 0, b = c_ctxt->clarisse_pars.buffer_size, active_nr_servers = client_global_state.active_nr_servers;

  rq.active_nr_servers = active_nr_servers;
  for (log_ios_rank = 0; log_ios_rank < active_nr_servers; log_ios_rank++) {
    if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
      int j;
      rq.global_descr = c_ctxt->global_descr;
      rq.l_d = BEGIN_BLOCK(l_d, b);
      j = rq.l_d % (b * active_nr_servers) / b;  
      if (i < j)
	rq.l_d +=  (active_nr_servers - j + i) * b;
      if (i > j)
	rq.l_d += (i - j) * b;
      rq.r_d = BEGIN_BLOCK(r_d, b);
      j = rq.r_d % (b * active_nr_servers) / b;
      if (i < j)
	rq.r_d -= (j - i) * b;
      if (i > j)
	rq.r_d -=  (active_nr_servers - i + j) * b;
      
      // rq.l_d > rq.r_d the server is not involved in the collective I/O
      if (rq.l_d <= rq.r_d) {
	rq.updated_block_mask = c_ctxt->updated_block_masks[i];
	
	ret = MPI_Send(&rq, sizeof( struct rq_file_buffer_map), MPI_BYTE, client_global_state.log2ph[log_ios_rank], op, client_global_state.initial_cli_serv_intercomm);
	cnt ++;
	if (ret != MPI_SUCCESS) {
	  handle_error(ret, "Error when sending the reqs");
	  return ret;
	}
      }
      i++;
    }
  }

  int /*active_log_ios_rank,*/ ni, nj, n;

  ni = l_d / b;
  nj = r_d / b;
  n = MIN(nj - ni + 1, active_nr_servers);
  assert(n == cnt);

  //active_log_ios_rank  = ni % client_global_state.active_nr_servers;
  for (i = 0; i < n; i++) {
    int ret, val;
    MPI_Status status;
    //ret = MPI_Recv(&val, 1, MPI_INT,  client_global_state.log2ph[client_global_state.active_log_servers[active_log_ios_rank]], (rq_type == RQ_FILE_WRITE_BUFFER_MAP)?RT_FILE_WRITE_BUFFER_MAP:RT_FILE_READ_BUFFER_MAP, client_global_state.initial_cli_serv_intercomm, &status);
    ret = MPI_Recv(&val, 1, MPI_INT,  MPI_ANY_SOURCE, (op == RQ_FILE_WRITE_BUFFER_MAP)?RT_FILE_WRITE_BUFFER_MAP:RT_FILE_READ_BUFFER_MAP, client_global_state.initial_cli_serv_intercomm, &status);
    if (ret != MPI_SUCCESS) {
      handle_error(ret, "get_send_block_masks: Error receiving the rets");
      return ret;	
    }
    //active_log_ios_rank = (active_log_ios_rank + 1) % client_global_state.active_nr_servers;
  }
  
  MPI_Barrier(c_ctxt->intracomm); //needed for propagating all buffer map

  return ret;
}

int client_buffer_map_isend(int op, client_iocontext_t *c_ctxt, clarisse_off_t l_d, clarisse_off_t r_d) {
  struct rq_file_buffer_map rq;
  int ret = MPI_SUCCESS, cnt = 0;
  clarisse_off_t log_ios_rank, i = 0, b = c_ctxt->clarisse_pars.buffer_size, active_nr_servers = client_global_state.active_nr_servers;
  MPI_Request *requests;

  requests = (MPI_Request *) clarisse_malloc(active_nr_servers * sizeof(MPI_Request));

  rq.active_nr_servers = active_nr_servers;
  for (log_ios_rank = 0; log_ios_rank < active_nr_servers; log_ios_rank++) {
    if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
      int j;
      rq.global_descr = c_ctxt->global_descr;
      rq.l_d = BEGIN_BLOCK(l_d, b);
      j = rq.l_d % (b * active_nr_servers) / b;  
      if (i < j)
	rq.l_d +=  (active_nr_servers - j + i) * b;
      if (i > j)
	rq.l_d += (i - j) * b;
      rq.r_d = BEGIN_BLOCK(r_d, b);
      j = rq.r_d % (b * active_nr_servers) / b;
      if (i < j)
	rq.r_d -= (j - i) * b;
      if (i > j)
	rq.r_d -=  (active_nr_servers - i + j) * b;
      
      // rq.l_d > rq.r_d the server is not involved in the collective I/O
      if (rq.l_d <= rq.r_d) {
	rq.updated_block_mask = c_ctxt->updated_block_masks[i];
	
	ret = MPI_Isend(&rq, sizeof( struct rq_file_buffer_map), MPI_BYTE, client_global_state.log2ph[log_ios_rank], op, client_global_state.initial_cli_serv_intercomm, requests + cnt);
	cnt ++;
	if (ret != MPI_SUCCESS) {
	  handle_error(ret, "Error when sending the reqs");
	  return ret;
	}
      }
      i++;
    }
  }

  int /*active_log_ios_rank,*/ ni, nj, n;

  ni = l_d / b;
  nj = r_d / b;
  n = MIN(nj - ni + 1, active_nr_servers);

  //active_log_ios_rank  = ni % client_global_state.active_nr_servers;
  for (i = 0; i < n; i++) {
    int ret, val;
    MPI_Status status;
    //ret = MPI_Recv(&val, 1, MPI_INT,  client_global_state.log2ph[client_global_state.active_log_servers[active_log_ios_rank]], (rq_type == RQ_FILE_WRITE_BUFFER_MAP)?RT_FILE_WRITE_BUFFER_MAP:RT_FILE_READ_BUFFER_MAP, client_global_state.initial_cli_serv_intercomm, &status);
    ret = MPI_Recv(&val, 1, MPI_INT,  MPI_ANY_SOURCE, (op == RQ_FILE_WRITE_BUFFER_MAP)?RT_FILE_WRITE_BUFFER_MAP:RT_FILE_READ_BUFFER_MAP, client_global_state.initial_cli_serv_intercomm, &status);
    if (ret != MPI_SUCCESS) {
      handle_error(ret, "get_send_block_masks: Error receiving the rets");
      return ret;	
    }
    //active_log_ios_rank = (active_log_ios_rank + 1) % client_global_state.active_nr_servers;
  }
  
  MPI_Waitall(cnt, requests, MPI_STATUSES_IGNORE);
  clarisse_free(requests);

  MPI_Barrier(c_ctxt->intracomm); //needed for propagating all buffer map

  return ret;
}





#endif
