/* ---------------------------------------------------------------- */
/* (C)Copyright IBM Corp.  2007, 2008                               */
/* ---------------------------------------------------------------- */
/**
 * \file ad_bg_aggrs.c
 * \brief The externally used function from this file is is declared in ad_bg_aggrs.h
 */

/* -*- Mode: C; c-basic-offset:4 ; -*- */
/* 
 *   Copyright (C) 1997-2001 University of Chicago. 
 *   See COPYRIGHT notice in top-level directory.
 */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <assert.h>
#include "aggr.h"
#include "util.h"
#include "error.h"
//#define TRACE_ON 

#ifdef BGQ
#include <spi/include/kernel/process.h>
#include <firmware/include/personality.h>

#ifdef HAVE_MPIX_H
#include <mpix.h>
#endif

static int intsort(const void *p1, const void *p2);
static unsigned get_manhattan_distance(unsigned *n1, unsigned *n2);

Personality_t pers;
MPIX_Hardware_t hw;
unsigned torusSize[MPIX_TORUS_MAX_DIMS];
unsigned dimTorus[MPIX_TORUS_MAX_DIMS];
int partition_size = 1;
sortstruct_t *bridges;
pset_info_t *psets;  
int cnt_psets;
int ppn;
int min_aggr_pset = INT_MAX;
int max_aggr_pset = 0;
int min_pset_size = INT_MAX;
int max_pset_size = 0;

#endif //BGQ

const char *aggr_assignment_names[]={"PLATFORM_BGQ_INCLUDE_BRIDGE",
				     "PLATFORM_BGQ_EXCLUDE_BRIDGE",
				     "PLATFORM_ANY_UNIFORM",
				     "PLATFORM_ANY_READFROMFILE"};
int flag_init_aggrs = 0;

void aggr_init(){
  if (!flag_init_aggrs) {
    flag_init_aggrs = 1;
#ifdef BGQ
    int i, myrank, nprocs, iambridge;
    unsigned bridge_coord[5];

    MPIX_Hardware(&hw);    
    Kernel_GetPersonality(&pers, sizeof(pers));
    for (i = 0; i < MPIX_TORUS_MAX_DIMS; i++) {
      torusSize[i] = hw.Size[i];
      dimTorus[i] = hw.isTorus[i];
      partition_size *= hw.Size[i];
    }
    ppn = hw.ppn;;
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    iambridge = am_i_bridge();
    cnt_psets = get_count_psets(iambridge);
    /* Allgather the ranks and bridgeCoords to determine the bridge
       rank and how many ranks belong to each bridge rank*/
    bridges = (sortstruct_t *) clarisse_malloc(sizeof(sortstruct_t) * nprocs);
    
    /* We're going to sort this structure by bridgeCoord:
       
       typedef struct{
       int rank;
       int bridgeCoord;
       } sortstruct_t; 
       
       and I want the rank that IS the bridge to sort first, so 
       OR in '1' on non-bridge ranks that use a bridge coord. 
    */ 
    
    bridges[myrank].rank = myrank;
    bridges[myrank].bridgeCoord = get_bridge_coords();
    bridges[myrank].iambridge = iambridge;
    bridge_coord[0] = pers.Network_Config.cnBridge_A;
    bridge_coord[1] = pers.Network_Config.cnBridge_B;
    bridge_coord[2] = pers.Network_Config.cnBridge_C;
    bridge_coord[3] = pers.Network_Config.cnBridge_D;
    bridge_coord[4] = pers.Network_Config.cnBridge_E;
    bridges[myrank].distance_to_bridge = get_manhattan_distance(bridge_coord, hw.Coords);
    //if(!iambridge)
    //  bridges[myrank].bridgeCoord |= 1;  /* I am not bridge, turn on bit */
    MPI_Allgather(MPI_IN_PLACE, 4, MPI_INT, bridges, 4, MPI_INT, MPI_COMM_WORLD);
    qsort(bridges, nprocs, sizeof(sortstruct_t), intsort);
    get_pset_info(&psets);
    if (myrank == 0) 
      printf("nprocs = %d ppn=%d, cnt_psets=%d min_pset_size=%d max_pset_size=%d\n", nprocs, ppn, cnt_psets, min_pset_size, max_pset_size);
#endif //BGQ
    
  }
}

void aggr_finalize(){
  if (flag_init_aggrs) {
#ifdef BGQ
    clarisse_free(bridges);
    clarisse_free(psets);
#endif
    flag_init_aggrs = 0;
  }
}

void aggr_print(int nr_aggrs,  int *aggr_vector){
  int myrank;
  MPI_Comm_rank( MPI_COMM_WORLD, &myrank);
  if (myrank == 0) {
    int i;
    printf("AGGREGATORS:%d \n_____________\n", nr_aggrs);
    for (i = 0; i < nr_aggrs; i++) 
      printf("%d ", aggr_vector[i]);	
    printf("\n");
  }
}


#ifdef BGQ
/*static int intsort(const void *p1, const void *p2){
   sortstruct_t *i1, *i2;
   i1 = (sortstruct_t *)p1;
   i2 = (sortstruct_t *)p2;
   return(i1->bridgeCoord - i2->bridgeCoord);
}
*/

// if they have the same bridge, sort them increasingly on the distance to bridge
static int intsort(const void *p1, const void *p2){
   sortstruct_t *i1, *i2;
   i1 = (sortstruct_t *)p1;
   i2 = (sortstruct_t *)p2;
   if (i1->bridgeCoord == i2->bridgeCoord)
     return (i1->distance_to_bridge - i2->distance_to_bridge); 
   else
     return(i1->bridgeCoord - i2->bridgeCoord);
}



int32_t get_bridge_coords(){
  int32_t bridgeCoords;
  aggr_init();
  bridgeCoords = pers.Network_Config.cnBridge_A << 24 |
    pers.Network_Config.cnBridge_B << 18 |
    pers.Network_Config.cnBridge_C << 12 |
    pers.Network_Config.cnBridge_D << 6 |
    pers.Network_Config.cnBridge_E << 2;
  assert((bridgeCoords >= 0)); /* A dim is < 6 bits or sorting won't work */
  return bridgeCoords;
}

// returns 1 if I am a bridge, 0 o/w
int am_i_bridge(){
  aggr_init();
  if((hw.Coords[0] == pers.Network_Config.cnBridge_A) && 
     (hw.Coords[1] == pers.Network_Config.cnBridge_B) && 
     (hw.Coords[2] == pers.Network_Config.cnBridge_C) && 
     (hw.Coords[3] == pers.Network_Config.cnBridge_D) && 
     (hw.Coords[4] == pers.Network_Config.cnBridge_E))
    return 1;      /* I am bridge */
  else
    return 0;
}



// Counts the number of psets for MPI_COMM_WORLD
int get_count_psets(int iambridge){
  int local_cnt_psets;
  aggr_init();
  MPI_Allreduce(&iambridge, &local_cnt_psets, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  local_cnt_psets /= hw.ppn;
  return local_cnt_psets;
}

/* This function computes the number of hops between the torus coordinates of the
 * n1 and n2.
*/
static unsigned get_manhattan_distance(unsigned *n1, unsigned *n2) {
  unsigned totalDistance = 0;
  int i;
  for (i = 0; i < MPIX_TORUS_MAX_DIMS; i++) {
    unsigned dimDistance = abs((int) n1[i] - (int) n2[i]);
    if (dimDistance > 0) { // could torus make it closer?
      if (dimTorus[i]) {
        if (n1[i] == torusSize[i]) { // is wrap-around closer
          if ((n2[i] + 1) < dimDistance) // assume will use torus link
            dimDistance = n2[i] + 1;
        }
        else if (n2[i] == torusSize[i]) { // is wrap-around closer
          if ((n1[i] + 1) < dimDistance) // assume will use torus link
            dimDistance = n1[i] + 1;
        }
      }
    } /* else: dimDistance == 0, meaning n1[i] and n2[i] are
	 the same and there's no closer point to pick */
    totalDistance += dimDistance;
  }
  return totalDistance;
}

//Gather pset info
void get_pset_info(pset_info_t **psets){
  int nprocs, i, previous_bridge = 0, crt_pset = -1, displ = 0;

  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  *psets = (pset_info_t *) clarisse_malloc(cnt_psets * sizeof(pset_info_t));
  for(i = 0; i < nprocs; i++){
    if (!previous_bridge && bridges[i].iambridge) { 
      previous_bridge = 1;
      crt_pset++;
      if (crt_pset > 0) { 
	displ += (*psets)[crt_pset-1].pset_size;
	min_pset_size = MIN(min_pset_size, (*psets)[crt_pset-1].pset_size);
	max_pset_size = MIN(min_pset_size, (*psets)[crt_pset-1].pset_size);	
      }
      (*psets)[crt_pset].pset = bridges + displ;
      (*psets)[crt_pset].pset_size = 0;
    }
    if (!bridges[i].iambridge)
      previous_bridge = 0;		
    if (crt_pset >= 0)
      (*psets)[crt_pset].pset_size++;
  }
  min_pset_size = MIN(min_pset_size, (*psets)[crt_pset].pset_size);
  max_pset_size = MIN(min_pset_size, (*psets)[crt_pset].pset_size);  
  assert((crt_pset + 1) == cnt_psets);
}

void aggr_print_psets(int nr_aggr,  int *aggr_vector, int *aggr_per_pset_vector){
  //#ifdef TRACE_ON	
  int myrank, nprocs, i, j;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  if (myrank == 0){
    /*printf("Number psets=%d Total aggr =%d\n", cnt_psets, nr_aggr);	
    for(i = 0 ; i < nprocs; i++){
      printf("%d (%d)", bridges[i].rank,bridges[i].distance_to_bridge );
      if (bridges[i].iambridge)
	printf("B");
      printf(" ");
      if (!((i + 1) % 16)) printf("\n");
    }*/
    printf("PSETS\n_____________\n");
    for(i = 0; i < cnt_psets; i++){
      printf("PSET %d SIZE = %d AGGR_PER_PSET = %d \n____\n", i, psets[i].pset_size, aggr_per_pset_vector[i]);
      for (j = 0; j < psets[i].pset_size; j++) { 
	printf("%4d(%d)", psets[i].pset[j].rank, psets[i].pset[j].distance_to_bridge);
	if (psets[i].pset[j].iambridge)
	  printf("B");
	else
	  printf(" ");
	if (!((j + 1) % 16)) printf("\n");
      }
      printf("\n");
    }
    aggr_print(nr_aggr,  aggr_vector);
   }
  //#endif //TRACE_ON
}
#endif //BGQ



int get_aggregators_bgq_include_bridge(int bg_nodes_pset, int **aggr_vector, int *flag_iamserver){	
#ifdef BGQ 
  int i, j, myrank;
  int crt_aggr = 0;
  int *aggr_per_pset_vector;

  *flag_iamserver = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  *aggr_vector = (int *) clarisse_malloc ((bg_nodes_pset + 1) * cnt_psets * sizeof(int));
  aggr_per_pset_vector=(int *) clarisse_malloc (cnt_psets * sizeof(int)); 
  
  // First place the bridges and then the non-bridges
  for(i = 0; i < cnt_psets; i++){
    int step = psets[i].pset_size / bg_nodes_pset ;
    aggr_per_pset_vector[i] = 0;
    for (j = 0; j < psets[i].pset_size; j += step) {
      if (psets[i].pset[j].iambridge) {
	(*aggr_vector)[crt_aggr] = psets[i].pset[j].rank;
	(aggr_per_pset_vector)[i]++;
	if (myrank == (*aggr_vector)[crt_aggr])
	  *flag_iamserver = 1;
	crt_aggr++;
      }
      else
	break;
    }
  }
  for(i = 0; i < cnt_psets; i++){
    int step = psets[i].pset_size / bg_nodes_pset ;
    for (j = 0; j < psets[i].pset_size; j += step) {
      if (!psets[i].pset[j].iambridge) {
	(*aggr_vector)[crt_aggr] = psets[i].pset[j].rank;
	(aggr_per_pset_vector)[i]++;
	if (myrank == (*aggr_vector)[crt_aggr])
	  *flag_iamserver = 1;
	crt_aggr++;
      }
    }
    if ((aggr_per_pset_vector)[i] < bg_nodes_pset + 1) {
      // The last aggregator is the last rank in the pset
      (*aggr_vector)[crt_aggr] = psets[i].pset[psets[i].pset_size-1].rank;
      (aggr_per_pset_vector)[i]++;
      if (myrank == (*aggr_vector)[crt_aggr])
	*flag_iamserver = 1;
      crt_aggr++;
    }
    assert((aggr_per_pset_vector)[i] == bg_nodes_pset + 1);
  }
  aggr_print(crt_aggr,  *aggr_vector);
  //aggr_print_psets(crt_aggr, *aggr_vector, aggr_per_pset_vector);
  clarisse_free(aggr_per_pset_vector);
  assert (crt_aggr == (bg_nodes_pset + 1) * cnt_psets);
  return crt_aggr;
#else
  CLARISSE_UNUSED(bg_nodes_pset);
  CLARISSE_UNUSED(aggr_vector);
  CLARISSE_UNUSED(flag_iamserver);
  return -1;
#endif
}

// Vesta: 128 nodes x 16 ppn : 8 psets with 16 nodes (256 ranks)
// max(bg_nodes_pset) = (min_pset_size - 1) * ppn
//  32 aggr (bg_nodes_pset = 4), ppn = 16 : step=1,2,4,8,16,32
// 128 aggr (bg_nodes_pset = 16), ppn = 16 : step=1,2,4,8 
// 256 aggr (bg_nodes_pset = 32), ppn = 16 : step=1,2,4
// 512 aggr (bg_nodes_pset = 64), ppn = 16 : step=1,2
 
int get_aggregators_bgq_exclude_bridge(int bg_nodes_pset, int step, int **aggr_vector, int *flag_iamserver){	
#ifdef BGQ 
  int i, j, myrank, crt_aggr = 0;
  int *aggr_per_pset_vector;

  *flag_iamserver = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  *aggr_vector = (int *) clarisse_malloc (bg_nodes_pset * cnt_psets * sizeof(int));
  aggr_per_pset_vector=(int *) clarisse_malloc (cnt_psets * sizeof(int)); 

  if (bg_nodes_pset >= (min_pset_size - 1) * ppn) {
    fprintf(stderr,"bg_nodes_pset(%d) >= (min_pset_size(%d) - 1) * ppn(%d)\n ", 
	    bg_nodes_pset, min_pset_size, ppn);
    handle_err(MPI_ERR_OTHER, "bg_nodes_pset value invalid");
  }
  for(i = 0; i < cnt_psets; i++){
    int k;
    //step = psets[i].pset_size / bg_nodes_pset ;
    //for (j = ppn; j < psets[i].pset_size; j += step) {
    (aggr_per_pset_vector)[i] = 0;	
    for (k = 0, j = ppn; k < bg_nodes_pset; k++, j += step){
      assert (!psets[i].pset[j].iambridge);
      (*aggr_vector)[crt_aggr] = psets[i].pset[j].rank;
      (aggr_per_pset_vector)[i]++;
      if (myrank == (*aggr_vector)[crt_aggr])
	*flag_iamserver = 1;
      crt_aggr++;
    }
    assert((aggr_per_pset_vector)[i] == bg_nodes_pset);
  }
  aggr_print(crt_aggr,  *aggr_vector);
  //aggr_print_psets(crt_aggr, *aggr_vector, aggr_per_pset_vector);
  clarisse_free(aggr_per_pset_vector);
  assert (crt_aggr == bg_nodes_pset * cnt_psets);
  return crt_aggr;
#else
  CLARISSE_UNUSED(bg_nodes_pset);
  CLARISSE_UNUSED(aggr_vector);
  CLARISSE_UNUSED(step);
  CLARISSE_UNUSED(flag_iamserver);
  return -1;
#endif
}



int get_aggregators_any_uniform(int nr_aggrs, int **aggr_vector, int *flag_iamserver){
  int step, nprocs, i, myrank;

  *flag_iamserver = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  step = nprocs / nr_aggrs;
  *aggr_vector = (int *) clarisse_malloc (nr_aggrs * sizeof(int));
  for(i = 0; i < nr_aggrs; i++) {
    (*aggr_vector)[i] = i * step;
    if (myrank == (*aggr_vector)[i])
      *flag_iamserver = 1;
  }
  return nr_aggrs;
}
// type = PLATFORM_BGQ_INCLUDE_BRIDGE / PLATFORM_ANY_UNIFORM
// param is 
//-  bg_nodes_pset for BGQ 
//-  nr_aggrs O/W
int get_aggregators(int type, int param, int step, int **aggr_vector, int *flag_iamserver){	
  int naggrs;

  //aggr_init();
  switch (type) {
  case PLATFORM_BGQ_INCLUDE_BRIDGE:
    naggrs = get_aggregators_bgq_include_bridge(param, aggr_vector, flag_iamserver);
    break;
    
  case PLATFORM_BGQ_EXCLUDE_BRIDGE:
    naggrs = get_aggregators_bgq_exclude_bridge(param, step, aggr_vector, flag_iamserver);
    break;

  case PLATFORM_ANY_UNIFORM: 
    naggrs = get_aggregators_any_uniform(param, aggr_vector, flag_iamserver);
    break;
  default:
    naggrs = -1;
  }
  if (naggrs < 0) {
    if (type < AGGR_ASSIGNMENT_COUNT)
      fprintf(stderr, "Aggregator assignment %s returned error or unsupported", aggr_assignment_names[type]);
    else
      fprintf(stderr, "Aggregator assignment %d NOT DEFINED", type);
  }
  
  /*
  for (i = 0; i < nprocs; i++)
    aggr_table[i]=0; 
  for(i = 0; i < nr_aggr; i++)
    aggr_table[aggr_vector[i]] = 1;
  */
  return naggrs;
}

/*
// Distribute the aggregators evenly over the ranks 

int get_aggregators2(int *aggr_table, int bg_nodes_pset){
  int nprocs, i, nr_aggr, step, myrank;

  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  nr_aggr = (bg_nodes_pset + 1) * get_count_psets(am_i_bridge());  
  step = nprocs / nr_aggr;
//  if (myrank == 0) {
//        printf("nprocs=%d nr_aggr=%d bg_nodes_pset=%d step=%d\n",nprocs, nr_aggr, bg_nodes_pset, step);
//  }
  for(i = 0; i < nprocs; i++)
    if (!(i % step))
      aggr_table[i]=1;
    else 
      aggr_table[i]=0;
#ifdef TRACE_ON 
  if (myrank==0){
    printf("AGGREGATORS:%d \n_____________\n", nr_aggr);
    for(i = 0; i < nprocs; i++)
      if (aggr_table[i])
	printf("%d ", i);
    printf("\n");
  }
#endif	
  return nr_aggr;
}


// Read aggregators from a file
int get_aggregators3(int *aggr_table, int bg_nodes_pset){
  int nprocs, i, nr_aggr, myrank;

  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

  nr_aggr = (bg_nodes_pset + 1) * get_count_psets(am_i_bridge());  

  for(i=0;i<nprocs;i++)
    aggr_table[i]=0;

  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  
  fp = fopen("aggregator_list", "r");
  if (fp == NULL)
    exit(EXIT_FAILURE);
  
  while ((read = getline(&line, &len, fp)) != -1) {
    //printf("Retrieved line of length %zu :\n", read);
    char *token;
    char *state;
    int i= 0, nproc, naggr;
    for (token = strtok_r(line, " ", &state);
	 token != NULL;
	 token = strtok_r(NULL, " ", &state), i++)
      {
	if (i==0) {
	  nproc = atoi(token);
	  if (nproc != nprocs) 
	    break;
	}
	else
	  if (i==1) {
	    naggr = atoi(token);
	    if (naggr != nr_aggr)
	      break;
	  }
	  else {
	    assert(atoi(token) < nprocs);
	    aggr_table[atoi(token)]=1;
	  }
      }
    //break;
    //printf("i-2=%d nr_aggr=%d\n",i-2, nr_aggr );
    //assert(i-2 == naggr);
  }
 
//  if (myrank == 0) {
//        printf("nprocs=%d nr_aggr=%d bg_nodes_pset=%d\n",nprocs, nr_aggr, bg_nodes_pset);
//  }
#ifdef TRACE_ON 
  if (myrank==0){
    printf("AGGREGATORS:%d \n_____________\n", nr_aggr);
    for(i=0;i<nprocs;i++)
      if (aggr_table[i])
	printf("%d ", i);
    printf("\n");
  }
#endif
  return nr_aggr;
}

*/



