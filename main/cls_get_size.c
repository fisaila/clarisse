#include <sys/stat.h>

#include "client_iocontext.h"
#include "hdr.h"
#include "util.h"
#include "error.h"

extern client_global_state_t client_global_state;
extern int i_am_client;

int MPI_File_get_size(MPI_File fh, MPI_Offset *size) {
  return MPI_File_get_size_clarisse(fh, size);
}

int MPI_File_get_size_clarisse(MPI_File fh, MPI_Offset *size) {
  int ret = MPI_SUCCESS;
  
  if (i_am_client) {
    if (client_global_state.clarisse_pars.collective ==  CLARISSE_ROMIO_COLLECTIVE) 
      ret = PMPI_File_get_size(fh, size);
    else {

      client_iocontext_t *c_ctxt;
      c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
      
      if (c_ctxt) {
	if (client_global_state.clarisse_pars.coupleness == CLARISSE_COUPLED) {
	  struct stat s;
	  if (c_ctxt->fd_sys < 0)
	    fstat(c_ctxt->fd_sys, &s);
	  else 
	    lstat(c_ctxt->filename, &s);
	  *size = s.st_size;
	}
	else {
	  struct rq_file_get_size rq;
	  int log_ios_rank;
	  MPI_Datatype d;
	  
	  net_dt_update(&c_ctxt->net_dt, (char *)&rq, sizeof(struct rq_file_get_size)); 
	  net_dt_update(&c_ctxt->net_dt, (char *)c_ctxt->filename, strlen(c_ctxt->filename) + 1); 
	  d = net_dt_commit(&c_ctxt->net_dt);	
	  rq.global_descr = c_ctxt->global_descr;
	  
	  for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++) {
	    if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	      ret = MPI_Send(&rq, 1, d, client_global_state.log2ph[log_ios_rank], RQ_FILE_GET_SIZE, client_global_state.initial_cli_serv_intercomm);
	      if (ret != MPI_SUCCESS) {
		handle_error(ret, "Error when sending the reqs");
		return ret;
	      }
	    }
	  }
	  net_dt_reset(&c_ctxt->net_dt);
	  MPI_Type_free(&d);
	  *size = 0;
	  for (log_ios_rank = 0; log_ios_rank < c_ctxt->clarisse_pars.nr_servers; log_ios_rank++) {
	    struct rt_file_get_size rt;
	    MPI_Status status;
	    if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	      ret = MPI_Recv(&rt, sizeof( struct rt_file_get_size), MPI_BYTE, MPI_ANY_SOURCE /*client_global_state.log2ph[log_ios_rank]*/, RT_FILE_GET_SIZE, client_global_state.initial_cli_serv_intercomm, &status);
	      if (ret != MPI_SUCCESS) {
		handle_error(ret, "Error when re the reqs");
		return ret;
	      }
	      *size = MAX(*size, rt.size);
	    }
	  }	
	  ret = MPI_SUCCESS;
	}
      }
      else
	//ret = PMPI_File_get_size(fh, size);
	ret = MPI_ERR_BAD_FILE;
    }
  }
  return ret;
}
