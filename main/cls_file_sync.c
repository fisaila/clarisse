#include <sys/stat.h>

#include "client_iocontext.h"
#include "hdr.h"
#include "util.h"
#include "error.h"

extern client_global_state_t client_global_state;
extern int i_am_client;


int cli_flush(client_iocontext_t *c_ctxt);

int MPI_File_sync(MPI_File fh) {
  return MPI_File_sync_clarisse(fh);
}


int MPI_File_sync_clarisse(MPI_File fh) {
  int ret = MPI_SUCCESS;

#ifdef CLARISSE_DEBUG
  printf("MPI_File_sync\n");
#endif

  if (client_global_state.clarisse_pars.collective ==  CLARISSE_ROMIO_COLLECTIVE) 
    ret = PMPI_File_sync(fh);
  else {
    client_iocontext_t *c_ctxt;

    c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
    if (c_ctxt)
      ret = cli_flush(c_ctxt);
  }
  return ret;
}


int cli_flush(client_iocontext_t *c_ctxt) {

  int ret = 0;

  if ((c_ctxt->clarisse_pars.coupleness != CLARISSE_COUPLED) &&
      (c_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_ON_CLOSE)) {
    int log_ios_rank, i;
    MPI_Request *requests;
    int errn;
    MPI_Status status;
    char **irq;
    
    //printf("CLIENT sends FLUSH to server\n"); 
    requests = (MPI_Request *) clarisse_malloc(client_global_state.active_nr_servers * sizeof(MPI_Request));
    irq = clarisse_malloc(sizeof(char *) * client_global_state.active_nr_servers); 
    for (log_ios_rank = 0, i = 0; log_ios_rank < client_global_state.clarisse_pars.nr_servers; log_ios_rank++) {
      if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	struct rq_file_flush *rq;
	
	rq = clarisse_malloc(sizeof(struct rq_file_flush)); 
	rq->global_descr = c_ctxt->global_descr;
	irq[i] = (char *) rq; 
	
	ret = MPI_Isend(irq[i], sizeof(struct rq_file_flush), MPI_BYTE, client_global_state.log2ph[log_ios_rank], RQ_FILE_FLUSH, client_global_state.initial_cli_serv_intercomm, requests + i);
	if (ret != MPI_SUCCESS) {
	  handle_error(ret, "Error when sending the reqs");
	  return ret;
	}
	i++;
      }
    }
    errn = 0;
    for (log_ios_rank = 0; log_ios_rank < client_global_state.clarisse_pars.nr_servers; log_ios_rank++) {
      if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	struct rt_file_flush fd_ret;
	ret = MPI_Recv(&fd_ret, sizeof(struct rt_file_delete), MPI_BYTE,  client_global_state.log2ph[log_ios_rank], RT_FILE_FLUSH, client_global_state.initial_cli_serv_intercomm, &status);
	if (ret != MPI_SUCCESS) {
	  handle_error(ret, "cls_close: Error receiving the flush rets");
	  return ret;
	}
	errn = MIN(errn, fd_ret.errn);
      }
    }
    MPI_Waitall(client_global_state.active_nr_servers, requests, MPI_STATUSES_IGNORE);
    if (ret != MPI_SUCCESS)
      handle_error(ret, "cls_close: waitall error\n");
    clarisse_free(requests);
    if (errn == 0)
      ret = MPI_SUCCESS;
    else
      ret = MPI_ERR_FILE;
  }
  return ret;
}
