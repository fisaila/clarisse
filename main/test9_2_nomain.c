#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "test_datatypes.h"
#include "test_commons.h" 
#include "client_iocontext.h"


#define WR 0
#define WRRD 1
#define RD 2

#define DTYPE_BYTE 0
#define DTYPE_BYTE_VECTOR 1

#define DO_NOT_ADD_RANK_TO_DISPL 0
#define ADD_RANK_TO_DISPL 1
double *write_iter_time;
double *read_iter_time;

char *buf1;
char *buf2;

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);


static MPI_Datatype get_dtype_displ(int type, int nprocs, int myrank, long long int size,  int add_rank, MPI_Offset *displ) {
  MPI_Datatype d;
  
  switch(type) {
  case DTYPE_BYTE: {
    d = MPI_BYTE;
    if (add_rank == ADD_RANK_TO_DISPL)
      *displ = *displ + (long long int) myrank * size;
    break;
  }
  case DTYPE_BYTE_VECTOR: {
    d = get_vec_nstride(nprocs, size);
    if (add_rank == ADD_RANK_TO_DISPL)
      *displ = *displ + myrank;
    break;
  }
  }
  return d;
}

static void setview_exec(char *filename, int access_type, MPI_Datatype d, MPI_Offset displ, MPI_Info info, int n) {
  int myrank, err;
  MPI_File fh;
  MPI_Status status;
  MPI_Comm intracomm = cls_get_client_intracomm();
  
 
  MPI_Comm_rank(intracomm, &myrank);  

  if ((access_type == WRRD) || (access_type == WR)){
    MPI_Barrier(intracomm);
  
    err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
			info, &fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_open");
    
    err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_set_view");
    
    err = MPI_File_write_all(fh, buf1, n, MPI_BYTE, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_write_all");

    err = MPI_File_close(&fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_close");
  }

  if ((access_type == WRRD) || (access_type == RD)){
    MPI_Barrier(intracomm);
    err = MPI_File_open(intracomm, filename, MPI_MODE_RDWR ,
			info, &fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_open");
   
    err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_set_view"); 
    
    err = MPI_File_read_at_all(fh, 0, buf2, n, MPI_CHAR, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_read_all");

    err = MPI_File_close(&fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_close");
 }
}


static void test9_2_write_read(char *filename, int dtype, int add_rank, long long int size, int sleep_before_write_read, int type) {
  int myrank, nprocs;
  MPI_Comm intracomm;
  MPI_Offset displ = 0; //(long long int)2*1024*1024*1024;//0;

  intracomm = cls_get_client_intracomm();
  MPI_Comm_rank(intracomm, &myrank);
  MPI_Comm_size(intracomm, &nprocs);

  if (myrank == 0) 
    printf("Client %d: nprocs = %d %s %lld bytes (%lld bytes / proc)\n", cls_get_client_id(), nprocs, (type == WR)?"writes":"reads", size * nprocs, size);
  
  MPI_Datatype d = get_dtype_displ(dtype, nprocs, myrank, size, add_rank, &displ); 
  sleep(sleep_before_write_read);
  //printf("Client %d:rank %d  nprocs = %d %s %lld bytes (%lld bytes / proc) at displ %lld\n", cls_get_client_id(), myrank, nprocs, (type == WR)?"writes":"reads", size * nprocs, size, displ);
  setview_exec(filename, type, d, displ, MPI_INFO_NULL, size);
  free_datatype(d);

  setenv("CLARISSE_TEST", __FILE__, 1);
}


void test9_2_write(char *filename, long long int size, int t) {
  write_iter_time[t] = MPI_Wtime();
  MPI_Barrier (cls_get_client_intracomm());
  test9_2_write_read(filename, DTYPE_BYTE, ADD_RANK_TO_DISPL, size, 0, WR);
  MPI_Barrier (cls_get_client_intracomm());
  write_iter_time[t] = MPI_Wtime() - write_iter_time[t] ;
}


void test9_2_read(char *filename, long long int size, int t) {
  read_iter_time[t] = MPI_Wtime();
  MPI_Barrier (cls_get_client_intracomm());
  test9_2_write_read(filename, DTYPE_BYTE, ADD_RANK_TO_DISPL, size, 0, RD);
  MPI_Barrier (cls_get_client_intracomm());
  read_iter_time[t] = MPI_Wtime() - read_iter_time[t] ;
}

void test9_2_comp(double comp_time){
  double t0;
  
  t0 = MPI_Wtime();
  while ((MPI_Wtime() - t0) < comp_time);
}

static void verification_step(int step, long long int size){
  int i;

  for (i = 0; i < size; i++) {
    if (buf1[i] != buf2[i]) {
      printf("VERIFICATION FAILED IN STEP %d: x[%d] = %c != read num_particles x[%d] = %c\n", step, i, buf1[i], i, buf2[i]); 
    return;
    }
  }
  
  printf("VERIFICATION SUCCESSFUL IN STEP %d\n", step);  
}



static void PrintUsage (int rank)
{
    if (rank == 0) {
        printf ("Usage: -n, -t, -c and (-w or -r)  are required!)\n");
        printf (" -n  size/process to R/W in MBytes \n");
        printf (" -t  timesteps \n");
        printf (" -c  compute time per step\n");
        printf (" -w  perform write\n");
	printf (" -r  perform read\n");
	printf (" -f  use the file system (needs synchronization)\n");
	printf (" -v  perform verification of write results\n");
	
    }
    MPI_Finalize();
    exit (EXIT_SUCCESS);
}



void test9_2(int argc, char *argv[]){

  char *filename = argv[1];
  int my_rank, num_procs;
  int i, check = 0;
  int t, timesteps;
  double timestep_time;
  int read_flag = 0, write_flag = 0, fs_flag = 0, verification_flag = 0;
  int read_or_write = 0;
  long long int size;
  long long int total_file_size;

  MPI_Comm_rank (cls_get_client_intracomm(), &my_rank);
  MPI_Comm_size (cls_get_client_intracomm(), &num_procs);
  

  i = 1;
  while (i < argc){
    if (strcmp(argv[i],"-n") == 0){
      i++;
      size = (long long int) atof (argv[i]) * 1024 * 1024;
      check++;
    }
    else if (strcmp(argv[i],"-t") == 0){
      i++;
      timesteps = atoi (argv[i]);
      check++;
    }
    else if (strcmp(argv[i],"-c") == 0){
      i++;
      timestep_time = atof (argv[i]);
      check++;
    }
    else if (strcmp(argv[i],"-r") == 0){
      read_flag = 1;
      if (read_or_write == 0) {
	check++;
	read_or_write = 1;
      }
    }
    else if (strcmp(argv[i],"-w") == 0){
      write_flag = 1;
      if (read_or_write == 0) {
	check++;
	read_or_write = 1;
      }
    }
    else if (strcmp(argv[i],"-f") == 0){
      fs_flag = 1;
    }
    else if (strcmp(argv[i],"-v") == 0){
      verification_flag = 1;
    }   
    else if (strcmp(argv[i],"--help") == 0){
	PrintUsage (my_rank);
    }
    i++;
  }
  
  // At least 4 parameters (n, t, c and (r or/and w)
  if (check != 4) {
    if (my_rank == 0) 
      PrintUsage(my_rank);
      //printf("Wrong number of params, call %s filename numparticles (default=8) timesteps timestep_time (seconds)", argv[0]); 
    MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
    MPI_Finalize();
  }

  total_file_size = (long long int) size * (long long int)num_procs/1024/1024;
  buf1 = (char *)malloc(size * sizeof(char));
  buf2 = (char *)malloc(size * sizeof(char));
  for (i = 0; i < size; i++)
    buf1[i] = 'a' + i % 17;
  bzero(buf2, size);
  
  if (my_rank == 0) {printf ("Total file size: %lld MB\n", total_file_size);}
  

  //int size, int sleep_before_write, int sleep_before_read,  int workflow_flag, int steps) {
  double total_time;
  write_iter_time = (double *) malloc(timesteps * sizeof(double));
  read_iter_time = (double *) malloc(timesteps * sizeof(double));
  MPI_Barrier (cls_get_client_intracomm());
  total_time = MPI_Wtime();
  MPI_Barrier (cls_get_client_intracomm());
  
  for (t = 0; t < timesteps; t++) {
    test9_2_comp(timestep_time);
 
    if (write_flag) 
      test9_2_write(filename, size, t);
    
    if (fs_flag) // && (write_flag != read_flag))
      MPI_Barrier(cls_get_allclients_intracomm());    

    if (read_flag)
      test9_2_read(filename, size, t);

    if (verification_flag && read_flag)
      verification_step(t, size);
  }

  MPI_Barrier (cls_get_client_intracomm());
  total_time = MPI_Wtime() - total_time;

  double max_total_time;
  MPI_Comm comm = cls_get_client_intracomm();

  for (i = 0; i < timesteps; i++) {
    if (write_flag) {
      double write_max_time, write_min_time, write_avg_time;
      MPI_Reduce(&write_iter_time[i], &write_max_time, 1, MPI_DOUBLE, MPI_MAX, 0, comm);
      MPI_Reduce(&write_iter_time[i], &write_min_time, 1, MPI_DOUBLE, MPI_MIN, 0, comm);
      MPI_Reduce(&write_iter_time[i], &write_avg_time, 1, MPI_DOUBLE, MPI_SUM, 0, comm);
      if (my_rank == 0) {
	printf("\tWrite time iteration %d: AVG=%g MIN=%g MAX=%g\n", i, write_avg_time / num_procs,  write_min_time,  write_max_time);
	printf("\tWrite throughput iteration %d = %g MBytes/s size =%lld MB\n", i, total_file_size/ write_avg_time, total_file_size); 
      }
    }
    if (read_flag){
      double read_max_time, read_min_time, read_avg_time;
      MPI_Reduce(&read_iter_time[i], &read_max_time, 1, MPI_DOUBLE, MPI_MAX, 0, comm);
      MPI_Reduce(&read_iter_time[i], &read_min_time, 1, MPI_DOUBLE, MPI_MIN, 0, comm);
      MPI_Reduce(&read_iter_time[i], &read_avg_time, 1, MPI_DOUBLE, MPI_SUM, 0, comm);
      if (my_rank == 0) {
	printf("\tRead time iteration %d: AVG=%g MIN=%g MAX=%g\n", i, read_avg_time / num_procs,  read_min_time,  read_max_time);
	printf("\tRead throughput iteration %d = %g MBytes/s size =%lld MB\n", i, total_file_size/ read_avg_time, total_file_size); 
      }
    }
    
  }
  
  MPI_Reduce(&total_time, &max_total_time, 1, MPI_DOUBLE, MPI_MAX, 0, comm);
  if (my_rank == 0) {
    printf("Total time  %g s\n", max_total_time);
    //printf("%s bandwidth iteration %d = %g MB/s size = %g MB time = %g s\n", (type == WRITEBLOCK)?"Write":"Read", iter, size / time, size, time);
  }
  
  free(buf1);
  free(buf2);
  free(write_iter_time);
  free(read_iter_time);
}



