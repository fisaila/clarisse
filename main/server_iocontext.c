#include <assert.h>
#include "server_iocontext.h"
#include "error.h"
#include "uthash.h"
#include "hdr.h"
#include "util.h"
#include "buffer.h"
#include "interval_list.h"

extern server_global_state_t server_global_state;

server_iocontext_t * server_iocontext_find(int global_descr) {
  server_iocontext_t *s_ctxt;
    /*for (aux = server_global_state.open_files.head; (aux!= NULL) && (((server_iocontext_t *)aux)->local_target.global_descr != global_descr); aux = aux->next); 
  return (server_iocontext_t *) aux;
    */
  HASH_FIND_INT(server_global_state.s_ctxt_map, &global_descr, s_ctxt);
  return s_ctxt;
}

server_iocontext_t * server_iocontext_find_del(int global_descr) {
  server_iocontext_t *s_ctxt;
  
  s_ctxt = server_iocontext_find(global_descr);
  HASH_DEL(server_global_state.s_ctxt_map, s_ctxt);
  return s_ctxt;
}

/*
server_iocontext_t * server_iocontext_alloc_init(clarisse_target_t *local_target, int partitioning_stride_factor, int nprocs) { 
  server_iocontext_t * s_ctxt;
  
  s_ctxt = server_iocontext_init(local_target->global_descr);
  server_iocontext_update(s_ctxt, local_target, partitioning_stride_factor, nprocs);
  return s_ctxt;
}
*/
server_iocontext_t * server_iocontext_init(int global_descr) { 
  server_iocontext_t * s_ctxt;
  int client_idx;
  int concurrency;

  s_ctxt = (server_iocontext_t *) clarisse_malloc(sizeof(server_iocontext_t));
  memcpy(&s_ctxt->clarisse_pars, &server_global_state.clarisse_pars, sizeof(clarisse_params_t));
  //s_ctxt->intracomm = server_global_state.intracomm;
  s_ctxt->global_descr = global_descr; // ***
  s_ctxt->local_target = NULL; // ***
  if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK)
    concurrency = DCACHE_CONCURRENT_ACCESS;
  else
    concurrency = DCACHE_NONCONCURRENT_ACCESS;
  dcache_init(&s_ctxt->dcache, s_ctxt->clarisse_pars.max_dcache_size, concurrency);
  if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK)
    tpool_task_queue_create(server_global_state.tpool, (void*)s_ctxt, 0);
  
  s_ctxt->client_rt_info = (client_rt_info_t *) clarisse_malloc(server_global_state.nr_clients * sizeof(client_rt_info_t));
  for (client_idx = 0; client_idx < server_global_state.nr_clients; client_idx++) { 
    //ctxt->client_rt_info[client_idx].intercomm = server_global_state.intercomms[client_idx];
    s_ctxt->client_rt_info[client_idx].client_nprocs = -1;
    s_ctxt->client_rt_info[client_idx].remote_view_distr = NULL;
    s_ctxt->client_rt_info[client_idx].partitioning_stride_factor = -1;

    if (s_ctxt->clarisse_pars.coupleness != CLARISSE_COUPLED) {
      s_ctxt->client_rt_info[client_idx].crt_write_ops = clarisse_malloc(server_global_state.client_nprocs[client_idx] * sizeof(short int));
      memset((void *)s_ctxt->client_rt_info[client_idx].crt_write_ops, 0, server_global_state.client_nprocs[client_idx] * sizeof(short int));
      s_ctxt->client_rt_info[client_idx].crt_arrived_writes = 0;
      s_ctxt->client_rt_info[client_idx].next_write_ops = clarisse_malloc(server_global_state.client_nprocs[client_idx] * sizeof(short int));
      memset((void *)s_ctxt->client_rt_info[client_idx].next_write_ops, 0, server_global_state.client_nprocs[client_idx] * sizeof(short int));
      s_ctxt->client_rt_info[client_idx].next_arrived_writes = 0;
    }
    
    net_dt_init_alloc(&s_ctxt->client_rt_info[client_idx].net_dt, 
		      s_ctxt->clarisse_pars.net_datatype_count, 
		      s_ctxt->clarisse_pars.net_datatype_incr);
    s_ctxt->client_rt_info[client_idx].mpi_rq_map = NULL;
    alloc_pool_init(&s_ctxt->client_rt_info[client_idx].mpi_rq_pool, s_ctxt->clarisse_pars.nr_servers, s_ctxt->clarisse_pars.nr_servers, sizeof(MPI_Request));
    s_ctxt->read_futures = NULL;
  }
  HASH_ADD_INT(server_global_state.s_ctxt_map, global_descr, s_ctxt);  
#ifdef CLARISSE_EXTERNAL_BUFFERING
  s_ctxt->shared_buffer_pool = NULL;
#endif
  return s_ctxt;
}

void server_iocontext_update_target(server_iocontext_t *s_ctxt, clarisse_target_t *local_target) { 
   assert(s_ctxt->global_descr == local_target->global_descr);
  s_ctxt->local_target = local_target;

}


void server_iocontext_update_client_rt_info(server_iocontext_t *s_ctxt, int client_idx, int partitioning_stride_factor, int client_nprocs) { 
  int i; 
  s_ctxt->client_rt_info[client_idx].client_nprocs = client_nprocs;
  if (!s_ctxt->client_rt_info[client_idx].remote_view_distr) {
    s_ctxt->client_rt_info[client_idx].remote_view_distr = (distrib_t **) clarisse_malloc(client_nprocs * sizeof(distrib_t *));
    for (i = 0; i < client_nprocs; i++)
      s_ctxt->client_rt_info[client_idx].remote_view_distr[i] = distrib_get(MPI_BYTE, 0);
  }
  if (s_ctxt->clarisse_pars.file_partitioning == CLARISSE_STATIC_FILE_PARTITIONING)
    s_ctxt->client_rt_info[client_idx].partitioning_stride_factor = partitioning_stride_factor;
  else
    s_ctxt->client_rt_info[client_idx].partitioning_stride_factor = 1;
}

void server_iocontext_update(server_iocontext_t *s_ctxt, clarisse_target_t *local_target, int client_idx, int partitioning_stride_factor, int client_nprocs) { 
  server_iocontext_update_target(s_ctxt, local_target);
  server_iocontext_update_client_rt_info(s_ctxt, client_idx, partitioning_stride_factor, client_nprocs);
}


void server_iocontext_free(server_iocontext_t * s_ctxt) {
  int client_idx;
  int  i;

  //  dllist_rem(&server_global_state.open_files, (struct dllist_link *)s_ctxt);
  //free_datatype(s_ctxt->local_target->distr->flat->id);
  //distrib_free_content(&s_ctxt->local_target->distr);
  if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK)
    tpool_task_queue_destroy(server_global_state.tpool, (void*)s_ctxt);

  for (client_idx = 0; client_idx < server_global_state.nr_clients; client_idx++) { 
    if (s_ctxt->client_rt_info[client_idx].remote_view_distr != NULL) {
      for (i = 0; i < s_ctxt->client_rt_info[client_idx].client_nprocs; i++){
	free_datatype(s_ctxt->client_rt_info[client_idx].remote_view_distr[i]->flat->id);
	distrib_free(s_ctxt->client_rt_info[client_idx].remote_view_distr[i]);
      }
      clarisse_free(s_ctxt->client_rt_info[client_idx].remote_view_distr);
    }
    if (s_ctxt->clarisse_pars.coupleness != CLARISSE_COUPLED) {
      clarisse_free(s_ctxt->client_rt_info[client_idx].crt_write_ops);
      clarisse_free(s_ctxt->client_rt_info[client_idx].next_write_ops);
    }
    net_dt_free(&s_ctxt->client_rt_info[client_idx].net_dt);
    alloc_pool_free(&s_ctxt->client_rt_info[client_idx].mpi_rq_pool);
  }
  clarisse_free(s_ctxt->client_rt_info );
  dcache_free(&s_ctxt->dcache);
#ifdef CLARISSE_EXTERNAL_BUFFERING
  shared_buffer_pool_free(s_ctxt->shared_buffer_pool);
#endif
  clarisse_free(s_ctxt);
}


int server_global_state_init(clarisse_params_t *clarisse_pars, MPI_Comm intracomm, int nr_clients, MPI_Comm *intercomms) {
  if (server_global_state.initialized == 0) {
    int concurrency;
    memcpy(&server_global_state.clarisse_pars, clarisse_pars, sizeof(clarisse_params_t));
    server_global_state.active_targets = NULL;
    server_global_state.s_ctxt_map = NULL;
    dllist_init(&server_global_state.write_task_queue);
    request_queues_init(&server_global_state.read_task_queue, 
			server_global_state.clarisse_pars.nr_servers, 
			server_global_state.clarisse_pars.io_sched_policy) ;

    if (clarisse_pars->write_type == CLARISSE_WRITE_BACK)
      concurrency = BPOOL_CONCURRENT_ACCESS; 
    else
      concurrency = BPOOL_NONCONCURRENT_ACCESS; 
    server_global_state.buf_pool = 
      bufpool_init_static(concurrency, server_global_state.clarisse_pars.buffer_size,
			 server_global_state.clarisse_pars.bufpool_size);
#ifdef CLARISSE_EXTERNAL_BUFFERING
    if (server_global_state.clarisse_pars.external_buffering == CLARISSE_EXTERNAL_BUFFERING_ENABLED)
      cls_init_buffering(&server_global_state.bufservice, server_global_state.clarisse_pars.buffer_size, server_global_state.clarisse_pars.max_bufpool_size); //max_elems=8
#endif
    server_global_state.net_buf = (char *) clarisse_malloc(server_global_state.clarisse_pars.net_buf_size);

    if ((server_global_state.clarisse_pars.write_type == CLARISSE_WRITE_BACK) ||
	(server_global_state.clarisse_pars.aggr_thread == CLARISSE_AGGR_THREAD))
      tpool_init(&server_global_state.tpool, CLARISSE_DEFAULT_SERVER_WORKER_THREADS, CLARISSE_DEFAULT_SERVER_TASK_QUEUE_SIZE, 0);  
    if (server_global_state.clarisse_pars.aggr_thread == CLARISSE_AGGR_THREAD)
      tpool_task_queue_create(server_global_state.tpool, (void*)0, 2);
    server_global_state.intracomm = intracomm;

    server_global_state.nr_clients = nr_clients;
    server_global_state.nr_active_clients = 0;
    if (server_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS)
      server_global_state.intercomms = (MPI_Comm *) clarisse_malloc(server_global_state.nr_clients * sizeof(MPI_Comm));
    else
      server_global_state.intercomms = intercomms;
    server_global_state.total_client_procs = 0;
    server_global_state.client_nprocs = (int *) clarisse_malloc(server_global_state.nr_clients * sizeof(int));

    if (server_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS)
      server_global_state.total_client_procs = 0;
    else {
      int client_idx;
      for (client_idx = 0; client_idx < server_global_state.nr_clients; client_idx++) {
	if (server_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM)
	  MPI_Comm_remote_size(server_global_state.intercomms[client_idx], &server_global_state.client_nprocs[client_idx]);
	else
	  MPI_Comm_size(server_global_state.intercomms[client_idx], &server_global_state.client_nprocs[client_idx]);
	server_global_state.total_client_procs += server_global_state.client_nprocs[client_idx];
      }
    }
    server_global_state.finalized_cnt = 0;
    server_global_state.controller_rank = 0;
    server_global_state.global_controller_rank = 0;
    server_global_state.initialized = 1;
    return 0;
  }
  else
    return -1;
}



void server_global_state_free() {
  if (server_global_state.initialized == 1) {
    if ((server_global_state.clarisse_pars.write_type == CLARISSE_WRITE_BACK) ||
	(server_global_state.clarisse_pars.aggr_thread == CLARISSE_AGGR_THREAD))  
      tpool_destroy(server_global_state.tpool, 1);
    if ((server_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS) || 
	(server_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM)){
      server_iocontext_t *s_ctxt, *tmp;
      clarisse_target_t *tgt, *tmp2;
      int client_idx;
      //printf("CLARISSE server: Freeing intercomm\n");
      for (client_idx = 0; client_idx < server_global_state.nr_clients; client_idx++)
	MPI_Comm_free(&(server_global_state.intercomms[client_idx]));    
      MPI_Comm_free(&server_global_state.intracomm);
      HASH_ITER(hh, server_global_state.s_ctxt_map, s_ctxt, tmp){
	HASH_DEL(server_global_state.s_ctxt_map, s_ctxt);
	flush_buffers(s_ctxt);
	server_iocontext_free(s_ctxt);
      }
      HASH_ITER(hh, server_global_state.active_targets, tgt, tmp2){
	HASH_DEL(server_global_state.active_targets, tgt);
	clarisse_free(tgt);
      }
    }
    request_queues_free(&server_global_state.read_task_queue);
   //request_queues_free(&server_global_state.state.write_task_queue);
    bufpool_free(server_global_state.buf_pool);
#ifdef CLARISSE_EXTERNAL_BUFFERING
    if (server_global_state.clarisse_pars.external_buffering == CLARISSE_EXTERNAL_BUFFERING_ENABLED)
      cls_destroy_buffering(&server_global_state.bufservice); //max_elems=8
#endif
    clarisse_free(server_global_state.net_buf);    
    clarisse_free(server_global_state.intercomms);
    clarisse_free(server_global_state.client_nprocs);
    server_global_state.initialized = 0;
  }
}


#ifdef CLARISSE_EXTERNAL_BUFFERING
shared_buffer_t *shared_buffer_alloc_init(clarisse_off_t block_off) {
  shared_buffer_t *sb = (shared_buffer_t *) clarisse_malloc(sizeof(shared_buffer_t));
  sb->block_off = block_off; 
  sb->coll_op.cnt_arrived_producers = 0;
  sb->coll_op.total_producers = 0;
  sb->coll_op.cnt_arrived_consumers = 0;
  sb->coll_op.total_consumers = 0;
  dllist_init(&sb->coll_op.off_len_lists);
  sb->ind_write_flag = 0;
  dllist_init(&sb->ind_write_interval_list);
  dllist_init(&sb->ind_read_off_len_list);
  return sb;
}

void shared_buffer_print(shared_buffer_t *sb) {
  struct dllist_link *aux; 
  printf("\tsb: offset=%lld\t", (long long int)sb->block_off);
  for(aux = sb->coll_op.off_len_lists.head; aux != NULL; aux = aux->next) {
    off_len_block_llint_node_t *olbn = DLLIST_ELEMENT(aux, off_len_block_llint_node_t, link);
    int i;
    for (i = 0; i < olbn->metadata_size; i++)
      printf("(%lld,%lld) ", olbn->offs[i], olbn->lens[i]); 
    
  }   
  printf("\n");
}

void shared_buffer_free(shared_buffer_t *sb){  
  interval_list_free(&sb->ind_write_interval_list);
  clarisse_free(sb);
}


void shared_buffer_pool_print(shared_buffer_t *sbp) {
  shared_buffer_t *aux;
  printf("sbp:\n");
  for (aux = sbp; aux != NULL; aux = aux->hh.next)
    shared_buffer_print(aux);
}

void shared_buffer_pool_free(shared_buffer_t *sbp){ 
  shared_buffer_t *sb, *tmp;
  HASH_ITER(hh, sbp, sb, tmp) {
    HASH_DEL(sbp, sb);
    shared_buffer_free(sb);
  }
}
#endif
