#ifndef CLIENT_IOCONTEXT_H
#define CLIENT_IOCONTEXT_H

#ifdef CLARISSE_IOLOGY
#include "ciology.h"
#endif

#ifdef CLARISSE_EXTERNAL_MONITORING
#include "evp_monitoring.h"
#endif

#include <sys/uio.h>
#include <fcntl.h>

#include "dcache.h"
#include "request_queues.h"
#include "list.h"
#include "clarisse.h"
#include "net_dt.h"
#include "target.h"
#include "uthash.h"
#include "alloc_pool.h" 
#include "cls_lm.h"
#include "off_len.h"

typedef struct {
  UT_hash_handle hh;
  int ph_ios_rank;
  int log_ios_rank;
} int_map_t;


typedef struct {
  UT_hash_handle hh;
  MPI_Request *mpi_rq;
  MPI_Datatype mpi_datatype;
  char *data_rq;
  struct dllist *off_len_block_list;
} mpi_rq_map_t;


#ifdef CLARISSE_IOLOGY

#define CLS_MAX_RECORDED_OPS 5

typedef struct {
  int nr_ops;
  int crt_op;
  off64_t offset[CLS_MAX_RECORDED_OPS];
  size_t size[CLS_MAX_RECORDED_OPS];
  double t_begin[CLS_MAX_RECORDED_OPS];
  double t_end[CLS_MAX_RECORDED_OPS];

  double rel_pred_error_offset[CLS_MAX_RECORDED_OPS]; // abs(predicted - measured)/measured  
  double rel_pred_error_size[CLS_MAX_RECORDED_OPS]; // abs(predicted - measu  
  double rel_pred_error_elapsed[CLS_MAX_RECORDED_OPS]; // abs(predicted - measured)/measured  
  double rel_pred_error_duration[CLS_MAX_RECORDED_OPS]; // abs(predicted - measured)/measured
  

} cls_real_t;

#define CLS_REL_PRED_ERR(measured, predicted) (((fabs((measured) - (predicted)))/(measured))*100)

typedef struct {
  intptr_t prediction; 
  iology_op_t type;
  int fd;
  int64_t sym;
  size_t size;
  off64_t offset;
  int whence;
  double elapsed;
  double duration;
} cls_prediction_t;
#endif

// client iocontext
typedef struct {
  UT_hash_handle hh;
  void *key;
  clarisse_target_t *local_target; // memory 
  clarisse_target_t *intermediary_target; // view
  //clarisse_target_t *remote_targets; // distribution over subfiles
  int global_descr;
  MPI_Comm cli_serv_comm; // can be either an inter- or and intra-communicator
  MPI_Comm intracomm; //
  clarisse_params_t clarisse_pars; // nr_ioservers are here
  //int *ph_ios_ranks; // mapping logical to physical ranks
  //int_map_t *ph2log;   // mapping physical to logical ranks
  request_queues_t task_queues; // one task queue for each ios 
  short *pending_ios_ret; // 0: no request pending // 1 :o/w
  int pending_ios;
#ifdef CLARISSE_EXTERNAL_BUFFERING 
  long long int *updated_block_masks;
#endif
  short *view_send;
  char *net_buf;
  struct net_dt net_dt;
  mpi_rq_map_t *mpi_rq_map;
  alloc_pool_t mpi_rq_pool; 
  //temp: for listio read
  //void *crt_iot;
  //clarisse_off_t crt_l_f;
  clarisse_off_t crt_page; // page currenlty used if this client is also aggr

  //***********************************************************
  MPI_File fh; // associate each c_ctxt to fh
  // Mapping from adio

  // int nr_servers; // fd->cb_nodes : global_client_state.clarisse_pars.nr_servers
  //int *ph_ios_ranks; // fd->hints->ranklist: global_client_state.ph_ios_ranks
  //int_map_t *ph2log;   // mapping physical to logical ranks: global_client_state.ps2log
  //MPI_Comm comm; // fd->comm : c_ctxt->intracomm 
  const char *filename; // fd->filename : intermediary target
  int fd_sys; // fd->fd_sys : s_ctxt->local_target->local_handle.fd
  //MPI_Info info; // fd->info : not necessary by now
  MPI_Offset fp_ind;      /* individual file pointer in MPI-IO (in bytes)*/
  MPI_Offset fp_sys_posn; /* current location of the file-pointer in bytes */
  int disp;   // view information
  MPI_Datatype etype; //information      
  MPI_Datatype filetype; // view information    
  MPI_Count etype_size; // view information
  int posix_access_mode;
  int mpi_access_mode;
  int perm;
  //char *io_buf; fd->io_buf : dcache
  // fns : adio functions

  //int coll_op_seq;
  //int last_coll_op_seq; // Last coll_op sequence in the current epoch
  //int epoch;
  //int active_log_ios_rank;

#ifdef CLARISSE_IOLOGY
  cls_prediction_t prediction;
  cls_real_t real;
#endif

} client_iocontext_t;



// client global state
typedef struct client_global_state{
  //clarisse_target_t *target_map;
  client_iocontext_t *c_ctxt_map; // maps a pointer (adio_fd) to a context
  MPI_Comm initial_cli_serv_intercomm;
  MPI_Comm intracomm;
  MPI_Comm allclients_intracomm;
  clarisse_params_t clarisse_pars;
  int active_nr_servers;
  int *active_log_servers; // mapping to active logical servers
  int server_leader; // rank in MPI_COMM_WORLD
  int *log2ph; // mapping logical to physical ranks
  int_map_t *ph2log;   // mapping physical to logical ranks
  int client_id; // starts with 1
  int controller_rank;
  int global_controller_rank;
  int load_injection_coll_seq;
  int load_detection_coll_seq;
  double load;
  int loaded_active_log_ios_rank; // slowed down server;
  int active_log_ios_rank; // ios that will be removed 
  int coll_op_seq;
  int last_coll_op_seq;
  int epoch;
#ifdef CLARISSE_EXPORT_ACCESS_PATTERN
  int access_pattern_log_fd;
#endif
#ifdef CLARISSE_EXTERNAL_MONITORING
  evp_monitoring_t mon;
#endif
  char port[MPI_MAX_PORT_NAME];
  // move here the task queues
} client_global_state_t;

//client_iocontext_t * client_iocontext_alloc_init(clarisse_target_t *local_tgt, clarisse_target_t *intermediary_tgt, /*clarisse_target_t *remote_tgt,*/  MPI_Comm intracomm, MPI_Comm cli_serv_comm);
void client_iocontext_init(client_iocontext_t *c_ctxt, clarisse_target_t *local_tgt,clarisse_target_t *intermediary_tgt);
void client_iocontext_free(client_iocontext_t *);
void client_iocontext_add(client_iocontext_t **c_ctxt_map, void *key, client_iocontext_t *c_ctxt); 
client_iocontext_t *client_iocontext_find(client_iocontext_t *c_ctxt_map, void *key);
void client_iocontext_find_delete_free(client_iocontext_t **c_ctxt_map, void *key);

int clarisse_am_i_coupled_server(client_iocontext_t *c_ctxt); 
int clarisse_am_i_server();

void client_global_state_init();
void client_global_state_free();

void off_len_pack(struct net_dt *net_dt,struct dllist *off_len_block_list);
void off_len_pack_llint(struct net_dt *net_dt,struct dllist *off_len_block_list);
void save_mpi_rq(mpi_rq_map_t **mpi_rq_map, MPI_Request *mpi_rq, MPI_Datatype mpidatatype, char *data_rq, struct dllist *off_len_block_list);
void free_mpi_rq(mpi_rq_map_t **mpi_rq_map);


#endif




