#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include "map.h"
#include "net_dt.h"
/*
  This version constructs a hindexed datatyped, subsequently used for transfers 
*/

void handle_error(int errcode, char *str);

// Scatters/gathers data from/into outbuf 
// type: SCATTER/GATHER copy data
// type: SCATTER_NOCOPY/GATHER_NOCOPY save only the pointers to the data and 
//      blocklens from inbuf (can be used in hindexed operation)
//        
// l,r : absolute positions from the beginning of buffer buf
//        extent=7 
//disp=1 lb=4 up=12
//       -------------------
//   |   |     |     |     |
//   v   v     v     v     v
//  ______***____***____***___
//  0123456790123456790123456790
//  |--------------------------->
//  l=0 ->
// sg_mem(5,15) copies bytes 6, 7, 9, 14, 15 into outbuf 
long long int sg_mem_nocopy (struct net_dt *net_dt, char *inbuf, 
			     long long int l, long long int r, 
			     distrib_t *distr, 
			     int type) {
  long long int cnt = 0;
  flatlist_node_t *flat = distr->flat;

  if (contig_flatlist(flat)) {
    net_dt_update(net_dt, inbuf + l, r - l + 1);
    cnt = r - l + 1;
  }
  else {
    if (r < distr->displ) return 0;
    else r -= distr->displ;
    l = (l < distr->displ) ? 0 : l - distr->displ;
    if (l > r) return 0;
    
    while (l <= r) {
      long long int i = find_bin_search(l %  flat->extent, distr, MAP_CURRENT_BLOCK);
      // if l is posisioned before or at the last active byte of current extent 
      if (i < flat->count ) {
	long long int rel_l = MAX(l %  flat->extent, flat->indices[i]); 
	long long int rel_r = MIN(r , END_BLOCK(l,  flat->extent)) %  flat->extent;
	while (rel_l <= rel_r) {
	  long long int crt_r = MIN (rel_r, flat->indices[i] + flat->blocklens[i] - 1); 
	  switch (type) {
	    /*	case SCATTER:
	    //printf ("distr->displ %lld + BEGIN_BLOCK(l,extent) %lld + rel_l %lld  l %lld\n",distr->displ,  BEGIN_BLOCK(l,extent), rel_l,l);
	    memcpy(inbuf + distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l, outbuf + cnt,  crt_r - rel_l + 1);
	    break;
	    case GATHER:
	    memcpy(outbuf + cnt, inbuf + distr->displ + BEGIN_BLOCK(l,  flat->extent) + rel_l, crt_r - rel_l + 1);
	    break;
	    */
	  case SCATTER_NOCOPY:
	  case GATHER_NOCOPY:
	    net_dt_update(net_dt, inbuf + distr->displ + BEGIN_BLOCK(l, flat->extent) + rel_l, crt_r - rel_l + 1);
	    break;
	  }	
	  cnt += (crt_r - rel_l + 1);
	  if ((++i) < flat->count )
	    rel_l =  flat->indices[i];
	  else
	    break;
	}
      }
      l = BEGIN_NEXT_BLOCK(l, flat->extent);
    }
  }
  return cnt;
}


