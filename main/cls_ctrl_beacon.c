#ifdef CLARISSE_CONTROL
#include "mpi.h"
#include "client_iocontext.h"
#include "cls_ctrl.h" 

#ifdef CLARISSE_BEACON
#include "beacon.h"
BEACON_beep_handle_t beacon_handle;
void load_handler(BEACON_receive_topic_t * caught_topic)
{
  fprintf(stderr,
	  "Received topic details: Name=%s, Beep name=%s, Hostname=%s, Scope = %s, Seqnum=%d Message: %s\n",
	  caught_topic->topic_name, caught_topic->beep_name, caught_topic->incoming_src.hostname, caught_topic->topic_scope, caught_topic->seqnum, caught_topic->topic_payload);     
}
#endif 

extern client_global_state_t  client_global_state;
cls_controller_t cls_controller;
int subscribed_load = 0;


void cls_ctrl_init(){
#ifdef CLARISSE_BEACON
  int ret;
  BEACON_beep_t binfo;
  /* Specify the beep information needed by the BEACON_Connect */
  memset(&binfo, 0, sizeof(binfo));
  strcpy(binfo.beep_version, "1.0");
  //    sprintf(beep_name, "beacon_pub_%d", getpid());
  strcpy(binfo.beep_name, "beacon_clarisse");

  /* Connect to BEACON using BEACON_Connect */
  ret = BEACON_Connect(&binfo, &beacon_handle);
  if (ret != BEACON_SUCCESS) {
    printf("BEACON_Connect is not successful ret=%d\n", ret);
    exit(-1);
  }
#else 

#endif


}
void cls_ctrl_finalize(){
#ifdef CLARISSE_BEACON
  BEACON_Disconnect(beacon_handle);
#else 

#endif

}
void cls_ctrl_publish(char *payload, int payload_size){
#ifdef CLARISSE_BEACON
  BEACON_topic_properties_t eprop;
  int ret;
  strncpy(eprop.topic_payload, payload, payload_size);
  //sprintf(eprop.topic_payload,"PAYLOAD");
  strcpy(eprop.topic_scope, "global"); 
  ret = BEACON_Publish(beacon_handle, "LOAD", &eprop);
  if (ret != BEACON_SUCCESS) {
    printf("BEACON_Publish failed with ret =%d\n", ret);
    exit(-1);
  }
#else 

#endif
  
}
void cls_ctrl_subscribe(){
#ifdef CLARISSE_BEACON
  int myrank;

  MPI_Comm_rank(client_global_state.intracomm, &myrank);
  if ((myrank == 0) && (!subscribed_load)) {
    
    BEACON_subscribe_handle_t shandle;
    int ret;
    char filter_string[] = "topic_scope=global,topic_name=LOAD";
    
    subscribed_load = 1;
    ret = BEACON_Subscribe(&shandle, beacon_handle, filter_string);
    if (ret != BEACON_SUCCESS) {
      printf("BEACON_Subscribe failed ret=%d!\n", ret);
      exit(-1);
    }
    BEACON_Signal(&shandle, beacon_handle, load_handler);
  }
#else

#endif

}

#endif
