#include <assert.h>
#include <errno.h>
#include "buffer.h"
#include "map.h"
#include "client_iocontext.h"
#include "server_iocontext.h"
#include "buffer_pool.h"
#include "tasks.h"
#include "list.h"
#include "hdr.h"
#include "error.h"
#include "marshal.h"
#include "buffer.h"
#include "net_dt.h"
#include "util.h"
#include "alloc_pool.h"
#include "cls_ctrl.h"
#include "clarisse_internal.h"
// WB: 
// Bufpool -> dcache + lru
// dirty: 

#ifdef CLARISSE_TIMING
extern clarisse_timing_t local_timing;
#endif
extern client_global_state_t client_global_state;
#ifdef CLARISSE_CONTROL
extern cls_controller_t cls_controller;
#endif

int client_task_to_write_rq(client_iocontext_t *c_ctxt, struct rq_file_write *rq, struct dllist **off_len_block_list);
int client_drain_task_queues(client_iocontext_t *c_ctxt, int op_type);
static int client_comm_write(client_iocontext_t *c_ctxt);
int client_file_access(client_iocontext_t *c_ctxt, clarisse_off_t l_v, clarisse_off_t r_v, int count, int access_type);


int MPI_File_write_at(MPI_File fh, MPI_Offset offset, const void *buf, int count, MPI_Datatype datatype, MPI_Status *status) {
  return MPI_File_write_at_clarisse(fh, offset, buf, count, datatype, status);
}

int MPI_File_write_at_clarisse(MPI_File fh, MPI_Offset offset, const void *buf, int count, MPI_Datatype datatype, MPI_Status *status) {
  int ret;

  /*
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write < CLARISSE_MAX_CNT_TIMED_OPS) 
    local_timing.time_write[local_timing.cnt_write][0] = MPI_Wtime();
#endif
  */


  if (client_global_state.clarisse_pars.collective ==  CLARISSE_ROMIO_COLLECTIVE) 
    ret = PMPI_File_write_at(fh, offset, buf, count, datatype, status); 
  else {
#ifdef CLARISSE_SAVE_TYPE_IN_FILE
    char *type_buf;
    int type_size;
    printf("WRITE_AT: OFFSET=%lld, COUNT=%d ", offset, count); 
    print_dt(datatype);
    type_buf = clarisse_malloc(1024);
    type_size = decode_pack_dt_out_buf(datatype, &type_buf);
    save_type_in_file("MEMORY", type_buf, type_size, 0);
    clarisse_free(type_buf);
#endif
    client_iocontext_t *c_ctxt;
    c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
    if (c_ctxt) {
      int bytes_xfered, datatype_size;
      bytes_xfered = cls_write(c_ctxt, (char *) buf, count,
			       datatype, CLARISSE_EXPLICIT_OFFSET, offset);
      
      MPI_Type_size(datatype, &datatype_size);
      MPI_Status_set_elements(status, datatype, bytes_xfered / datatype_size);
      ret = MPI_SUCCESS; 
    }
    else
      ret = MPI_ERR_BAD_FILE;
  }
  /*
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write < CLARISSE_MAX_CNT_TIMED_OPS) {
    int size, transfer_count;

    MPI_Get_count(status, datatype, &transfer_count);
    local_timing.time_write[local_timing.cnt_write][1] = MPI_Wtime();
    MPI_Type_size(datatype, &size);
    local_timing.size_write[local_timing.cnt_write] = (double) transfer_count * size;
    local_timing.cnt_write++;
  }
#endif  
  */
  return ret;
}

int MPI_File_write(MPI_File fh, const void *buf, int count, MPI_Datatype datatype, MPI_Status *status) {
  return MPI_File_write_clarisse(fh, buf, count, datatype, status);
}

int MPI_File_write_clarisse(MPI_File fh, const void *buf, int count, MPI_Datatype datatype, MPI_Status *status) {
  int ret;
  /*
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write < CLARISSE_MAX_CNT_TIMED_OPS) 
    local_timing.time_write[local_timing.cnt_write][0] = MPI_Wtime();
#endif
  */
  if (client_global_state.clarisse_pars.collective ==  CLARISSE_ROMIO_COLLECTIVE) 
    ret = PMPI_File_write(fh, buf, count, datatype, status); 
  else {
#ifdef CLARISSE_SAVE_TYPE_IN_FILE
    char *type_buf;
    int type_size;
    printf("WRITE: COUNT=%d ", count); 
    print_dt(datatype);
    type_buf = clarisse_malloc(1024);
    type_size = decode_pack_dt_out_buf(datatype, &type_buf);
    save_type_in_file("MEMORY", type_buf, type_size, 0);
    clarisse_free(type_buf);
#endif
    client_iocontext_t *c_ctxt;
    c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)fh);
    if (c_ctxt) {
      int bytes_xfered, datatype_size;
    
      bytes_xfered = cls_write(c_ctxt, (char *) buf, count,
			       datatype, CLARISSE_INDIVIDUAL,  c_ctxt->fp_ind);
      MPI_Type_size(datatype, &datatype_size);
      MPI_Status_set_elements(status, datatype, bytes_xfered / datatype_size);
      ret = MPI_SUCCESS;
    }
    else
      ret = MPI_ERR_BAD_FILE;
  }
  /*
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_write < CLARISSE_MAX_CNT_TIMED_OPS) {
    int size, transfer_count;

    MPI_Get_count(status, datatype, &transfer_count);
    local_timing.time_write[local_timing.cnt_write][1] = MPI_Wtime();
    MPI_Type_size(datatype, &size);
    local_timing.size_write[local_timing.cnt_write] = (double) transfer_count*size;
    local_timing.cnt_write++;
  }
#endif  
  */
  return ret;
}


int cls_write(client_iocontext_t *c_ctxt, 
	      void *buf, int count,
	      MPI_Datatype datatype, int file_ptr_type,
	      MPI_Offset offset)
{

  /*
#ifdef CLARISSE_CONTROL
  if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED) {
    if ((client_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM) &&
	(client_global_state.clarisse_pars.global_scheduling == CLARISSE_GLOBAL_SCHEDULING_FCFS)) {
      int myrank;

#ifdef CLARISSE_TIMING
      if (local_timing.cnt_write < CLARISSE_MAX_CNT_TIMED_OPS)
	local_timing.time_write_wait[local_timing.cnt_write][0] = MPI_Wtime();
#endif  
      MPI_Comm_rank(client_global_state.intracomm, &myrank);
      if (myrank == cls_controller.rank){
	clarisse_ctrl_event_t *event;
	clarisse_ctrl_msg_io_request_start_t *msg_request_start;
	clarisse_ctrl_msg_t msg;
	msg.type = CLARISSE_CTRL_MSG_IO_REQUEST_START;
	msg_request_start = (clarisse_ctrl_msg_io_request_start_t *)&msg;
	msg_request_start->client_id = client_global_state.client_id; 
	cls_publish(&msg, sizeof(clarisse_ctrl_msg_io_request_start_t));
	event = cls_wait_event(CLARISSE_CTRL_MSG_IO_SCHEDULE);
	clarisse_free(event);
      }
      MPI_Barrier(c_ctxt->intracomm);
#ifdef CLARISSE_TIMING
      if (local_timing.cnt_write < CLARISSE_MAX_CNT_TIMED_OPS)
	local_timing.time_write_wait[local_timing.cnt_write][1] = MPI_Wtime();
#endif  
    }
    if 	(client_global_state.clarisse_pars.elasticity == CLARISSE_ELASTICITY_ENABLED) {
      if (c_ctxt->active_log_ios_rank  == -1) {
	clarisse_ctrl_event_t *event;
	event = cls_test_event(CLARISSE_CTRL_MSG_IO_STOP);

	if (event != NULL) {
	  clarisse_ctrl_msg_t *msg;
	  clarisse_ctrl_msg_io_stop_t *msg_io_stop;
	  clarisse_ctrl_msg_io_current_coll_op_seq_t msg_io_current_coll_op_seq;
	  clarisse_ctrl_msg_io_resume_t *msg_io_resume;
	  communication_point_t dest;
	  //	  printf("%s: Received CLARISSE_CTRL_MSG_IO_STOP\n", __FUNCTION__);
	  msg_io_stop = (clarisse_ctrl_msg_io_stop_t *)&event->msg;
	  c_ctxt->active_log_ios_rank = msg_io_stop->active_log_ios_rank;

	  msg_io_current_coll_op_seq.type = CLARISSE_CTRL_MSG_IO_CURRENT_COLL_OP_SEQ;
	  msg_io_current_coll_op_seq.coll_op_seq = c_ctxt->coll_op_seq;
	  msg = (clarisse_ctrl_msg_t *)&msg_io_current_coll_op_seq;
	  dest.comm = cls_controller.comm;
	  dest.rank = cls_controller.rank;
	  cls_publish_remote(&dest, msg, sizeof(clarisse_ctrl_msg_io_current_coll_op_seq_t));
     
	  event = cls_wait_event(CLARISSE_CTRL_MSG_IO_RESUME);
	  //printf("%s: Received CLARISSE_CTRL_MSG_IO_RESUME coll_op_seq = %d\n", __FUNCTION__,  c_ctxt->coll_op_seq);
	  msg_io_resume = (clarisse_ctrl_msg_io_resume_t *)&event->msg;	  
	  c_ctxt->last_coll_op_seq =  msg_io_resume->last_coll_op_seq;
	}
      
	if (((event == NULL) && (c_ctxt->last_coll_op_seq == c_ctxt->coll_op_seq)) || 
	    ((event != NULL) && (c_ctxt->last_coll_op_seq == c_ctxt->coll_op_seq)))  {
	  cls_active_server_down( c_ctxt->active_log_ios_rank);	
	  c_ctxt->epoch++;
	  c_ctxt->last_coll_op_seq = -1;
	  c_ctxt->coll_op_seq = 0;
	  c_ctxt->active_log_ios_rank  = -1;	  
	  if (client_global_state.active_nr_servers == 0) 
	    handle_error(MPI_ERR_OTHER, "No active server left\n");
	}
      }
    }
  }
#endif    
  */

  int myrank, bytes_xfered;

  c_ctxt->local_target = clarisse_target_alloc_init("", -1, CLARISSE_TARGET_MEMORY, distrib_get(datatype, 0), (void *)buf);	
  MPI_Comm_rank(c_ctxt->intracomm, &myrank);  
  

  if (count == 0)
    bytes_xfered = 0;
  else {
    clarisse_off_t l_v, r_v;
    int bufsize, buftype_size;

    MPI_Type_size(datatype, &buftype_size);
    bufsize = buftype_size * count;
    l_v = (file_ptr_type == CLARISSE_INDIVIDUAL) ? 
      func_sup(c_ctxt->fp_ind, c_ctxt->intermediary_target->distr):
      c_ctxt->etype_size * offset;
    r_v = l_v + bufsize - 1;
    
    //printf("CLS_WRITE: offset interval=(%lld %lld) count=%d\n", offset, offset + count - 1, count);   
    if (client_global_state.clarisse_pars.coupleness == CLARISSE_COUPLED)
      bytes_xfered = client_file_access(c_ctxt, l_v, r_v, count, CLARISSE_WRITE);
    else {
      clarisse_off_t l_f, r_f;
      
      l_f = func_1(l_v, c_ctxt->intermediary_target->distr); 
      r_f = func_1(r_v, c_ctxt->intermediary_target->distr);
      bytes_xfered = 0;

      if ((client_global_state.clarisse_pars.external_buffering == CLARISSE_EXTERNAL_BUFFERING_ENABLED) 
	  && (client_global_state.clarisse_pars.file_partitioning == CLARISSE_WORKFLOW_FILE_PARTITIONING)) {
	clarisse_off_t crt_l_f = l_f;
	int b = c_ctxt->clarisse_pars.buffer_size; 
	while (crt_l_f <= r_f) {
	  clarisse_off_t crt_r_f = MIN(r_f, END_BLOCK(crt_l_f, b));
	  create_io_tasks(c_ctxt, l_v, crt_l_f, crt_r_f, 0, 0, CLARISSE_STATIC_FILE_PARTITIONING);
	  bytes_xfered += client_comm_write(c_ctxt) ;
	  crt_l_f = BEGIN_NEXT_BLOCK(crt_l_f, b);
	}
      }
      else {
	create_io_tasks(c_ctxt, l_v, l_f, r_f, 0, 0, CLARISSE_STATIC_FILE_PARTITIONING);
	bytes_xfered = client_comm_write(c_ctxt) ;
      }
    }
   
    if (file_ptr_type == CLARISSE_INDIVIDUAL) 
      c_ctxt->fp_ind = func_1(r_v, c_ctxt->intermediary_target->distr) + 1; // r_f + 1    
    c_ctxt->fp_sys_posn = -1;   /* set it to null. */

  }
  distrib_free(c_ctxt->local_target->distr);
  clarisse_free(c_ctxt->local_target);

  /*
#ifdef CLARISSE_CONTROL
  if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED) {
    if ((client_global_state.clarisse_pars.coupleness == CLARISSE_INTERCOMM) &&
	(client_global_state.clarisse_pars.global_scheduling == CLARISSE_GLOBAL_SCHEDULING_FCFS)) {
      int myrank;
      MPI_Comm_rank(client_global_state.intracomm, &myrank);
      if (myrank == cls_controller.rank){
	clarisse_ctrl_msg_t msg;
	msg.type = CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED;
	cls_publish(&msg, sizeof(int));
      }
    }
  }
#endif
  */
  return bytes_xfered;
}

static int client_comm_write(client_iocontext_t *c_ctxt) {
  int bytes_xfered, err;

  bytes_xfered = client_drain_task_queues(c_ctxt, RQ_FILE_WRITE);
  err = MPI_Waitall(c_ctxt->mpi_rq_pool.cnt, c_ctxt->mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
  if (err != MPI_SUCCESS)
    handle_err(err, "cls_write_coll: waitall error\n");
  free_mpi_rq(&c_ctxt->mpi_rq_map);   

  return bytes_xfered;
}

int client_file_access(client_iocontext_t *c_ctxt, clarisse_off_t l_v, clarisse_off_t r_v, int count, int access_type) {
  int bytes_xfered;
  clarisse_off_t l_f, r_f;

  //open file if not already open
  if (c_ctxt->fd_sys == -1) {
    c_ctxt->fd_sys = open(c_ctxt->filename, c_ctxt->posix_access_mode, c_ctxt->perm);
    if (c_ctxt->fd_sys < 0) {
      perror("Error opening file\n");
      handle_error(MPI_ERR_OTHER, "POSIX file open error\n");
      return -1;
    }
  }

  // conting or noncontig memory
  if (distrib_contig(c_ctxt->local_target->distr)) {
    int scatter_gather;

    scatter_gather = ((access_type == CLARISSE_WRITE)?SCATTER:GATHER);
    l_f = func_1(l_v, c_ctxt->intermediary_target->distr); 
    r_f = func_1(r_v, c_ctxt->intermediary_target->distr);
    bytes_xfered = sg_file(c_ctxt->fd_sys, 0, l_f, r_f, c_ctxt->intermediary_target->distr, c_ctxt->local_target->local_handle.buf, scatter_gather);
  }
  else {
    int i, j;
    bytes_xfered = 0;
    for (i = 0; i < count; i++) {
      // if c_ctxt->local_target->distr->flat->count == 1 
      // optimize with pwritev/preadv
      for (j = 0; j < c_ctxt->local_target->distr->flat->count; j++) {
	clarisse_off_t cnt;
	l_f = func_1(l_v, c_ctxt->intermediary_target->distr); 
	r_f = func_1(l_v + c_ctxt->local_target->distr->flat->blocklens[j] - 1, c_ctxt->intermediary_target->distr);
	cnt = sg_file(c_ctxt->fd_sys, 0, l_f, r_f, c_ctxt->intermediary_target->distr, c_ctxt->local_target->local_handle.buf + i * c_ctxt->local_target->distr->flat->extent + c_ctxt->local_target->distr->flat->indices[j], SCATTER);
	l_v += cnt;
	bytes_xfered += cnt;
      }
    }
  }
    
  return bytes_xfered;
}
