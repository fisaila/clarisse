#ifdef CLARISSE_CONTROL
#include <sys/time.h>
#include <errno.h>
#include <assert.h>
#include "mpi.h"
#include "client_iocontext.h"
#include "cls_ctrl.h" 
#include "error.h"
#include "util.h"

#ifdef CLARISSE_BEACON
#include "beacon.h"
BEACON_beep_handle_t beacon_handle;
void load_handler(BEACON_receive_topic_t * caught_topic)
{
  fprintf(stderr,
	  "Received topic details: Name=%s, Beep name=%s, Hostname=%s, Scope = %s, Seqnum=%d Message: %s\n",
	  caught_topic->topic_name, caught_topic->beep_name, caught_topic->incoming_src.hostname, caught_topic->topic_scope, caught_topic->seqnum, caught_topic->topic_payload);     
}
#endif 

#define PUBLISH_INTERVAL_SEC 1

extern client_global_state_t  client_global_state;
cls_controller_t cls_controller;
cls_global_controller_t cls_global_controller;

int subscribed_load = 0;
dllist event_queue = DLLIST_INITIALIZER;

pthread_t controller_tid, pub_tid;
pthread_mutex_t     pub_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t      pub_cond_wait_event = PTHREAD_COND_INITIALIZER;



void* publisher_thread(/*void *ptr*/){
  clarisse_ctrl_msg_t msg;
  int mem = 0;
  struct timespec timeout;
  int done = 0;

  clock_gettime(CLOCK_REALTIME, &timeout);
  timeout.tv_sec += PUBLISH_INTERVAL_SEC;
#ifdef DEBUG
  printf("Publisher started\n");
#endif  
  msg.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
  pthread_mutex_lock (&pub_mutex);
  while (!done) {
    int err;
#ifdef DEBUG
    printf("Blocking waiting for an event\n");
#endif
    err = pthread_cond_timedwait (&pub_cond_wait_event,&pub_mutex, &timeout);
    if ((err != 0) && (err != ETIMEDOUT)){
	perror("pthread_cond_timedwait");
	exit(1);
    }   
    // Treat first non-timer events
    while (!dllist_is_empty(&event_queue)) {
      dllist_link *elm = dllist_rem_head(&event_queue);
      clarisse_ctrl_event_t *event = DLLIST_ELEMENT(elm, clarisse_ctrl_event_t, link);
      pthread_mutex_unlock (&pub_mutex);
      if (event->msg.type == CLARISSE_CTRL_EVENT_DONE)
	done = 1;
      else {
	err = MPI_Send(&event->msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, cls_controller.rank, CLARISSE_CTRL_TAG, cls_controller.comm);
	if (err != MPI_SUCCESS)
	  handle_err(err, "MPI_Send error");
      }	
      clarisse_free(event);
      pthread_mutex_lock (&pub_mutex);
    }
    // TImer events only if I am not done
    if ((!done) && (err == ETIMEDOUT)) {
      pthread_mutex_unlock (&pub_mutex);
#ifdef DEBUG
      printf("TIMEOUT, check again the condition\n");
#endif 
      timeout.tv_sec += PUBLISH_INTERVAL_SEC;
      msg.payload.clarisse_ctrl_msg_memory.memory = mem;
      //err = MPI_Send(&msg, sizeof(int) + sizeof(msg.payload.clarisse_ctrl_msg_memory), MPI_BYTE, mp->dest, CLARISSE_CTRL_TAG, mp->comm);
      err = MPI_Send(&msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, cls_controller.rank, CLARISSE_CTRL_TAG, cls_controller.comm);
      if (err != MPI_SUCCESS)
	handle_err(err, "MPI_Send error");
      mem++;
      pthread_mutex_lock (&pub_mutex);
    }
    
  }
  pthread_mutex_unlock (&pub_mutex);
  
  printf("PUBLISHER DONE\n");
  return NULL;
}


double t0;
static int local_controller_action(int source, clarisse_ctrl_msg_t *msg, int cnt){
  int done = 0;
  
  switch (msg->type) {
  case CLARISSE_CTRL_MSG_DONE: 
    {
      int pending_comm = 1;
      printf("%9.6f: Controller recved DONE (msg type = %d) from %d\n", MPI_Wtime() - t0, msg->type, source);
      done = 1;
      //sleep(2);
      while (pending_comm){
	MPI_Status status;
#ifdef DEBUG
	printf("Pending comm\n");
#endif
	MPI_Iprobe(MPI_ANY_SOURCE, CLARISSE_CTRL_TAG , cls_controller.comm, &pending_comm, &status);
	if (pending_comm) {
	  MPI_Recv(&msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, MPI_ANY_SOURCE, CLARISSE_CTRL_TAG , cls_controller.comm, &status);
	  printf("%9.6f: Controller recved msg type = %d from %d \n", MPI_Wtime() - t0, msg->type, status.MPI_SOURCE);
	}
      }
      
      break;
    }
    // Not used yet in CLARISSE
  case CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY: 
    {
      
      //tpool_add_work(global_thread_pool, publisher_thread, (void *)mp, (void*)0);
      break;
    }
  case CLARISSE_CTRL_MSG_PUBLISH_MEMORY: 
    {
      printf("%9.6f: Subscriber received msg type = %d of %d bytes from %d avail memory=%d\n", MPI_Wtime() - t0, msg->type, cnt, source, msg->payload.clarisse_ctrl_msg_memory.memory);
      break;
    }
  case CLARISSE_CTRL_MSG_PUBLISH_FILE_WRITE_TIME: 
    {
      printf("%9.6f: Subscriber received msg type = %d from %d file write time=%10.6f\n", MPI_Wtime() - t0, msg->type, source, msg->payload.clarisse_ctrl_msg_file_write_time.time);
      break;
    }  
  case CLARISSE_CTRL_MSG_IO_REQUEST_START: 
    {
      int err;

      printf("%9.6f: GLOBAL controller received msg type = %d from %d\n", MPI_Wtime() - t0, msg->type, source);
      if (cls_global_controller.scheduled_client_id < 0) {
	clarisse_ctrl_msg_t ret;
	cls_global_controller.scheduled_client_id = msg->payload.clarisse_ctrl_msg_io_request.client_id;
	ret.type = CLARISSE_CTRL_MSG_IO_SCHEDULE;
	err = MPI_Send(&ret, sizeof(int), MPI_BYTE, source, CLARISSE_CTRL_TAG, MPI_COMM_WORLD);
	if (err != MPI_SUCCESS)
	  handle_err(err, "MPI_Send error");	
      }
      else {
	cls_ctrl_request_t *ctrl_rq;

	ctrl_rq = (cls_ctrl_request_t *)clarisse_malloc(sizeof(cls_ctrl_request_t));
	ctrl_rq->client_id = msg->payload.clarisse_ctrl_msg_io_request.client_id;
	ctrl_rq->rank = source;
	dllist_iat(&cls_global_controller.request_queue, &ctrl_rq->link);
      }
      break;
    }
    case CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED: 
    {
      printf("%9.6f: GLOBAL controller received msg type = %d from %d\n", MPI_Wtime() - t0, msg->type, source);
      if (dllist_is_empty(&cls_global_controller.request_queue))
	cls_global_controller.scheduled_client_id = -1;
      else {
	int err;
	clarisse_ctrl_msg_t ret;
	dllist_link *elm = dllist_rem_head(&cls_global_controller.request_queue);
	cls_ctrl_request_t *ctrl_rq = DLLIST_ELEMENT(elm, cls_ctrl_request_t, link);
	cls_global_controller.scheduled_client_id = ctrl_rq->client_id;
	ret.type = CLARISSE_CTRL_MSG_IO_SCHEDULE;
	err = MPI_Send(&ret, sizeof(int), MPI_BYTE, ctrl_rq->rank, CLARISSE_CTRL_TAG, MPI_COMM_WORLD);
	if (err != MPI_SUCCESS)
	  handle_err(err, "MPI_Send error");
      }
      break;
    }

  }
  return done;
}



void * controller_recv_thread(/*void *ptr*/)
{
  int done = 0;
  //int *pval = (int*)ptr;
  //MPI_Comm comm = client_global_state.intracomm;
  //MPI_Comm global_comm = MPI_COMM_WORLD;
  t0 = MPI_Wtime();

  while (!done) {
    clarisse_ctrl_msg_t msg;
    MPI_Status status;
    int cnt;
#ifdef DEBUG
    printf("Controller recving ...\n");
#endif
    /*if (cls_controller.comm == MPI_COMM_WORLD) {
      MPI_Recv(&msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, MPI_ANY_SOURCE, CLARISSE_CTRL_TAG, cls_controller.comm, &status);
      
    }
    else */{
      int received = 0, received_global = 0;
      while (!received && !received_global) {
	MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, cls_controller.comm, &received,&status);
	if (!received)
	  MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &received_global,&status);
      }
      if (received) {
	MPI_Recv(&msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, MPI_ANY_SOURCE, CLARISSE_CTRL_TAG, cls_controller.comm, &status);
	received = 0;
      }
      else {
	MPI_Recv(&msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, MPI_ANY_SOURCE, CLARISSE_CTRL_TAG, MPI_COMM_WORLD, &status);
	assert(received_global != 0);
	received_global = 0;
      }
    }
    MPI_Get_count(&status, MPI_BYTE, &cnt);
    done = local_controller_action(status.MPI_SOURCE, &msg, cnt);    
  }
  return NULL;
}


int cls_ctrl_init(){
  if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED) {
#ifdef CLARISSE_BEACON
    int ret;
    BEACON_beep_t binfo;
    /* Specify the beep information needed by the BEACON_Connect */
    memset(&binfo, 0, sizeof(binfo));
    strcpy(binfo.beep_version, "1.0");
    //    sprintf(beep_name, "beacon_pub_%d", getpid());
    strcpy(binfo.beep_name, "beacon_clarisse");
    
    /* Connect to BEACON using BEACON_Connect */
    ret = BEACON_Connect(&binfo, &beacon_handle);
    if (ret != BEACON_SUCCESS) {
      printf("BEACON_Connect is not successful ret=%d\n", ret);
      exit(-1);
    }
#else 
    int myrank, myrank_global, ret;
    
    cls_controller.rank = client_global_state.controller_rank;
    cls_controller.comm = client_global_state.intracomm;
    MPI_Comm_size(client_global_state.intracomm, &cls_controller.nprocs);
  
    cls_global_controller.rank = client_global_state.global_controller_rank;
    cls_global_controller.scheduled_client_id = -1;
    dllist_init(&cls_global_controller.request_queue);
        
    MPI_Comm_rank(client_global_state.intracomm, &myrank);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank_global);
    if ((myrank == cls_controller.rank) || 
	(myrank_global == cls_global_controller.rank)) {
      if ((ret = pthread_create(&controller_tid, NULL, controller_recv_thread, NULL)) != 0){	
	fprintf(stderr,"pthread_create %d",ret);
	exit(-1);
      }
    }
#endif
  }
  return 0;
}

int cls_ctrl_finalize(){
  if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED){
#ifdef CLARISSE_BEACON
    BEACON_Disconnect(beacon_handle);
#else 
    int myrank, ret;
    
    MPI_Comm_rank(client_global_state.intracomm, &myrank);
    if (myrank == cls_controller.rank){
      int err;
      clarisse_ctrl_msg_t msg;
      msg.type = CLARISSE_CTRL_MSG_DONE;
      err = MPI_Send(&msg, sizeof(int), MPI_BYTE, cls_controller.rank, CLARISSE_CTRL_TAG, cls_controller.comm);
      if (err != MPI_SUCCESS)
	handle_err(err, "MPI_Send error");
    }
//#ifdef CLARISSE_INTERVAL_PUBLISHER
    if (clarisse_am_i_server()) {
      clarisse_ctrl_msg_t msg;
      msg.type = CLARISSE_CTRL_EVENT_DONE;
      cls_ctrl_publish(&msg);
#ifdef DEBUG
      printf("Signaled publisher termination");
#endif
      ret = pthread_join(pub_tid, NULL);
      if (ret) { printf("ERROR; return code from pthread_join() is %d\n", ret); exit(-1); }
    }
    //#endif
    if (myrank == cls_controller.rank) {
      ret = pthread_join(controller_tid, NULL);
      if (ret) { printf("ERROR; return code from pthread_join() is %d\n", ret); exit(-1); } 
    }
#endif
  }
  return 0;
}

int cls_ctrl_publish(clarisse_ctrl_msg_t *msg){
  if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED){
#ifdef CLARISSE_BEACON
    BEACON_topic_properties_t eprop;
    int ret;
    strncpy(eprop.topic_payload, msg->payload, sizeof(clarisse_ctrl_msg_t));
    //sprintf(eprop.topic_payload,"PAYLOAD");
    strcpy(eprop.topic_scope, "global"); 
    ret = BEACON_Publish(beacon_handle, "LOAD", &eprop);
    if (ret != BEACON_SUCCESS) {
      printf("BEACON_Publish failed with ret =%d\n", ret);
      exit(-1);
    }
#else 
    //int err;
    clarisse_ctrl_event_t *event;
#ifdef DEBUG
    printf("Publish\n");  
#endif
    event = (clarisse_ctrl_event_t *) clarisse_malloc(sizeof(clarisse_ctrl_event_t));
    memcpy(&(event->msg), msg, sizeof(clarisse_ctrl_msg_t));
    pthread_mutex_lock (&pub_mutex);
    dllist_iat(&event_queue, &event->link);
    pthread_cond_signal(&pub_cond_wait_event);
    pthread_mutex_unlock (&pub_mutex);
    /*
    err = MPI_Send(msg, sizeof(clarisse_ctrl_msg_t), MPI_BYTE, cls_controller.rank, CLARISSE_CTRL_TAG, cls_controller.comm);
    if (err != MPI_SUCCESS)
      handle_err(err, "MPI_Send error");
    */
#endif
  }
  return 0;
}

/*	pthread_mutex_lock (&pub_mutex);
	event_flag = 1;
	pthread_cond_signal( &pub_cond_wait_event);
	pthread_mutex_unlock (&pub_mutex);
*/

int cls_ctrl_subscribe(){
  if (client_global_state.clarisse_pars.control == CLARISSE_CONTROL_ENABLED){
    // called on coordinator
#ifdef CLARISSE_BEACON
    int myrank;
    
    MPI_Comm_rank(client_global_state.intracomm, &myrank);
    if ((myrank == 0) && (!subscribed_load)) {
      
      BEACON_subscribe_handle_t shandle;
      int ret;
      char filter_string[] = "topic_scope=global,topic_name=LOAD";
      
      subscribed_load = 1;
      ret = BEACON_Subscribe(&shandle, beacon_handle, filter_string);
      if (ret != BEACON_SUCCESS) {
	printf("BEACON_Subscribe failed ret=%d!\n", ret);
	exit(-1);
      }
      BEACON_Signal(&shandle, beacon_handle, load_handler);
    }
#else
    // called on stager only
    //#ifdef CLARISSE_INTERVAL_PUBLISHER
    if (clarisse_am_i_server()) {
      int ret;
      if (subscribed_load == 0) {
	if ((ret = pthread_create(&pub_tid, NULL, publisher_thread, NULL)) != 0) {	
	  fprintf(stderr,"pthread_create %d",ret); 
	  exit(-1); 
	}
	subscribed_load = 1;

      }
    }
    //#endif
#endif
  }
  return 0;
}


// called by a local coordinator to request a I/O operation permission from the 
// global coordinator
void cls_ctrl_request_start(int client_id) {
  int err;
  clarisse_ctrl_msg_t msg;
  MPI_Status status;

  msg.type = CLARISSE_CTRL_MSG_IO_REQUEST_START;
  msg.payload.clarisse_ctrl_msg_io_request.client_id = client_id;
  err = MPI_Send(&msg,  sizeof(clarisse_ctrl_msg_t), MPI_BYTE, cls_global_controller.rank, CLARISSE_CTRL_TAG, MPI_COMM_WORLD);
  if (err != MPI_SUCCESS)
    handle_err(err, "MPI_Send error");
  err = MPI_Recv(&msg, sizeof(int), MPI_BYTE, cls_global_controller.rank, CLARISSE_CTRL_TAG, MPI_COMM_WORLD, &status);
  if (err != MPI_SUCCESS)
    handle_err(err, "MPI_Recv error");
}

void cls_ctrl_request_terminated(int client_id) {
  CLARISSE_UNUSED(client_id);
  int err;
  clarisse_ctrl_msg_t msg;
  msg.type = CLARISSE_CTRL_MSG_IO_REQUEST_TERMINATED;
  err = MPI_Send(&msg,  sizeof(int), MPI_BYTE, cls_global_controller.rank, CLARISSE_CTRL_TAG, MPI_COMM_WORLD);
  if (err != MPI_SUCCESS)
    handle_err(err, "MPI_Send error");
}

#endif
