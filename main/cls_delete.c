#include "mpi.h"
#include "client_iocontext.h"
#include "util.h"
#include "error.h"
#include "hdr.h"

extern int i_am_client;
extern client_global_state_t client_global_state;

int MPI_File_delete(const char *filename, MPI_Info info) {
  return MPI_File_delete_clarisse(filename, info);
}

int MPI_File_delete_clarisse(const char *filename, MPI_Info info) {

  int ret = MPI_SUCCESS;
  //printf("MPI_File_delete\n");
  if (i_am_client) {


    if (client_global_state.clarisse_pars.collective ==  CLARISSE_ROMIO_COLLECTIVE) 
      ret = PMPI_File_delete(filename, info);
    else {

      if (client_global_state.clarisse_pars.coupleness == CLARISSE_COUPLED) {
	if (unlink(filename) < 0) {
	  //perror("MPI_File_delete:");
	  ret = MPI_ERR_IO;
	}
      }
      else {
	int log_ios_rank, i;
	MPI_Request *requests;
	int errn;
	MPI_Status status;
	/*int rsize, myrank;
	  MPI_Comm_remote_size(client_global_state.initial_cli_serv_intercomm, &rsize);
	  MPI_Comm_rank(client_global_state.intracomm, &myrank);
	  if (myrank == 0)
	  printf("Server communicator size =%d\n", rsize);
	*/
	requests = (MPI_Request *) clarisse_malloc(client_global_state.active_nr_servers * sizeof(MPI_Request));
	for (log_ios_rank = 0, i = 0; log_ios_rank < client_global_state.clarisse_pars.nr_servers; log_ios_rank++) {
	  if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	  
	    ret = MPI_Isend(filename, strlen(filename) + 1, MPI_BYTE, client_global_state.log2ph[log_ios_rank], RQ_FILE_DELETE, client_global_state.initial_cli_serv_intercomm, requests + i);
	    if (ret != MPI_SUCCESS) {
	      handle_error(ret, "Error when sending the reqs");
	      return ret;
	    }
	    i++;
	  }
	}
	errn = 0;
	for (log_ios_rank = 0; log_ios_rank < client_global_state.clarisse_pars.nr_servers; log_ios_rank++) {
	  if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE) {
	    struct rt_file_delete fd_ret;
	    ret = MPI_Recv(&fd_ret, sizeof(struct rt_file_delete), MPI_BYTE,  client_global_state.log2ph[log_ios_rank], RT_FILE_DELETE, client_global_state.initial_cli_serv_intercomm, &status);
	  if (ret != MPI_SUCCESS) {
	    handle_error(ret, "MPI_File_delete: Error receiving the rets");
	    return ret;
	  }
	  errn = MIN(errn, fd_ret.errn);
	  }
	}
	MPI_Waitall(client_global_state.active_nr_servers, requests, MPI_STATUSES_IGNORE);
	if (ret != MPI_SUCCESS)
	  handle_err(ret, "MPI_File_delete: waitall error\n");
	clarisse_free(requests);
	if (errn == 0)
	  ret = MPI_SUCCESS;
	else
	  ret = MPI_ERR_FILE;
	
	if (unlink(filename) < 0) {
	  perror("MPI_File_delete:");
	  ret = MPI_ERR_BAD_FILE;
	}
      }
    }
    //}
    //MPI_Barrier(client_global_state.intracomm);
  }
  CLARISSE_UNUSED(info);
  return ret;
}
