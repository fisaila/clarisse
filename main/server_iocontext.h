#ifndef SERVER_IOCONTEXT_H
#define SERVER_IOCONTEXT_H

#include "client_iocontext.h"
#include "list.h"
#include "map.h"
#include "request_queues.h"
#include "clarisse.h"
#include "dcache.h"
#include "buffer_pool.h"
#include "target.h"
#include "thr_pool.h"
#include "client_iocontext.h"
#include <stdio.h>
#ifdef CLARISSE_EXTERNAL_BUFFERING
#include "cls_buffering.h"
#endif

typedef struct {
  //MPI_Comm intercomm; // inter-communicator
  int partitioning_stride_factor;
  int client_nprocs;
  distrib_t **remote_view_distr; // Remote views  
  short int *crt_write_ops; // For intercomm and dynamic record where i received from
  int crt_arrived_writes; // For current collective 
  short int *next_write_ops; // For next collective I record where I received from 
  int next_arrived_writes;    // count of writes received so far
  struct net_dt net_dt;
  mpi_rq_map_t *mpi_rq_map;
  alloc_pool_t mpi_rq_pool; 
  clarisse_off_t crt_page;
} client_rt_info_t;

typedef struct {
  UT_hash_handle hh;
  clarisse_off_t block_off;
  dllist task_list;
} future_access_t;

#ifdef CLARISSE_EXTERNAL_BUFFERING

typedef struct {
  int active_flag;
  int cnt_arrived_producers;
  int total_producers; 
  //int max_producers;
  int cnt_arrived_consumers;
  int total_consumers;
  dllist off_len_lists;
  //int client_idx;
  //int source;
} coll_op_t;

/*typedef struct {
  struct dllist_link link;
  dllist off_len_lists;
} ind_op_t;
*/

typedef struct {
  UT_hash_handle hh;
  //  cls_buf_handle_t bh;
  clarisse_off_t block_off;
  coll_op_t coll_op;
  short ind_write_flag; // indicates if at least one independent write arrived
  dllist ind_write_interval_list;
  dllist ind_read_off_len_list;
} shared_buffer_t;
#endif

// server iocontext
typedef struct {
  UT_hash_handle hh;
  int global_descr;
  dcache_t dcache; // used buffer pool 
  clarisse_params_t clarisse_pars; // params 
  clarisse_target_t *local_target; //e.g., file
  //MPI_Comm intracomm;
  
  client_rt_info_t *client_rt_info;
  future_access_t *read_futures;
  future_access_t *write_futures;
#ifdef CLARISSE_EXTERNAL_BUFFERING
  shared_buffer_t *shared_buffer_pool;
#endif 
} server_iocontext_t;

// global stager state
typedef struct server_global_state_t {
  int initialized;
  clarisse_target_t *active_targets;
  server_iocontext_t *s_ctxt_map;
  dllist write_task_queue; // each task is associated to a context
  request_queues_t read_task_queue; // 
  buffer_pool_t * buf_pool; // Free buffer pool for the stager 
#ifdef CLARISSE_EXTERNAL_BUFFERING
  cls_buffering_t bufservice;
#endif 


  tpool_t tpool;
  clarisse_params_t clarisse_pars; 
  char *net_buf;

  int nr_clients;
  int nr_active_clients;
  int total_client_procs;
  MPI_Comm intracomm;
  MPI_Comm *intercomms; // no_clients
  int *client_nprocs; // number of clients
  int finalized_cnt;
  int controller_rank;
  int global_controller_rank;

  char port[MPI_MAX_PORT_NAME]; // port for independent applications
} server_global_state_t;

server_iocontext_t * server_iocontext_init(int global_descr);
server_iocontext_t * server_iocontext_alloc_init(clarisse_target_t *local_target, int partitioning_stride_factor, int nprocs);
void server_iocontext_update(server_iocontext_t *s_ctxt, clarisse_target_t *local_target, int client_idx, int partitioning_stride_factor, int client_nprocs);
void server_iocontext_update_client_rt_info(server_iocontext_t *s_ctxt, int client_idx, int partitioning_stride_factor, int client_nprocs);
void server_iocontext_update_target(server_iocontext_t *s_ctxt, clarisse_target_t *local_target);
server_iocontext_t * server_iocontext_find(int global_descr);
server_iocontext_t * server_iocontext_find_del(int global_descr);
void server_iocontext_free(server_iocontext_t *s_ctxt);


int server_global_state_init(clarisse_params_t *clarisse_pars, MPI_Comm intracomm, int nr_clients, MPI_Comm *intercomms);
void server_global_state_free();

#ifdef CLARISSE_EXTERNAL_BUFFERING
shared_buffer_t *shared_buffer_alloc_init(clarisse_off_t block_off);
void shared_buffer_print(shared_buffer_t *sbp);
void shared_buffer_free(shared_buffer_t *sb);

void shared_buffer_pool_free(shared_buffer_t *sbp);
void shared_buffer_pool_print(shared_buffer_t *sbp);
#endif

#endif




