#ifndef ADIO_MAP_H
#define ADIO_MAP_H

#include "client_iocontext.h"

typedef struct {
  UT_hash_handle hh;
  void *key;
  client_iocontext_t *c_ctxt;
} cls_adio_c_ctxt_map_t;


/*
long long int ADIO_get_fsize(clarisse_target_t *tgt);
long long int bcast_ADIO_get_fsize(ADIO_File fd);
*/
void cls_adio_set_generic_functions(MPI_File fh);
void cls_adio_open(client_iocontext_t *c_ctxt);
void cls_adio_close(client_iocontext_t *c_ctxt);

#endif
