#include "client_iocontext.h"
#include "server_iocontext.h"
#include "buffer_pool.h"
#include "hdr.h"
#include "buffer.h"
#include "util.h"
#include "error.h"

#ifdef CLARISSE_TIMING
extern clarisse_timing_t local_timing;
#endif
extern server_global_state_t server_global_state;
extern client_global_state_t client_global_state;
extern int i_am_client;
extern int i_am_server;

int cli_close(client_iocontext_t *c_ctxt);
void serv_close(client_iocontext_t *c_ctxt);
int cli_flush(client_iocontext_t *c_ctxt);

int MPI_File_close(MPI_File *fh) {
  return MPI_File_close_clarisse(fh);
}


int MPI_File_close_clarisse(MPI_File *fh) {
  int ret = MPI_SUCCESS;
#ifdef CLARISSE_TIMING
  local_timing.time_close[local_timing.cnt_close][0] = MPI_Wtime();
#endif   
  
  if (i_am_client) {
#ifdef CLARISSE_DEBUG
    printf("MPI_File_close\n");
#endif

    if (client_global_state.clarisse_pars.collective ==  CLARISSE_ROMIO_COLLECTIVE) 
      ret = PMPI_File_close(fh);
    else {
      client_iocontext_t *c_ctxt;
      c_ctxt = client_iocontext_find(client_global_state.c_ctxt_map, (void *)*fh);
    
      if (c_ctxt) {
	if (clarisse_am_i_coupled_server(c_ctxt)){
	  serv_close(c_ctxt);
	}  
	ret = cli_close(c_ctxt);  
      }
      else
	ret = MPI_ERR_BAD_FILE;
    }
  }
#ifdef CLARISSE_TIMING
  if (local_timing.cnt_close < CLARISSE_MAX_CNT_TIMED_OPS) {
    local_timing.time_close[local_timing.cnt_close][1] = MPI_Wtime();
    local_timing.cnt_close++;
  }
#endif  
  
  return ret;
}


int  cli_close(client_iocontext_t *c_ctxt) {
  int ret;
  //MPI_Barrier(client_global_state.intracomm);
  //cls_adio_close(c_ctxt);
  ret = cli_flush(c_ctxt);
  
  HASH_DEL(client_global_state.c_ctxt_map, c_ctxt);  
  // Wait for flushing to end
  if (!(c_ctxt->mpi_access_mode & MPI_MODE_RDONLY))
    MPI_Barrier(c_ctxt->intracomm);
  client_iocontext_free(c_ctxt); 
  return ret;
}

void serv_close(client_iocontext_t *c_ctxt) {
  server_iocontext_t *s_ctxt;
  clarisse_target_t *tgt;

  s_ctxt = server_iocontext_find_del(c_ctxt->global_descr);
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif

  if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK){
    tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 1);
    tpool_wait_for_task_queue_empty(server_global_state.tpool, (void*)s_ctxt);
    tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 0);
    //    tpool_task_queue_destroy(server_global_state.tpool, (void*)s_ctxt);
    dcache_atomic_bufpool_move_all(&s_ctxt->dcache, server_global_state.buf_pool);
#ifdef CLARISSE_DCACHE_DEBUG
    dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
  }
  else
    flush_buffers(s_ctxt);
  // Find and free target
  HASH_FIND_INT(server_global_state.active_targets, &c_ctxt->global_descr, tgt);
  HASH_DEL(server_global_state.active_targets, tgt);
  distrib_free(tgt->distr);
  clarisse_free(tgt);
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
  // Free context
  server_iocontext_free(s_ctxt);
  //printf("******************************************CLOSE finish\n");
}
