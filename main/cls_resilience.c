#include "client_iocontext.h"
#include "util.h"
#include "hdr.h"
#include "error.h"
#include "cls_ctrl.h"
#include <assert.h>


extern client_global_state_t client_global_state;
#ifdef CLARISSE_CONTROL
extern cls_controller_t cls_controller;
#endif

/*                   01234
active_log_servers   014    (2, 3, 0 are active)
log2ph               23410
ph2log               2->0(active), 3->1 (active), 4->2 (dead), 1->3 (dead), 0->4 (active)

Physical 0 fails:   m->log_ios = 4
                     01234
active_log_servers   01     (2, 3 are active)
log2ph               23410
ph2log               2->0(active), 3->1 (active), 4->2 (dead), 1->3 (dead), 0->4 (dead)
*/

// Returns 0 if successfull
//        -1 if server is already down or not a server  
int cls_ph_server_down(int ph_ios_rank){
  int_map_t *m;
  int ret = 0;
  
  HASH_FIND_INT(client_global_state.ph2log, &ph_ios_rank, m);
  if (m) {
    if (client_global_state.log2ph[m->log_ios_rank] == CLS_SERVER_INACTIVE)
      ret = -1;
    else {
      int log_ios_rank, active_log_ios_rank;
      client_global_state.log2ph[m->log_ios_rank] = CLS_SERVER_INACTIVE;
      for (active_log_ios_rank = MIN(m->log_ios_rank, client_global_state.active_nr_servers - 1); active_log_ios_rank >= 0;  active_log_ios_rank--){
	if (client_global_state.active_log_servers[active_log_ios_rank] == m->log_ios_rank)
	  break;
      }
      assert(active_log_ios_rank >= 0);
      //shift left all active servers	
      for (log_ios_rank = active_log_ios_rank + 1; log_ios_rank < client_global_state.active_nr_servers; log_ios_rank++)
	client_global_state.active_log_servers[log_ios_rank - 1] = client_global_state.active_log_servers[log_ios_rank];
      client_global_state.active_nr_servers--;
    }
  }
  else {
    printf("WARNING: Rank %d was not a physical CLARISSE server\n", ph_ios_rank); 
    ret = -1;
  }
  return ret;
  //cls_print_servers();
}

int cls_log_server_down(int log_ios_rank){
  if (client_global_state.log2ph[log_ios_rank] == CLS_SERVER_INACTIVE)
    return -1;
  else
    return cls_ph_server_down(client_global_state.log2ph[log_ios_rank]);
}

int cls_active_server_down(int active_log_ios_rank){
  return cls_log_server_down(client_global_state.active_log_servers[active_log_ios_rank]);
}

int cls_active_server_down_publish(int active_log_ios_rank){
#ifdef CLARISSE_CONTROL
  clarisse_ctrl_msg_t *msg;
  clarisse_ctrl_msg_io_server_down_t msg_io_server_down;
  communication_point_t dest;
  int msg_size = sizeof(clarisse_ctrl_msg_io_server_down_t);
  
  msg = (clarisse_ctrl_msg_t *)&msg_io_server_down;
  msg_io_server_down.type = CLARISSE_CTRL_MSG_IO_SERVER_DOWN;
  msg_io_server_down.active_log_ios_rank = active_log_ios_rank;
  dest.rank = cls_controller.rank;
  dest.comm = cls_controller.comm;

  return cls_publish_remote(&dest, msg, msg_size);  
#else
  CLARISSE_UNUSED(active_log_ios_rank);
  return -1;
#endif
}


void cls_set_load_injection_coll_seq(int coll_seq, int active_log_ios_rank, double load) {
  client_global_state.load_injection_coll_seq = coll_seq;
  client_global_state.loaded_active_log_ios_rank = active_log_ios_rank;
  client_global_state.load = load;
}
void cls_set_load_detection_coll_seq(int coll_seq) {
  client_global_state.load_detection_coll_seq = coll_seq;
}



void cls_active_server_slow(int active_log_ios_rank, double slow_sec){
  int ret, log_ios_rank, retval;
  MPI_Status status;

  log_ios_rank = client_global_state.active_log_servers[active_log_ios_rank] ;
  
  ret = MPI_Send(&slow_sec, 1, MPI_DOUBLE, client_global_state.log2ph[log_ios_rank], RQ_SERVER_DELAY, client_global_state.initial_cli_serv_intercomm);
  if (ret != MPI_SUCCESS) {
    handle_error(ret, "Error when sending the reqs");
  }
  ret = MPI_Recv(&retval, 1, MPI_INT,  client_global_state.log2ph[log_ios_rank], RT_SERVER_DELAY, client_global_state.initial_cli_serv_intercomm, &status);
  if (ret != MPI_SUCCESS) {
    handle_error(ret, "cls_active_server_slow: Error receiving the rets");
  }
}


int cls_get_active_log_ios_rank(int log_ios_rank) {
  int active_log_ios_rank;
  for (active_log_ios_rank = MIN(log_ios_rank, client_global_state.active_nr_servers - 1); active_log_ios_rank >= 0;  active_log_ios_rank--){
    if (client_global_state.active_log_servers[active_log_ios_rank] == log_ios_rank)
      return active_log_ios_rank;
  }
  return -1;
}


int cls_log_server_up(int log_ios_rank){
  if (client_global_state.log2ph[log_ios_rank] != CLS_SERVER_INACTIVE)
    return -1;
  else
    return cls_ph_server_up(client_global_state.log2ph[log_ios_rank]);
}

// Returns 0 if successfull
//        -1 if server is already down or not a server  

int cls_ph_server_up(int ph_ios_rank){
  int_map_t *m;
  int ret = 0;

  HASH_FIND_INT(client_global_state.ph2log, &ph_ios_rank, m);
  if (m) {
    if (client_global_state.log2ph[m->log_ios_rank] == ph_ios_rank)
      ret = -1;
    else {
      int log_ios_rank, active_log_ios_rank;
      client_global_state.log2ph[m->log_ios_rank] = ph_ios_rank;
      for (active_log_ios_rank = MIN(m->log_ios_rank, client_global_state.active_nr_servers - 1); active_log_ios_rank >= 0;  active_log_ios_rank--){
	if (client_global_state.active_log_servers[active_log_ios_rank] < m->log_ios_rank)	
	  break;
      }
      //shift right all active servers
      for (log_ios_rank = active_log_ios_rank + 1; log_ios_rank < client_global_state.active_nr_servers; log_ios_rank++)
	client_global_state.active_log_servers[log_ios_rank + 1] = client_global_state.active_log_servers[log_ios_rank];
      client_global_state.active_log_servers[active_log_ios_rank + 1] = m->log_ios_rank;
      client_global_state.active_nr_servers++;
    }
  }
  else {
    ret = -1;
    printf("WARNING: Rank %d was not a physical CLARISSE server\n", ph_ios_rank); 
  }
  //cls_print_servers();
  return ret;
}



// adds/removes a new server (down/up just mark existing servers as down/up)
int cls_ph_server_add(int ph_ios_rank){
  CLARISSE_UNUSED(ph_ios_rank);
  return -1;
}

int cls_ph_server_remove(int ph_ios_rank){
  CLARISSE_UNUSED(ph_ios_rank);
  return -1;
}
