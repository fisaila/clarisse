#ifdef CLARISSE_CONTROL
#ifndef CLS_PUB_SUB_H
#define CLS_PUB_SUB_H

#include "mpi.h"
#include "list.h" 
#include "uthash.h"
#include "clarisse_internal.h"

#define CLARISSE_MAX_COUNT_PUB_SUB_COMMUNICATORS 16

#define CLARISSE_EVENT_PRIORITY_LOW 0
#define CLARISSE_EVENT_PRIORITY_HIGH 1

#define  CLARISSE_EVENT_SCOPE_PROCESS 0
#define  CLARISSE_EVENT_SCOPE_NODE 1
#define  CLARISSE_EVENT_SCOPE_GROUP 2
#define  CLARISSE_EVENT_SCOPE_GLOBAL 3

#define  CLARISSE_EVENT_REPLY_TO_SENDER 0
#define  CLARISSE_EVENT_USE_DESTINATION 1  
#define  CLARISSE_EVENT_DYNAMIC_DESTINATION 2  

#define clarisse_subscribe_handle_t int

typedef struct {
  MPI_Comm comm;
  int rank;
} communication_point_t;
/*
typedef struct {
  int priority;
  int scope;
  int dest_type; // CLARISSE_CTRL_EVENT_REPLY_TO_SENDER 
                 // CLARISSE_CTRL_EVENT_USE_DESTINATION  
  communication_point_t dest;
} clarisse_ctrl_event_props_t;
*/

#define CLARISSE_CTRL_MAX_MSG_SIZE 256

typedef struct {
  int type;
  char padding[CLARISSE_CTRL_MAX_MSG_SIZE - sizeof(int)];
} clarisse_ctrl_msg_t;

typedef struct {
  struct dllist_link link;
  clarisse_ctrl_msg_t msg;
  communication_point_t source;
  int msg_size;
} clarisse_ctrl_event_t;

typedef struct {
  UT_hash_handle hh;
  int type; // Event type: used for associating with event
  int priority;
  int scope;
  int dest_type; // CLARISSE_CTRL_EVENT_REPLY_TO_SENDER 
                 // CLARISSE_CTRL_EVENT_USE_DESTINATION  
  communication_point_t dest;
  int (*handler)(void *eprops ,clarisse_ctrl_event_t *event);
  int handler_refcount; // if >0 the handler can not be changed
} clarisse_ctrl_event_props_t;

int cls_pub_sub_init(int nr_comms, MPI_Comm comms[]);
int cls_pub_sub_finalize();
int cls_publish(clarisse_ctrl_msg_t *msg, int msg_size);
int cls_publish_remote(communication_point_t *dest, clarisse_ctrl_msg_t *msg, int msg_size);
int cls_subscribe(clarisse_ctrl_event_props_t *eprops);
clarisse_ctrl_event_t *cls_wait_event(int event_type);
clarisse_ctrl_event_t *cls_test_event(int event_type);

#endif  /* CLS_PUB_SUB_H */
#endif /* CLARISSE_CONTROL */
