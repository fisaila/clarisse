#include "hdr.h"
#include "mpi.h"
#include "assert.h"
#include "clarisse.h"
#include "uthash.h"
#include "util.h" 
#include "error.h"
#include "marshal.h"
#include "buffer.h"
#include "dcache.h"
#include "tasks.h"
#include <float.h>
#include <limits.h>

double delay = 0;

extern server_global_state_t server_global_state;

pthread_mutex_t     mutex_intercomms = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t      cond_zero_active_clients = PTHREAD_COND_INITIALIZER;
pthread_cond_t      cond_max_active_clients = PTHREAD_COND_INITIALIZER;

int serv_op_finalize_io(int client_idx);
void serv_op_write_coll(struct rq_file_write * r, int client_idx, int source, int cnt);
void serv_op_write(struct rq_file_write * r, int client_idx, int source, int cnt, int coll_flag);
void serv_op_read_coll(struct rq_file_read * r, int client_idx, int source, int cnt, int coll_flag);
void serv_op_read(struct rq_file_read * r, int client_idx, int source, int cnt);
void serv_op_read_coll_future(struct rq_file_read * r, int client_idx, int source, int cnt);
#ifdef CLARISSE_EXTERNAL_BUFFERING
void serv_op_buffer_map(struct rq_file_buffer_map * r, int op, int client_idx, int source);
#endif
void serv_op_flush(struct rq_file_flush * r, int client_idx, int source);
void serv_op_setview(struct rq_file_set_view * r, int client_idx);
void serv_op_delete(char * r, int client_idx, int source);
void serv_op_get_size(struct rq_file_get_size * r, int client_idx, int source);
void serv_op_delay(double t, int client_idx, int source);

void serv_write_task(server_iocontext_t *s_ctxt, struct rq_file_write  *r, int client_idx, int source);
void serv_write_task_listio(server_iocontext_t * s_ctxt, struct rq_file_write  *r, int client_idx, int source);
void serv_write_task_listio_buffering(server_iocontext_t * s_ctxt, struct rq_file_write  *r, int client_idx, int source);
void serv_write_task_listio_buffering2(server_iocontext_t * s_ctxt, struct rq_file_write  *r, int client_idx, int source);
void serv_write_task_listio_buffering2_ind(server_iocontext_t * s_ctxt, struct rq_file_write  *r, int client_idx, int source);
void serv_read_task(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source);
void serv_read_future_task(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source);
void serv_read_task_listio(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source, int type);
void serv_read_task_listio_buffering(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source);
void serv_read_task_listio_buffering2(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source);
void serv_read_task_listio_buffering2_ind(server_iocontext_t *s_ctxt, struct rq_file_read  *r, int client_idx, int source);
void serv_create_write_tasks(server_iocontext_t * s_ctxt);
int serv_read_n_shares(int client_idx);
void flush_dcache_buffers_future(server_iocontext_t *s_ctxt); // cls_read_coll.c


void *accept_thr(void * param) {

  while (1) {
    MPI_Comm intercomm;
    MPI_Comm_accept(server_global_state.port, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercomm);
    pthread_mutex_lock (&mutex_intercomms);
    printf("Client %d arrived\n", server_global_state.nr_active_clients);
    server_global_state.intercomms[server_global_state.nr_active_clients] = intercomm;
    MPI_Comm_remote_size(intercomm, &server_global_state.client_nprocs[server_global_state.nr_active_clients]);
    server_global_state.nr_active_clients++;
    if (server_global_state.nr_active_clients == 1)
      pthread_cond_signal(&cond_zero_active_clients);  

    if (server_global_state.nr_active_clients == CLARISSE_MAX_NR_ACTIVE_CLIENTS)
      pthread_cond_wait(&cond_max_active_clients, &mutex_intercomms);  
    pthread_mutex_unlock (&mutex_intercomms);

  } 
  CLARISSE_UNUSED(param);
  return NULL;
}

void *disconnect_thr(void * param) {
  MPI_Comm *intercomm = (MPI_Comm *) param;
  printf("Server created DISCONNECT thread\n");
  MPI_Comm_disconnect(intercomm);
  clarisse_free(intercomm);
  printf("DISCONNECT thread finishing \n");
  return NULL;
}

void mysleep(double sleep_time_sec){
  double t0;
  
  t0 = MPI_Wtime();
  while ((MPI_Wtime() - t0) < sleep_time_sec);
}

void server_init(int argc, char *argv[]){
  CLARISSE_UNUSED(argc); 
  CLARISSE_UNUSED(argv);

  /*
  clarisse_params clarisse_pars;
  MPI_Comm intracomm, *intercomms;

  clarisse_pars.buffer_size = atoi(argv[1]);
  clarisse_pars.bufpool_size = atoi(argv[2]);
  clarisse_pars.net_buf_size = atoi(argv[4]);
  clarisse_pars.net_datatype_count = atoi(argv[5]);
  clarisse_pars.net_datatype_incr = atoi(argv[6]);
  clarisse_pars.metadata_transfer_size = atoi(argv[7]);
  clarisse_pars.io_sched_policy = (short int) atoi(argv[8]);
  clarisse_pars.lazy_eager_view = (short int) atoi(argv[9]);
  clarisse_pars.prefetch = (short int) atoi(argv[10]);
  clarisse_pars.write_type  = (short int) atoi(argv[11]);

  clarisse_pars.coupleness = CLARISSE_DYN_PROCESS;
  
  //MPI_Comm_get_parent(&intercomm);
  //  if (server_global_state.initial_cli_serv_intercomm == MPI_COMM_NULL)
  //  handle_error(MPI_ERR_OTHER, "Server inter-comm is MPI_COMM_NULL\n");
  intracomm = MPI_COMM_WORLD;
  //server_global_state_init(&clarisse_pars, intracomm, 1, intercomms);
  */
}

void server_finalize(){
  server_global_state_free();
}




void server_operate(){
  int again;
  int client_idx = 0;
  pthread_t accept_tid;
  int ret;

  if (server_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS){
    ret = pthread_create(&accept_tid, NULL, accept_thr, NULL);
    if (ret)
      handle_error(MPI_ERR_OTHER, "ERROR in pthread_create()\n");
  }
    

  again = 1;
  while (again) { 
    MPI_Status status;
    int cnt;
    int received, coll_flag;
    MPI_Comm intercomm;

    if (server_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS) {
      pthread_mutex_lock (&mutex_intercomms);
      while (server_global_state.nr_active_clients == 0) {
	pthread_cond_wait (&cond_zero_active_clients, &mutex_intercomms);
      }
      pthread_mutex_unlock (&mutex_intercomms);
#ifdef CLARISSE_SERVER_DEBUG       
      printf("SERVER waked up: Client arrived nr active clients =%d\n", 
	     server_global_state.nr_active_clients);
#endif
    }

    received = 0;
    while (!received) {
      int err;
      if (server_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS){ 
	pthread_mutex_lock (&mutex_intercomms);
	client_idx = (client_idx + 1) % server_global_state.nr_active_clients;	
      }
      else 
	client_idx = (client_idx + 1) % server_global_state.nr_clients;
      intercomm = server_global_state.intercomms[client_idx];
      if (server_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS) 
	pthread_mutex_unlock (&mutex_intercomms);
      err = MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, intercomm, &received,&status);
      if (err != MPI_SUCCESS)
	handle_error(err, "MPI_Iprobe error\n");      
    }
#ifdef CLARISSE_SERVER_DEBUG       
      printf("SERVER received a message\n");
#endif


    MPI_Recv(server_global_state.net_buf, server_global_state.clarisse_pars.net_buf_size, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, intercomm, &status );	
    MPI_Get_count(&status, MPI_BYTE, &cnt);

    switch (status.MPI_TAG) { 
      //double max = 0, min = DBL_MAX, avg = 0;
      //int cntops = 0;
    case RQ_FINALIZE_IO:
      again = serv_op_finalize_io(client_idx);
      break;
      //case RQ_FILE_OPEN:
      //case RQ_FILE_CLOSE:
    case RQ_FILE_SET_VIEW: 
      serv_op_setview((struct rq_file_set_view *)server_global_state.net_buf, client_idx);
      break;
    case RQ_FILE_WRITE: 
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Write rq from %d\n", status.MPI_SOURCE);
#endif
      coll_flag = 0;
      serv_op_write((struct rq_file_write *)server_global_state.net_buf, client_idx, status.MPI_SOURCE, cnt, coll_flag);
      break;
    case RQ_FILE_WRITE_COLL: 
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Write coll rq from %d crt delay=%f\n", status.MPI_SOURCE, delay);
#endif
      //if (delay > 0)
      //printf("SERVER Before op_write: time=%10.6f\n", MPI_Wtime());
      mysleep(delay);
      //double t0, t1;
      //t0 = MPI_Wtime();
      serv_op_write_coll((struct rq_file_write *)server_global_state.net_buf, client_idx, status.MPI_SOURCE, cnt);
      //if (delay > 0)
      //printf("SERVER After op write: time=%10.6f\n", MPI_Wtime());
      //t1 = MPI_Wtime();
      //max = MAX(max, t1 - t0);
      //min = MIN(min, t1 - t0);
      //avg += (t1 - t0);
      //cntops++;

      break;
    case RQ_FILE_READ:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Read rq from %d\n", status.MPI_SOURCE);
#endif 
      coll_flag = 0;
      serv_op_read_coll((struct rq_file_read *)server_global_state.net_buf, client_idx, status.MPI_SOURCE, cnt, coll_flag);
      break;
    case RQ_FILE_READ_COLL:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Read COLL rq from %d\n", status.MPI_SOURCE);
#endif 
      coll_flag = 1;
      serv_op_read_coll((struct rq_file_read *)server_global_state.net_buf, client_idx, status.MPI_SOURCE, cnt, coll_flag);
      break;

    case RQ_FILE_READ_COLL_FUTURE:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Read COLL future rq from %d\n", status.MPI_SOURCE);
#endif 
      serv_op_read_coll_future((struct rq_file_read *)server_global_state.net_buf, client_idx, status.MPI_SOURCE, cnt);
      break;

#ifdef CLARISSE_EXTERNAL_BUFFERING
    case RQ_FILE_WRITE_BUFFER_MAP:
    case RQ_FILE_READ_BUFFER_MAP:  
#ifdef CLARISSE_SERVER_DEBUG      
      printf("%s buffer map rq from %d\n", (status.MPI_TAG == RQ_FILE_WRITE_BUFFER_MAP)?"Write":"Read", status.MPI_SOURCE);
#endif 
      serv_op_buffer_map((struct rq_file_buffer_map *)server_global_state.net_buf, status.MPI_TAG, client_idx, status.MPI_SOURCE);
      break;
#endif

    case RQ_FILE_FLUSH:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Flush rq from %d\n", status.MPI_SOURCE);
#endif 
      serv_op_flush((struct rq_file_flush *)server_global_state.net_buf, client_idx, status.MPI_SOURCE);
     
      break;
      //case RQ_FILE_FTRUNCATE:
    case RQ_FILE_DELETE:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Delete rq from %d\n", status.MPI_SOURCE);
#endif
      serv_op_delete((char *)server_global_state.net_buf, client_idx, status.MPI_SOURCE);
      break;
    case RQ_FILE_GET_SIZE:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Get size rq from %d\n", status.MPI_SOURCE);
#endif
      serv_op_get_size((struct rq_file_get_size *)server_global_state.net_buf, client_idx, status.MPI_SOURCE);
      break;


    case RQ_SERVER_DELAY:
#ifdef CLARISSE_SERVER_DEBUG      
      printf("Server delay rq from %d for %f seconds\n", status.MPI_SOURCE, 
	     *((double *) server_global_state.net_buf));
#endif
      
      serv_op_delay(*((double *) server_global_state.net_buf), client_idx, status.MPI_SOURCE);
     
      break;
    default: 
      // Unexpected message type  
#ifdef CLARISSE_SERVER_DEBUG      
      fprintf(stderr,"Unexpected message \n");
#endif
      MPI_Abort(MPI_COMM_WORLD, 1); 
    } 
    
    /* if (!chash_empty(&ios_state.read_req_list)) { 
      ios_task_p i;
      i = (ios_task_p) chash_peek_crt(&ios_state.read_req_list);
      read_n_shares(i);
      if (i->l_f > i->r_f) 
	free(chash_remove_crt(&ios_state.read_req_list));
      else 
	chash_set_next(&ios_state.read_req_list);
    }
    */
  }
  if (server_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS){
    ret = pthread_join(accept_tid, NULL);
    if (ret) {
      printf("ERROR; return code from pthread_join() is %d\n", ret);
      MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
    }
  }
}
  

int serv_op_finalize_io(int client_idx){
  int again = 1;
#ifdef CLARISSE_SERVER_DEBUG       
  printf("SERVER received RQ_FINALIZE_IO\n");
#endif

  if (server_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS) {
    int ret;
    pthread_t disconnect_tid;
    pthread_attr_t attr;
    MPI_Comm *intercomm = (MPI_Comm *) clarisse_malloc(sizeof(MPI_Comm)); 
    *intercomm = server_global_state.intercomms[client_idx];
    pthread_mutex_lock (&mutex_intercomms);
    server_global_state.nr_active_clients --;
    if (server_global_state.nr_active_clients > 0) {
      server_global_state.intercomms[client_idx] = server_global_state.intercomms[server_global_state.nr_active_clients];
      server_global_state.client_nprocs[client_idx] = server_global_state.client_nprocs[server_global_state.nr_active_clients];
    }
    //printf("Server %d received DISCONNECT request from client %d rank %d nr_active_clients = %d\n", rank, client_idx, status.MPI_SOURCE, server_global_state.nr_active_clients);
    if (server_global_state.nr_active_clients == CLARISSE_MAX_NR_ACTIVE_CLIENTS - 1)
      pthread_cond_signal(&cond_max_active_clients);  
    
    pthread_mutex_unlock (&mutex_intercomms);
    ret = pthread_attr_init(&attr);
    if (ret) handle_error(MPI_ERR_OTHER, "ERROR in pthread_attr_init()\n");
    ret = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    if (ret) handle_error(MPI_ERR_OTHER, "ERROR in pthread_attr_setdetachstate()\n");
    ret = pthread_create(&disconnect_tid, NULL, disconnect_thr, intercomm);
    if (ret) handle_error(MPI_ERR_OTHER, "ERROR in pthread_create()\n");
    
  }
  else {
    
    server_global_state.finalized_cnt++;
    if (server_global_state.finalized_cnt == server_global_state.total_client_procs){
      again = 0;
      //	printf("WRITE OP TIMINGS: %d ops avg = %9.6f min = %9.6f max = %9.6f\n",cntops, avg/ cntops, min, max);
      /* Need to move it to a thread
      // This approach disconnects all clients after all finish
      if (server_global_state.clarisse_pars.coupleness == CLARISSE_DYN_PROCESS) {
      MPI_Comm_disconnect(&intercomm);
      pthread_mutex_lock (&mutex_intercomms);
      server_global_state.nr_active_clients --;
      server_global_state.intercomms[client_idx] = server_global_state.intercomms[server_global_state.nr_active_clients];
      //printf("Server %d received DISCONNECT request from client %d rank %d nr_active_clients = %d\n", rank, client_idx, status.MPI_SOURCE, server_global_state.nr_active_clients);
      pthread_mutex_unlock (&mutex_intercomms);
      }
      */      
    }
  }  
  return again;
}



void serv_op_write_coll(struct rq_file_write * r, int client_idx, int source, int cnt){

  server_iocontext_t *s_ctxt;
  int global_descr;

  if (cnt == sizeof(int)) 
    global_descr = *((int *) r);
  else
    global_descr = r->global_descr;


  if (cnt > (int) sizeof(int)) {
    int coll_flag = 1;
    serv_op_write(r, client_idx, source, cnt, coll_flag);
  }
  if (server_global_state.clarisse_pars.external_buffering == CLARISSE_EXTERNAL_BUFFERING_DISABLED) {
    s_ctxt = server_iocontext_find(global_descr);

    if (!s_ctxt)
      s_ctxt = server_iocontext_init(global_descr);
    assert(s_ctxt != NULL);

    if ((cnt == sizeof(int) || (r->last_msg == 1)) && 
	((s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK) ||
	 (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_ON_DEMAND))) { 
      if (s_ctxt->client_rt_info[client_idx].crt_write_ops[source] == 0) {
	s_ctxt->client_rt_info[client_idx].crt_write_ops[source] = 1;
	s_ctxt->client_rt_info[client_idx].crt_arrived_writes++;
#ifdef CLARISSE_SERVER_DEBUG      
	printf("%s crt_arrived_writes=%d nr_clients=%d\n", __FUNCTION__, s_ctxt->client_rt_info[client_idx].crt_arrived_writes, server_global_state.client_nprocs[client_idx]);
#endif
	if (s_ctxt->client_rt_info[client_idx].crt_arrived_writes == server_global_state.client_nprocs[client_idx]) {
	  short int * tmp;
	  
	  tmp = s_ctxt->client_rt_info[client_idx].crt_write_ops;
	  s_ctxt->client_rt_info[client_idx].crt_write_ops = s_ctxt->client_rt_info[client_idx].next_write_ops; 
	  s_ctxt->client_rt_info[client_idx].crt_arrived_writes = s_ctxt->client_rt_info[client_idx].next_arrived_writes;
	  s_ctxt->client_rt_info[client_idx].next_write_ops = tmp;
	  memset((void *)s_ctxt->client_rt_info[client_idx].next_write_ops, 0, server_global_state.client_nprocs[client_idx] * sizeof(short int));
	  s_ctxt->client_rt_info[client_idx].next_arrived_writes = 0;
	  
	  if (s_ctxt->clarisse_pars.read_future == CLARISSE_READ_FUTURE_ENABLED) {
	    flush_dcache_buffers_future(s_ctxt);
	  }
	  else {
	    if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK) 	
	      serv_create_write_tasks(s_ctxt);
	    if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_ON_DEMAND){
#ifdef CLARISSE_SERVER_DEBUG      
	      printf("Flush dcache buffers\n");
#endif
	      flush_dcache_buffers(s_ctxt);
	    }
	  }
	}
      }
      else { // if (s_ctxt->client_rt_info[client_idx].crt_write_ops[source] == 0)
	assert(s_ctxt->client_rt_info[client_idx].next_write_ops[source] == 0);
	s_ctxt->client_rt_info[client_idx].next_write_ops[source] = 1;
	s_ctxt->client_rt_info[client_idx].next_arrived_writes++;
	assert(s_ctxt->client_rt_info[client_idx].next_arrived_writes < server_global_state.client_nprocs[client_idx]);
      }
    }
  } // if (external_buffering == CLARISSE_EXTERNAL_BUFFERING_DISABLED)
#ifdef CLARISSE_DCACHE_DEBUG
    dcache_print(&s_ctxt->dcache, "serv_op_write_coll END");
#endif 
} 

void serv_op_write(struct rq_file_write * r, int client_idx, int source, int cnt, int coll_flag){

  server_iocontext_t *s_ctxt;
  int global_descr;

  global_descr = r->global_descr;
  s_ctxt = server_iocontext_find(global_descr);
  if (!s_ctxt)
    s_ctxt = server_iocontext_init(global_descr);
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, "serv_op_write BEGINNING");
#endif      
  //  if ((s_ctxt->local_target == NULL) && ((server_global_state.clarisse_pars.external_buffering == CLARISSE_EXTERNAL_BUFFERING_DISABLED))){
  if (s_ctxt->local_target == NULL){
    clarisse_target_t *tgt;
    char *filename;
    
    assert (r->first_msg > 0);
    HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
    if (!tgt) {
      filename = (char *)(r + 1) /*+ r->view_size*/;
      tgt = clarisse_target_alloc_init(filename, r->global_descr, CLARISSE_TARGET_FILE, NULL, NULL); 
      HASH_ADD_INT(server_global_state.active_targets, global_descr, tgt);
    }
    server_iocontext_update_target(s_ctxt, tgt);
  }
  //  if ((r->first_msg) && ((server_global_state.clarisse_pars.external_buffering == CLARISSE_EXTERNAL_BUFFERING_DISABLED)))
  if (r->first_msg)
    server_iocontext_update_client_rt_info(s_ctxt, client_idx, r->partitioning_stride_factor, r->nprocs);
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, "serv_op_write before serv_write_task");
#endif
  if (s_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE){
#ifdef CLARISSE_EXTERNAL_BUFFERING      
    if (server_global_state.clarisse_pars.external_buffering == CLARISSE_EXTERNAL_BUFFERING_ENABLED){
      if (coll_flag)
	serv_write_task_listio_buffering2(s_ctxt, r, client_idx, source);
      else
	serv_write_task_listio_buffering2_ind(s_ctxt, r, client_idx, source);
	
    }
    else
#endif
      {
	serv_write_task_listio(s_ctxt, r, client_idx, source);
	if ((!coll_flag) && (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_ON_DEMAND))
          flush_dcache_buffers(s_ctxt);
      }
  }
  else
    serv_write_task(s_ctxt, r, client_idx, source);
  
#ifdef CLARISSE_DCACHE_DEBUG
  dcache_print(&s_ctxt->dcache, "serv_op_write END");
#endif 
  CLARISSE_UNUSED(cnt);
} 




void serv_op_read_coll(struct rq_file_read * r, int client_idx, int source, int cnt, int coll_flag){
  if (cnt > (int) sizeof(int)) {
    clarisse_target_t *tgt;
    server_iocontext_t *s_ctxt;
    int err;
 
    s_ctxt = server_iocontext_find(r->global_descr);
    if (!s_ctxt)
       s_ctxt = server_iocontext_init(r->global_descr);

    HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
    if (!tgt) {
      char *filename;
      
      assert (r->first_msg > 0);
      filename = (char *)(r + 1) /*+ r->view_size*/;
      tgt = clarisse_target_alloc_init(filename, r->global_descr, CLARISSE_TARGET_FILE, NULL, NULL); 
      HASH_ADD_INT(server_global_state.active_targets, global_descr, tgt);
    }
   
    server_iocontext_update_target(s_ctxt, tgt);
    if (r->first_msg)
      server_iocontext_update_client_rt_info(s_ctxt, client_idx, r->partitioning_stride_factor, r->nprocs);

    if (s_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE){
#ifdef CLARISSE_EXTERNAL_BUFFERING      
      if (server_global_state.clarisse_pars.external_buffering == CLARISSE_EXTERNAL_BUFFERING_ENABLED)
	if (coll_flag)
	  serv_read_task_listio_buffering2(s_ctxt, r, client_idx, source);
	else
	  serv_read_task_listio_buffering2_ind(s_ctxt, r, client_idx, source);
      else 
#endif
	if (coll_flag)
	  serv_read_task_listio(s_ctxt, r, client_idx, source, RT_FILE_READ_COLL);
	else
	  serv_read_task_listio(s_ctxt, r, client_idx, source, RT_FILE_READ);
    }
    else {
      serv_read_task(s_ctxt, r, client_idx, source);
      serv_read_n_shares(client_idx);
    }
    if (server_global_state.clarisse_pars.external_buffering != CLARISSE_EXTERNAL_BUFFERING_ENABLED) {
      err = MPI_Waitall(s_ctxt->client_rt_info[client_idx].mpi_rq_pool.cnt, s_ctxt->client_rt_info[client_idx].mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
      if (err != MPI_SUCCESS)
	handle_err(err, "SERV waitall error\n");
      free_mpi_rq(&s_ctxt->client_rt_info[client_idx].mpi_rq_map); 
    }
  }
}


void serv_op_read_coll_future(struct rq_file_read * r, int client_idx, int source, int cnt){
  clarisse_target_t *tgt;
  server_iocontext_t *s_ctxt;
 
  s_ctxt = server_iocontext_find(r->global_descr);
  if (!s_ctxt)
    s_ctxt = server_iocontext_init(r->global_descr);

  HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
  if (!tgt) {
    char *filename;
    
    assert (r->first_msg > 0);
    filename = (char *)(r + 1); 
    tgt = clarisse_target_alloc_init(filename, r->global_descr, CLARISSE_TARGET_FILE_FUTURE, NULL, NULL); 
    HASH_ADD_INT(server_global_state.active_targets, global_descr, tgt);
  }
   
  server_iocontext_update_target(s_ctxt, tgt);
  if (r->first_msg)
    server_iocontext_update_client_rt_info(s_ctxt, client_idx, r->partitioning_stride_factor, r->nprocs);

  // Check if the write is available
  // insert in a queue otherwise

  if (s_ctxt->clarisse_pars.collective == CLARISSE_LISTIO_COLLECTIVE)
    serv_read_task_listio(s_ctxt, r, client_idx, source, RT_FILE_READ_COLL);
  else {
    serv_read_future_task(s_ctxt, r, client_idx, source);
    //serv_read_n_shares(client_idx);
  }
  /*
  err = MPI_Waitall(s_ctxt->client_rt_info[client_idx].mpi_rq_pool.cnt, s_ctxt->client_rt_info[client_idx].mpi_rq_pool.mem, MPI_STATUSES_IGNORE);
  if (err != MPI_SUCCESS)
    handle_err(err, "SERV waitall error\n");
  free_mpi_rq(&s_ctxt->client_rt_info[client_idx].mpi_rq_map); 
  */
  CLARISSE_UNUSED(cnt);
}

#ifdef CLARISSE_EXTERNAL_BUFFERING
void serv_op_buffer_map(struct rq_file_buffer_map * r, int op, int client_idx, int source) {
  server_iocontext_t *s_ctxt;
  clarisse_off_t crt_l_d;
  int i = 0;

  s_ctxt = server_iocontext_find(r->global_descr);
  if (!s_ctxt)
    s_ctxt = server_iocontext_init(r->global_descr);

  for (crt_l_d = r->l_d; crt_l_d <= r->r_d; crt_l_d += (clarisse_off_t)((clarisse_off_t) s_ctxt->clarisse_pars.buffer_size * (clarisse_off_t) r->active_nr_servers), i++) {
    shared_buffer_t *sbp;
    HASH_FIND_LLINT(s_ctxt->shared_buffer_pool, &crt_l_d, sbp);
    if (!sbp) {
      sbp = shared_buffer_alloc_init(crt_l_d);
      assert(crt_l_d % s_ctxt->clarisse_pars.buffer_size == 0);
      HASH_ADD_LLINT(s_ctxt->shared_buffer_pool, block_off, sbp);
    }
    if (CHECK_NTH_BIT(r->updated_block_mask, i)){
      if (op == RQ_FILE_WRITE_BUFFER_MAP)
	sbp->coll_op.total_producers++;
      else
	sbp->coll_op.total_consumers++;
    }
    sbp->coll_op.active_flag = 1;
  }
  int ret = 0;
  ret = MPI_Send(&ret, 1, MPI_INT,  source, (op == RQ_FILE_WRITE_BUFFER_MAP)?RT_FILE_WRITE_BUFFER_MAP:RT_FILE_READ_BUFFER_MAP, server_global_state.intercomms[client_idx]);
  if (ret != MPI_SUCCESS) {
    handle_error(ret, "serv_op_buffer_map: Error sending rets");
  }
}
#endif

void serv_op_setview(struct rq_file_set_view * r, int client_idx) {
  MPI_Datatype dt;
  server_iocontext_t *s_ctxt;
  int global_descr;
  
  global_descr = r->global_descr;
  s_ctxt = server_iocontext_find(global_descr);
  if (!s_ctxt)
    s_ctxt = server_iocontext_init(global_descr);
      
  if (s_ctxt->local_target == NULL) {
    clarisse_target_t *tgt;
    char *filename;
    
    HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
    if (!tgt) {
      filename = (char *)(r + 1);
      tgt = clarisse_target_alloc_init(filename, r->global_descr, CLARISSE_TARGET_FILE, NULL, NULL); 
      HASH_ADD_INT(server_global_state.active_targets, global_descr, tgt);
    }
    server_iocontext_update(s_ctxt, tgt, client_idx, r->partitioning_stride_factor, r->nprocs);
  }

  dt = reconstruct_dt((char *)(r + 1) + r->target_name_size, r->view_size);
  distrib_update(s_ctxt->client_rt_info[client_idx].remote_view_distr[r->rank], dt, r->displ);
}

void serv_op_delete(char * filename, int client_idx, int source){
  clarisse_target_t *tgt;
  int global_descr;
  struct rt_file_delete fd_ret;
  int ret;

  global_descr = clarisse_hash((unsigned char *)filename);
  HASH_FIND_INT(server_global_state.active_targets, &global_descr, tgt);
  if (tgt) {
    server_iocontext_t *s_ctxt;

    HASH_DEL(server_global_state.active_targets, tgt);
    clarisse_free(tgt);
    s_ctxt = server_iocontext_find(global_descr);
    if (s_ctxt) {
      if (server_global_state.clarisse_pars.external_buffering == CLARISSE_EXTERNAL_BUFFERING_DISABLED) {
	if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK){
	  tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 1);
	  tpool_wait_for_task_queue_empty(server_global_state.tpool, (void*)s_ctxt);
	  tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 0);
	  //    tpool_task_queue_destroy(server_global_state.tpool, (void*)s_ctxt);
	  dcache_atomic_bufpool_move_all(&s_ctxt->dcache, server_global_state.buf_pool);
#ifdef CLARISSE_DCACHE_DEBUG
	  dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
	}
	else
	  drop_buffers(s_ctxt);
	s_ctxt->local_target = NULL;
      }
    }
  }
  fd_ret.errn = 0;
  ret = MPI_Send(&fd_ret, sizeof(struct rt_file_delete), MPI_BYTE,  source, RT_FILE_DELETE, server_global_state.intercomms[client_idx]);
  if (ret != MPI_SUCCESS) {
    handle_error(ret, "serv_op_delete: Error sending the rets");
  }
}

void serv_op_flush(struct rq_file_flush *r, int client_idx, int source){
  clarisse_target_t *tgt;
  struct rt_file_flush fd_ret;
  int ret, global_descr;

  global_descr = r->global_descr;
  HASH_FIND_INT(server_global_state.active_targets, &global_descr, tgt);
  if (tgt) {
    server_iocontext_t *s_ctxt;

    s_ctxt = server_iocontext_find(global_descr);
    if (s_ctxt) {
      if (s_ctxt->clarisse_pars.write_type == CLARISSE_WRITE_BACK){
	tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 1);
	tpool_wait_for_task_queue_empty(server_global_state.tpool, (void*)s_ctxt);
	tpool_set_queue_priority(server_global_state.tpool, (void*)s_ctxt, 0);
	//    tpool_task_queue_destroy(server_global_state.tpool, (void*)s_ctxt);
	dcache_atomic_bufpool_move_all(&s_ctxt->dcache, server_global_state.buf_pool);
#ifdef CLARISSE_DCACHE_DEBUG
	dcache_print(&s_ctxt->dcache, (char *)__FUNCTION__);
#endif
      }
      else 
	flush_buffers(s_ctxt);
    }
  }
  fd_ret.errn = 0;
  ret = MPI_Send(&fd_ret, sizeof(struct rt_file_flush), MPI_BYTE,  source, RT_FILE_FLUSH, server_global_state.intercomms[client_idx]);
  if (ret != MPI_SUCCESS) {
    handle_error(ret, "serv_op_flush: Error sending the rets");
  }
}

void serv_op_get_size(struct rq_file_get_size *r, int client_idx, int source){
  struct rt_file_get_size rt;
  int ret;

  if ( server_global_state.clarisse_pars.file_partitioning == CLARISSE_WORKFLOW_FILE_PARTITIONING)
    rt.size = LLONG_MAX;
  else {
    int global_descr;
    server_iocontext_t *s_ctxt;

    global_descr = r->global_descr;
    
    s_ctxt = server_iocontext_find(global_descr);
    if (!s_ctxt)
      s_ctxt = server_iocontext_init(global_descr);
    
    if (s_ctxt->local_target == NULL) {
      clarisse_target_t *tgt;
      char *filename;
      
      HASH_FIND_INT(server_global_state.active_targets, &r->global_descr, tgt);
      if (!tgt) {
	filename = (char *)(r + 1);
	tgt = clarisse_target_alloc_init(filename, r->global_descr, CLARISSE_TARGET_FILE, NULL, NULL); 
	HASH_ADD_INT(server_global_state.active_targets, global_descr, tgt);
      }
      server_iocontext_update_target(s_ctxt, tgt);
    }
    
    rt.size = s_ctxt->local_target->size; 
  }
  ret = MPI_Send(&rt, sizeof(struct rt_file_get_size), MPI_BYTE,  source, RT_FILE_GET_SIZE, server_global_state.intercomms[client_idx]);
  if (ret != MPI_SUCCESS) {
    handle_error(ret, "serv_op_get_size: Error sending the rets");
  }
#ifdef CLARISSE_SERVER_DEBUG      
  printf("GETSIZE= %lld\n", rt.size);
#endif
}


void serv_op_delay(double t, int client_idx, int source) {
  int ret = 0;
  delay = t;
  ret = MPI_Send(&ret, 1, MPI_INT,  source, RT_SERVER_DELAY, server_global_state.intercomms[client_idx]);
  if (ret != MPI_SUCCESS) {
    handle_error(ret, "serv_op_delete: Error sending the rets");
  }
}
