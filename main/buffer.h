#ifndef BUFFER_H
#define BUFFER_H

#include "server_iocontext.h"
#include "buffer_pool.h"
#include "target.h"
#include "clarisse.h"
#include <stdio.h>

void flush_buffer(server_iocontext_t * s_ctxt, clarisse_off_t offset);
void drop_buffer(server_iocontext_t * s_ctxt, clarisse_off_t offset);
void flush_buffers(server_iocontext_t * s_ctxt);
void flush_dcache_buffers(server_iocontext_t * s_ctxt);
void drop_buffers(server_iocontext_t * s_ctxt);
void flush_dirty_buffer(clarisse_target_t *tgt, buffer_unit_t * bu);

#endif /*BUFFER_H_*/
