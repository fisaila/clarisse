
#include "mpi.h"
#include "clarisse.h"
#include "hdr.h"
#include "server.h"

//server_global_state_t server_global_state;

int main( int argc, char *argv[] )
{
  //int myrank;
  setenv("CLARISSE_COUPLENESS", "dynamic", 1 /*overwrite*/);
  setenv("CLARISSE_SERVER", "enable", 1 /*overwrite*/);
  MPI_Init(&argc, &argv);
  //MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  //printf("CLARISSE server %d started\n", myrank);
  
  //server_init(argc, argv);
  server_operate();
  //server_finalize();
  //MPI_Comm c;
  //MPI_Comm_get_parent(&c);
  //MPI_Comm_free(&c);

  //printf("CLARISSE server %d finalized\n", myrank);
  
  MPI_Finalize();

  return 0;
}
