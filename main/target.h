#ifndef TARGET_H
#define TARGET_H

#include <stdio.h>
#include "list.h"
#include "map.h"
#include "uthash.h" 
#include "clarisse.h"


#define CLARISSE_TARGET_FILE         0
#define CLARISSE_TARGET_MEMORY       1
#define CLARISSE_TARGET_MPI_FILE     2
#define CLARISSE_TARGET_VIEW         3
#define CLARISSE_TARGET_FILE_FUTURE  4

typedef union clarisse_handle_t{
  int fd;
  char *buf;
  void *mpi_fh;
  //void *clarisse_fd;
} clarisse_handle_t;


// Target: memory and file by now
// FILE: 
// type: CLARISSE_TARGET_FILE
// name: name in the file system 
// distr: if it is just a subfile of the file, this is the mapping
// global handle: a system-wide unique int representing the file
// local handle: a file descriptor if a file has been locally open
// MEMORY
// type: CLARISSE_TARGET_MEMORY
// name: optional: "node_name:buf_address"
// distr: mapping from another memory space to this space
// global handle: -1
// local handle: local memory address
// MPI file
// type: CLARISSE_TARGET_MPI_FILE
// name: optional: name in the file system 
// distr: mapping from another memory space to this space
// global handle: -1
// local handle: an MPI handle if a file has been locally open
// VIEW
// type: CLARISSE_TARGET_VIEW
// name: name in the file system 
// distr: mapping from another memory space to this space
// global handle:  a system-wide unique int representing the file
// local handle:  a file descriptor if a file has been locally open




typedef struct {
  UT_hash_handle hh;
  int global_descr; // a unique system wide identifier  
  int type; // type : memory, file, object
  char name[CLARISSE_MAX_TARGET_NAME_SIZE]; // name if name space available
  distrib_t *distr; // data distribution (it is not, but could be a function)
  clarisse_handle_t local_handle; // e.g. file descriptor or buf address
  //  clarisse_off_t fsize_ondisk; // MAX fsize for what it has been committed to disk 
  //  clarisse_off_t fsize_real;   // MAX fsize for what it has been written to memory
  clarisse_off_t size;
} clarisse_target_t;

void clarisse_target_init(clarisse_target_t *tgt, const char *name, int global_descr, int type, distrib_t* distr, void * handle);
clarisse_target_t * clarisse_target_alloc_init(const char *name, int global_descr, int type, distrib_t* distr, void * handle);
clarisse_target_t * clarisse_target_find_alloc_init_add(clarisse_target_t **target_map, char *name, int type, distrib_t* distr, void * handle);
clarisse_target_t * clarisse_target_clone(clarisse_target_t *orig_tgt);
/*
void clarisse_target_free(clarisse_target_t *tgt);
void clarisse_target_free_content(clarisse_target_t *tgt);
*/

#endif




