#ifndef OFF_LEN_H
#define OFF_LEN_H


typedef struct { 
  struct dllist_link link;
  long long int *offs;      
  long long int *lens; 
  int metadata_size;
  short last_msg;
  int client_idx;
  int source;
} off_len_block_llint_node_t;

typedef struct { 
  struct dllist_link link;
  long long int initial_off;
  int *offs;      
  int *lens; 
  int metadata_size;
  //short last_msg;
} off_len_block_node_t;


#endif
