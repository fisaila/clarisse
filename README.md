# CLARISSE #

CLARISSE is a software platform targeting to facilitate the design and implementation of cross-layer mechanisms 
for the the I/O software stack of large scale HPC infrastructures. 

Details about CLARISSE design and implementation can be read in: 

Florin Isaila, Jesus Carretero, Rob Ross. *CLARISSE: a middleware for data-staging coordination and control on 
large-scale HPC platforms* In Proceedings of IEEE/ACM CCGrid 2016 (Best Paper Award). 
[ pdf ](https://www.arcos.inf.uc3m.es/wp-content/uploads/sites/46/2017/01/2016_ccgrid.pdf)


### Software functionality ###
The CLARISSE software offers: 

* a publish/subscribe library for building distributed control algorithms for large scale HPC platforms running MPI applications 
* two control algorithms 
	* elastic collective I/O 
	* parallel I/O scheduling 
* an implementation of the MPI-IO interface  
* two implementations of collective I/O techniques 
	* view-based I/O 
	* list-based collective I/O 
* support for two native versions of MPI-IO views 
	* lazy views 
	* eager views 
* support for asynchronous collective I/O 
* support for 
	* independent MPI applications 
	* MPI applications running as subspaces of MPI_COMM_WORLD and interconnected through MPI inter-communicators 
	* independent MPI applications with own MPI_COMM_WORLD and interconnected through MPI inter-communicators 
* support for data-parallel task communication of MPI workflows in conjunction with the [ Bufferflow ](https://github.com/robertbindar/CLARISSE-AdaptiveBuffering) software

### Building ###

#### Supported target platforms ####

The CLARISSE software has been successfully  built and evaluated on the following platforms: 

* Linux clusters 
* Blue Gene/Q systems (Blue Gene systems from Argonne National Laboratory: Mira, Vesta, Cetus) 
* Cray systems (Archer XC30 Cray system) 


#### Building process ####
1. Download the code
2. $ cd clarisse 
3. $ mkdir build 
4. $ cd build 
5. $ ccmake ..
6. Configure the desired options as described below and press c for configuring (several times until the g option appears) 
7. Press g for generating the Makefile
8. Press q for exiting the ccmake
9. make 

#### Important build options ####

Useful build options for the build process described above based on ccmake tool

* BG_SYSTEM: Blue Gene system is automatically identified in the build process
* CRAY_SYSTEM: Cray system  is automatically identified in the build process
* CLARISSE_CONTROL: The publish/subscribe control is enabled (ON) or disabled (OFF)
* CLARISSE_EXTERNAL_BUFFERING: Support for workflow-aware buffering is enabled (ON) or disabled (OFF)
* CLARISSE_IOLOGY: Support for external I/O prediction is enabled (ON) or disabled (OFF)
* CLARISSE_TIMING: Support for timing of main operations is enabled (ON) or disabled (OFF)

