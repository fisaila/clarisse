#include <string.h>
#include <stdio.h>
#include "buffer_pool.h"
#include "util.h"

// Get from static pool until empty
void test1(){ 
  buffer_pool_t * b = bufpool_init_static(BPOOL_NONCONCURRENT_ACCESS, 4, 2);
  while (!bufpool_empty(b)) {
    bufpool_print(b);
    bufpool_get(b);
  }
  bufpool_print(b);
  bufpool_free(b);
}

// Put into static pool up to max
void test2(){
  int i = 0, j;
  buffer_unit_t * bu[4];
  buffer_pool_t * b = bufpool_init_static(BPOOL_NONCONCURRENT_ACCESS, 4, 2);
  while (!bufpool_empty(b)) {
    //bufpool_print(b);
    bu[i++] = bufpool_get(b);
  }
  bufpool_print(b);
  for (j = 0; j < i; j++) {
    bufpool_put(b, bu[j]);
    bufpool_print(b);
  }
  bufpool_free(b);
}

// Get from dynamic pool until empty
void test3(){ 
  buffer_pool_t * b = bufpool_init_dynamic(BPOOL_NONCONCURRENT_ACCESS, 4, 2, 4, 1);
  while (!bufpool_empty(b)) {
    //buffer_unit_t * bu;
    bufpool_print(b);
    bufpool_get(b);
  }
  bufpool_print(b);
  bufpool_free(b);
}

// Put into static pool up to max
void test4(){
  int i = 0, j;
  buffer_unit_t * bu[4];
  buffer_pool_t * b = bufpool_init_dynamic(BPOOL_NONCONCURRENT_ACCESS, 4, 2, 4, 1);
  while (!bufpool_empty(b)) {
    //bufpool_print(b);
    bu[i++] = bufpool_get(b);
  }
  bufpool_print(b);
  for (j = 0; j < i; j++) {
    bufpool_put(b, bu[j]);
    bufpool_print(b);
  }
  bufpool_free(b);
}


int main(){

  //test1();
  //test2();
  //test3();
  test4();
  return 0;
}
