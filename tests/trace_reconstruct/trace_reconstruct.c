#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>	
#include <unistd.h>
#include <stdlib.h>
#include "clarisse_internal.h"
#include "util.h"

int main(int argc, char **argv)
{	
#ifdef CLARISSE_EXPORT_ACCESS_PATTERN
  int fd;
  if (argc < 2) {
    fprintf(stderr, "Missing argument, call %s trace_filename\n", argv[0]);
    exit(1);
  }

  if ((fd = open(argv[1], O_RDONLY, 0666)) < 0){
    perror("Open access pattern log file");
    exit(1);
  }
  int nprocs, buffer_size, nr_servers, myrank;
  
  read(fd, &nprocs, sizeof(int));
  read(fd, &buffer_size, sizeof(int));
  read(fd, &nr_servers, sizeof(int));
  read(fd, &myrank, sizeof(int));
  printf("nprocs=%d buffer_size=%d nr_servers=%d myrank=%d\n",nprocs, buffer_size, nr_servers, myrank);

  int n;
  clarisse_listio_log_t log_rec;
  while ((n = read(fd, &log_rec, sizeof(clarisse_listio_log_t))) > 0) {
    printf("\tlog_ios_rank=%d global_descr=%d global_offset=%lld off_len_list_size=%d", log_rec.log_ios_rank, log_rec.global_descr, log_rec.global_offset, log_rec.off_len_list_size);  
    int *offs;
    int *lens;
    offs = (int *) malloc(log_rec.off_len_list_size);
    lens = (int *) malloc(log_rec.off_len_list_size);
    read(fd, offs, log_rec.off_len_list_size);
    read(fd, lens, log_rec.off_len_list_size);

    printf("\n\t\t");
    int i;
    for (i = 0; i < log_rec.off_len_list_size  / (int)sizeof(int); i++)
      printf(" off = %d len = %d", offs[i], lens[i]);
    printf("\n");
    free(offs);
    free(lens);
  }
  close(fd);
#else
  CLARISSE_UNUSED(argc);
  CLARISSE_UNUSED(argv);
#endif
  return 0;
}


