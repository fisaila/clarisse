#include "thr_pool.h"
#include <stdio.h>
#include <unistd.h>

#define WAIT_WORKERS_TO_DRAIN_QUEUE 1

static pthread_mutex_t count_mutex = PTHREAD_MUTEX_INITIALIZER;
int count;


void task0(void *ptr){

  int *pval = (int*)ptr;
  int i;
  
  printf("slow task: count value is %d.\n",count);
  for (i = 0; i < 10000000; i++) {
    (*pval)++;
  }
  sleep(1);
  pthread_mutex_lock(&count_mutex);
  count++;
  pthread_mutex_unlock(&count_mutex);
}

void task1(void *ptr)
{
  printf("fast task: count value is %d.\n",count);
  pthread_mutex_lock(&count_mutex);
  count++;
  pthread_mutex_unlock(&count_mutex);
}

void test0(){
  tpool_t thread_pool;
  int val;
  int DO_NOT_BLOCK_WHEN_FULL = 1;
  int MAX_THREADS = 10;
  int MAX_QUEUE = 10;

  printf("%s\n", __FUNCTION__);
  tpool_init(&thread_pool, MAX_THREADS, MAX_QUEUE, DO_NOT_BLOCK_WHEN_FULL);	
  tpool_add_work(thread_pool, task0, (void *) &val); 
  tpool_destroy(thread_pool, WAIT_WORKERS_TO_DRAIN_QUEUE);
}

void test1(){
  tpool_t thread_pool;
  int val;
  int DO_NOT_BLOCK_WHEN_FULL = 1;
  int MAX_THREADS = 1;
  int MAX_QUEUE = 1;
  
  printf("%s\n", __FUNCTION__);
  tpool_init(&thread_pool, MAX_THREADS, MAX_QUEUE, DO_NOT_BLOCK_WHEN_FULL);	
  tpool_add_work(thread_pool, task0, (void *) &val); 
  tpool_add_work(thread_pool, task0, (void *) &val); 
  tpool_destroy(thread_pool, WAIT_WORKERS_TO_DRAIN_QUEUE);
}

void test2(){
  tpool_t thread_pool;
  int val;
  int DO_NOT_BLOCK_WHEN_FULL = 0;
  int MAX_THREADS = 1;
  int MAX_QUEUE = 1;
  
  printf("%s\n", __FUNCTION__);
  tpool_init(&thread_pool, MAX_THREADS, MAX_QUEUE, DO_NOT_BLOCK_WHEN_FULL);	
  tpool_add_work(thread_pool, task0, (void *) &val); 
  tpool_add_work(thread_pool, task0, (void *) &val); 
  tpool_destroy(thread_pool, WAIT_WORKERS_TO_DRAIN_QUEUE);
}


int main(){
  test0();
  test1();
  test2();
  return 0;
}
