#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "map.h"

/* func_sup test */

MPI_Datatype get_contig();    //0
MPI_Datatype get_lb_contig(); //1
MPI_Datatype get_contig_ub(); //2
MPI_Datatype get_simple_vec(); //3
MPI_Datatype get_displ_simple_vec(); //4
MPI_Datatype get_simple_struct(); //5
MPI_Datatype get_displ_displ_struct(); //6
MPI_Datatype get_vect_struct(); //7
MPI_Datatype get_darray();

#define MAX_CASES_PER_TEST 1024

int test_func_sup(const char *funname, char *testname, int test_cnt, MPI_Datatype *dt, long long int *offset_input, int *expected_output) {
  int i, ret = 0;
  distrib_t *distr = distrib_get(*dt, 0);

  assert(test_cnt <= MAX_CASES_PER_TEST);

  for (i=0; i<test_cnt; i++) {
    int output;
    if ((output = func_sup(offset_input[i], distr)) != expected_output[i]){
      printf("func_sup test %s:%s failed on input %d with output %d (expected %d)!!!\n", funname, testname, (int)offset_input[i], output, expected_output[i] );
      distrib_print(distr);
      distrib_draw(distr, 1);
      ret = -1;
      break;
    }
  }
  distrib_free(distr);
  MPI_Type_free(dt);
  return ret; 
}

//  0         1         
//  012345678901234
//  **        
//I:01 
//O:01
int test1_0() {
  MPI_Datatype dt = get_contig();
  long long int offset_input[]={0,1,2,3,4};
  int  expected_output[]=    {0,1,2,3,4};
  return test_func_sup(__func__,"CONTIG",5, &dt, offset_input, expected_output);
}

//  0         1         
//  012345678901234
//  -------[---****        
//I: all above 
//O:000000000000123
int test1_1() {
  MPI_Datatype dt = get_lb_contig();
  long long int offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,18,19,22,23,30,31,32};
  int  expected_output[]=    {0,0,0,0,0,0,0,0,0,0, 0, 0, 1, 2, 3, 4, 4, 4, 7, 8,11,12,12};
  return test_func_sup(__func__,"LB CONTIG",23, &dt, offset_input, expected_output);
}

//  0         1         
//  012345678901234
//  ---****---]        
//
//I:                
//O:000000001234444
int test1_2() {
  MPI_Datatype dt = get_contig_ub();
  long long int offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11,14,17,18,21};
  int  expected_output[]=    {0,0,0,0,1,2,3,4,4,4, 4, 5, 8, 8, 9,12};
  return test_func_sup(__func__,"CONTIG UB",10, &dt, offset_input, expected_output);
}

//  0         1         
//  012345678901234
//  *-*        
//I:012 
//O:011
int test1_3() {
  MPI_Datatype dt = get_simple_vec();
  long long int offset_input[]={0,1,2,3,4,5,6,7,8,9};
  int  expected_output[]=    {0,1,1,2,3,3,4,5,5,6};
  return test_func_sup(__func__,"SIMPLE VECTOR",10, &dt, offset_input, expected_output);
}

//  0         1         
//  012345678901234
//  ___[_*_**_*____]
//              1
//I: as above
//O:000000112334444
int test1_4() {
  MPI_Datatype dt = get_displ_simple_vec();
  long long int offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
  int  expected_output[] =   {0,0,0,0,0,0,1,1,2,3, 3, 4, 4, 4, 4, 4, 4, 4, 5, 5};
  return test_func_sup(__func__,"DISPL SIMPLE VECT",20, &dt, offset_input, expected_output);
}

//  0         1         
//  01234567890123456789
//  _______[___****____]
//                  1
//I: as above
//O:0000000000001234444
int test1_5() {
  MPI_Datatype dt = get_simple_struct();
  long long int offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,23,39};
  int  expected_output[] =   {0,0,0,0,0,0,0,0,0,0, 0, 0, 1, 2, 3, 4, 4, 4, 4, 4, 4,12};
  return test_func_sup(__func__,"SIMPLE STRUCT",22, &dt, offset_input, expected_output);
}

//  0         1         
//  01234567890123456789
//  ________**
//                  
//I: as above 
//O:0000000001
int test1_6() {
  MPI_Datatype dt = get_displ_displ_struct();
  long long int offset_input[]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14};
  int  expected_output[] =   {0,0,0,0,0,0,0,0,0,1, 2, 3, 4, 5, 6};
  return test_func_sup(__func__,"DISPL DISPL STRUCT",15, &dt, offset_input, expected_output);
}

// 0         1         2         3         4         5         6         7         8         9         
// 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
// ********____****____****____****____*_______________________________________________________________
// ________________________****____****____****____*___________________________________________________
// ____________________________________****____****____****____*_______________________________________
// ________________________________________________****____****____****____*___________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// *___**__
//I: as above
//O:1234567888889 

int test1_7() {
  MPI_Datatype dt = get_vect_struct();
  long long int offset_input[]={0,7,8,12,13,20,24,36,99,123,124,147,148,149,350,371,372,999,1000,1001,1004,1005,1006,1008,1009,2016};
  int  expected_output[] =   {0,7,8, 8, 9,12,16,20,21, 21, 21, 33, 33, 34, 49, 59, 59, 60,  60,  61,  61,  62,  63,  63,  64, 126};
  return test_func_sup(__func__,"VECT STRUCT",26, &dt, offset_input, expected_output);
}

int main(int argc, char* argv[]) {
    int ret;
    MPI_Init(&argc, &argv);

    ret = test1_0();
    if (ret >= 0) ret = test1_1();
    if (ret >= 0) ret = test1_2();
    if (ret >= 0) ret = test1_3();
    if (ret >= 0) ret = test1_4();
    if (ret >= 0) ret = test1_5();
    if (ret >= 0) ret = test1_6();
    if (ret >= 0) ret = test1_7();
 
    if (ret >= 0) printf ("func_sup tests passed!!\n");

    MPI_Finalize();
    return ret;
}  // main 
