#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "map.h"

/* find_size_bin_search test */

MPI_Datatype get_contig();    //0
MPI_Datatype get_lb_contig(); //1
MPI_Datatype get_contig_ub(); //2
MPI_Datatype get_simple_vec(); //3
MPI_Datatype get_displ_simple_vec(); //4
MPI_Datatype get_simple_struct(); //5
MPI_Datatype get_displ_displ_struct(); //6
MPI_Datatype get_vect_struct(); //7
MPI_Datatype get_darray();

#define MAX_CASES_PER_TEST 1024

int test_find_size_bin_search(const char *funname, char *testname, int test_cnt, MPI_Datatype *dt, long long int *offset_input, int *expected_output) {
  int i, ret = 0;
  distrib_t *distr = distrib_get(*dt, 0);

  assert(test_cnt <= MAX_CASES_PER_TEST);

  for (i=0; i<test_cnt; i++) {
    int output;
    if ((output = find_size_bin_search(offset_input[i], distr)) != expected_output[i]){
      printf("find_size_bin_search test %s:%s failed on input %d with output %d (expected %d)!!!\n", funname, testname, (int)offset_input[i], output, expected_output[i] );
      distrib_print(distr);
      distrib_draw(distr, 1);
      ret = -1;
      break;
    }
  }
  distrib_free(distr);
  MPI_Type_free(dt);
  return ret; 
}

//  0         1         
//  012345678901234
//  **        
//I:01 
//O:01
int test1_0() {
  MPI_Datatype dt = get_contig();
  long long int offset_input[]={0,1};
  int  expected_output[]=    {0,1};
  return test_find_size_bin_search(__func__,"CONTIG",2, &dt, offset_input, expected_output);
}

//  0         1         
//  012345678901234
//  -------[---****        
//I:           0123 
//O:           4567
int test1_1() {
  MPI_Datatype dt = get_lb_contig();
  long long int offset_input[]={0,1,2,3};
  int  expected_output[]=    {4,5,6,7};
  return test_find_size_bin_search(__func__,"LB CONTIG",4, &dt, offset_input, expected_output);
}

//  0         1         
//  012345678901234
//  ---****---]        
//I:   0123 
//O:   0123
int test1_2() {
  MPI_Datatype dt = get_contig_ub();
  long long int offset_input[]={0,1,2,3};
  int  expected_output[]=    {0,1,2,3};
  return test_find_size_bin_search(__func__,"CONTIG UB",4, &dt, offset_input, expected_output);
}

//  0         1         
//  012345678901234
//  *-*        
//I:0 1 
//O:0 2

int test1_3() {
  MPI_Datatype dt = get_simple_vec();
  long long int offset_input[]={0,1};
  int  expected_output[]={0,2};
  return test_find_size_bin_search(__func__,"SIMPLE VECTOR",2, &dt, offset_input, expected_output);
}

// 0         1         
// 012345678901234
// ___[_*_**_*____]
//              1
//I:    0 12 3 
//O:    2 45 7
int test1_4() {
  MPI_Datatype dt = get_displ_simple_vec();
  long long int offset_input[]={0,1,2,3};
  int  expected_output[] =   {2,4,5,7};
  return test_find_size_bin_search(__func__,"DISPL SIMPLE VECT",4, &dt, offset_input, expected_output);
}

// 0         1         
// 01234567890123456789
// _______[___****____]
//                  1
//I:          0123 
//O:          4567
int test1_5() {
  MPI_Datatype dt = get_simple_struct();
  long long int offset_input[]={0,1,2,3};
  int  expected_output[] =   {4,5,6,7};
  return test_find_size_bin_search(__func__,"SIMPLE STRUCT",4, &dt, offset_input, expected_output);
}

// 0         1         
// 01234567890123456789
// ________**
//                  
//I:       01 
//O:       01
int test1_6() {
  MPI_Datatype dt = get_displ_displ_struct();
  long long int offset_input[]={0,1};
  int  expected_output[] =   {0,1};
  return test_find_size_bin_search(__func__,"DISPL DISPL STRUCT",2, &dt, offset_input, expected_output);
}

// 0         1         2         3         4         5         6         7         8         9         
// 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
// ********____****____****____****____*_______________________________________________________________
// ________________________****____****____****____*___________________________________________________
// ____________________________________****____****____****____*_______________________________________
// ________________________________________________****____****____****____*___________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// *___**__
//I:   12  
//O:   45  
int test1_7() {
  MPI_Datatype dt = get_vect_struct();
  long long int offset_input[]={0,7, 8,11,12,15,16,19,20, 21, 24, 25, 33, 50, 59,  60,  61,  62};
  int  expected_output[] =   {0,7,12,15,20,23,28,31,36,124,127,132,148,351,372,1000,1004,1005};
  return test_find_size_bin_search(__func__,"VECT STRUCT",16, &dt, offset_input, expected_output);
}

int main(int argc, char* argv[]) {
    int ret;
    MPI_Init(&argc, &argv);

    ret = test1_0();
    if (ret >= 0) ret = test1_1();
    if (ret >= 0) ret = test1_2();
    if (ret >= 0) ret = test1_3();
    if (ret >= 0) ret = test1_4();
    if (ret >= 0) ret = test1_5();
    if (ret >= 0) ret = test1_6();
    if (ret >= 0) ret = test1_7();

    if (ret >= 0) printf ("find_size_bin_search tests passed!!\n");

    MPI_Finalize();
    return ret;
}  // main 
