#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "map.h"
#include "util.h"

/* Scatter/gather test */

MPI_Datatype get_contig();    //0
MPI_Datatype get_lb_contig(); //1
MPI_Datatype get_contig_ub(); //2
MPI_Datatype get_simple_vec(); //3
MPI_Datatype get_displ_simple_vec(); //4
MPI_Datatype get_simple_struct(); //5
MPI_Datatype get_displ_displ_struct(); //6
MPI_Datatype get_vect_struct(); //7
MPI_Datatype get_darray();

#define MAX_CASES_PER_TEST 1024

int test_get_offs_lens(const char *funname, char *testname, MPI_Datatype *dt, long long int l, long long int r, int max_size, int expected_idx, long long int expected_l, long long int expected_initial_offset, int *expected_offs, int *expected_lens, int expected_data_size, int expected_metadata_size) {
  int ret = 0, j;
  distrib_t *distr = distrib_get(*dt, 0);
  int *offs, *lens;
  long long int initial_offset;
  long long int initial_l = l;
  int data_size, metadata_size;
  int idx = get_offs_lens5(&l, r, distr, &initial_offset, &offs, &lens, max_size, &data_size,&metadata_size);
  if ((idx == expected_idx) && (data_size == expected_data_size)) {
    for (j = 0; j < idx; j++) {
      if ((offs[j] != expected_offs[j]) || 
	  (lens[j] != expected_lens[j])) 
	break;
    }
  }
  if (((idx > 0) && (initial_offset != expected_initial_offset)) ||(idx != expected_idx) || (j != idx) || (l != expected_l) || (data_size != expected_data_size) || (metadata_size != expected_metadata_size)){
    printf("get_offs_lens test %s:%s failed on inputs l=%lld (returned=%lld expected=%lld) r=%lld with output idx=%d (expected %d) data_size=%d (expected %d) metadata_size=%d (expected %d) intial_offset=%lld (expected %lld)!!!\n", funname, testname, initial_l, l, expected_l, r, idx, expected_idx, data_size, expected_data_size, metadata_size, expected_metadata_size, initial_offset, expected_initial_offset);
    if (idx == expected_idx)
      for (j = 0; j < idx; j++) {
	printf("output_offs=%d expected_offs=%d\toutput_lens=%d expected_lens=%d\n", offs[j], expected_offs[j], lens[j], expected_lens[j]);
      }
    distrib_print(distr);
    distrib_draw(distr, 1);
    ret = -1;
  }
  if (idx > 0)
    clarisse_free(offs);
  if (idx > 0)
    clarisse_free(lens);
  distrib_free(distr);
  MPI_Type_free(dt);
  return ret; 
}



//  0         1         
//  012345678901234
//  **        
int test1_0() {
  int ret = 0;
  int i;
  long long int l[]=            {0, 1, 2, 3, 4, 5};
  long long int r[]=            {4, 4, 4, 4, 4, 4};
  int max_size[] =             {13,11, 9, 8, 9, 8};
  int  expected_idx[]=          {1, 1, 1, 0, 1, 0};
  int  expected_l[]=            {5, 4, 3, 3, 5, 5};
  int expected_data_size[]=     {5, 3, 1, 0, 1, 0};
  int expected_metadata_size[]= {8, 8, 8, 0, 8, 0};
  // last value does not matter as expected idx==0
  long long int expected_initial_offset[6] = {0, 1, 2, 3, 4, 0};
  int expected_offs[6][1]={{0},{0},{0},{0},{0},{1023333}};
  int expected_lens[6][1]={{5},{3},{1},{2},{1},{0}};
  

  for (i = 0; i < 6; i++) {
    MPI_Datatype dt = get_contig();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], expected_initial_offset[i], (int *)expected_offs[i], (int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  
  return ret;
}


//  0         1         2
//  01234567890123456789012
//  -------[---****[---****                 

int test1_1() {
  int ret = 0;
  int i;
  long long int l[]=          {10,10,11,12,13,14,15,11};
  long long int r[]=          {10,14,14,14,14,14,14,21};
  int max_size[] =            { 8,13,11, 9, 8, 9, 8,12};
  int expected_idx[]=         { 0, 1, 1, 1, 0, 1, 0, 1};
  int expected_l[]=           {10,15,14,13,13,15,15,19};
  int expected_data_size[]=   { 0, 4, 3, 1, 0, 1, 0, 4};
  int expected_metadata_size[]={0, 8, 8, 8, 0, 8, 0, 8};
  // last value does not matter as expected idx==0
  long long int expected_initial_offset[8] = {0,11,11,12,13,14, 0,11};
  int expected_offs[8][1]={{0},{0}, {0},{0},{0},{0},{1023333},{0}};
  int expected_lens[8][1]={{5},{ 4}, { 3}, {1}, {0}, {1},{0},{4}};
  
  for (i = 0; i < 8; i++) {
    MPI_Datatype dt = get_lb_contig();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], expected_initial_offset[i], (int *)expected_offs[i], (int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  
  return ret;
}

//  0         1         
//  012345678901234
//  ---****---]        

int test1_2() {
  int ret = 0;
  int i;
  long long int l[]=            {0, 2, 3, 6, 7, 8};
  long long int r[]=            {5, 6, 7, 7, 7,11};
  int max_size[] =             {10,12,12, 9, 8, 9};
  int expected_idx[]=           {1, 1, 1, 1, 0, 1};
  int expected_l[]=             {5, 7, 8, 8, 7,11};
  int expected_data_size[]=     {2, 4, 4, 1, 0, 1};
  int expected_metadata_size[]= {8, 8, 8, 8, 0, 8};
  // last value does not matter as expected idx==0
  long long int expected_initial_offset[6] = {3, 3, 3, 6, 0,10};
  int expected_offs[6][1]={{0},{0},{0},{0},{10003},{0}};
  int expected_lens[6][1]={{2},{4},{4},{1},{0},{1}};
  
  for (i = 0; i < 6; i++) {
    MPI_Datatype dt = get_contig_ub();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], expected_initial_offset[i], (int *)expected_offs[i], (int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)

    if (ret < 0)
      break;
  }
  
  return ret;
}

//  0         1         
//  012345678901234
//  *-**-**-**-*        
int test1_3() {
  int ret = 0;
  int i;
  long long int l[]=            {0, 0, 1, 3, 6, 7, 8};
  long long int r[]=            {2, 5, 6, 7, 7, 7,11};
  int max_size[] =              {9,28,19,19, 9, 8, 9};
  //unoptimized
  //int  expected_idx[]=    {4,4,3,1,0,3};
  int expected_idx[]=           {1, 3, 2, 2, 1, 0, 1};
  int expected_l[]=             {2, 6, 6, 8, 8, 7, 9};
  int expected_data_size[]=     {1, 4, 3, 3, 1, 0, 1};
  int expected_metadata_size[]= {8,24,16,16, 8, 0, 8};
  // some values do not matter when expected idx==0
  // unoptimized
  //long long int expected_offs[6][4]={{0,2,3,5}, {2,3,5,6},{3,5,6},{6},{10003},{8,9,11}};
  long long int expected_initial_offset[7] = {0, 0, 2, 3, 6, 0, 8};
  int expected_offs[7][4]={{0},{0,2,5}, {0,3},{0,2},{0},{10003},{0}};
  // unoptimized
  //long long int expected_lens[6][4]={{1,1,1,1}, {1,1,1,1},{1,1,1},{1},{2},{1,1,1}};
  int expected_lens[7][4]={{1},{1,2,1}, {2,1},{1,2},{1},{2},{1}};
  
  for (i = 0; i < 7; i++) {
    MPI_Datatype dt = get_simple_vec();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], expected_initial_offset[i], (int *)expected_offs[i], (int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  return ret;
}


// 0         1         
// 012345678901234
// ___[_*_**_*____]
int test1_4() {
  int ret = 0;
  int i;
  long long int l[]=            {0, 2, 3, 6, 7, 8, 5};
  long long int r[]=            {5, 6, 7, 7, 7,11, 7};
  int max_size[] =              {9, 9,18, 9, 9,18, 9};
  int  expected_idx[]=          {1, 1, 2, 1, 1, 2, 1};
  int expected_l[]=             {6, 7, 8, 8, 8,12, 7};
  int expected_data_size[]=     {1, 1, 2, 1, 1, 2, 1};
  int expected_metadata_size[]= {8, 8,16, 8, 8,16, 8};
  // some values do not matter when expected idx==0
  long long int expected_initial_offset[7] = {5, 5, 5, 7, 7, 8, 5};
  int expected_offs[7][2]={{0}, {0},{0, 2},{0},{0},{0,2}, {0}};
  int expected_lens[7][2]={{1}, {1},{1, 1},{1},{1},{1,1}, {1}};
  
  for (i = 0; i < 7; i++) {
    MPI_Datatype dt = get_displ_simple_vec();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], expected_initial_offset[i], (int *)expected_offs[i], (int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  return ret;
}


// 0         1         
// 01234567890123456789
// _______[___****____]
int test1_5() {
  int ret = 0;
  int i;
  long long int l[]=          {10,10,11,12,13,14,15};
  long long int r[]=          {10,14,14,14,23,14,14};
  int max_size[] =             {8,12,11, 9,19, 9, 8};
  int expected_idx[]=          {0, 1, 1, 1, 2, 1, 0};
  int expected_l[]=           {10,15,14,13,24,15,15};
  int expected_data_size[]=    {0, 4, 3, 1, 3, 1, 0};  
  int expected_metadata_size[]={0, 8, 8, 8,16, 8, 0};
  // last value does not matter as expected idx==0
  long long int expected_initial_offset[7] = {0,11,11,12,13,14, 0};
  int expected_offs[7][2]={{0},{ 0}, { 0},{0},{0,10},{0},{1023333}};
  int expected_lens[7][2]={{5},{ 4}, { 3},{1},{2, 1},{1},{0}};
  
  for (i = 0; i < 7; i++) {
    MPI_Datatype dt = get_simple_struct();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], expected_initial_offset[i], (int *)expected_offs[i], (int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  
  return ret;
}


// 0         1         
// 01234567890123456789
// ________************
int test1_6() {
  int ret = 0;
  int i;
  long long int l[]=           {10,10,11,12,13,14,15};
  long long int r[]=           {10,14,14,14,20,14,14};
  int max_size[] =             { 9,13,11,11,10, 9, 8};
  int expected_idx[]=          { 1, 1, 1, 1, 1, 1, 0};
  int expected_l[]=            {11,15,14,15,15,15,15};
  int expected_data_size[]=     {1, 5, 3, 3, 2, 1, 0};  
  int expected_metadata_size[]={ 8, 8, 8, 8, 8, 8, 0};
  // last value does not matter as expected idx==0
  long long int expected_initial_offset[7] = {10,10,11,12,13,14,0};
  int expected_offs[7][1]={{0},{0},{0},{0},{0},{0},{1023333}};
  int expected_lens[7][1]={{1},{5},{3},{3},{2},{1},{0}};
  
  for (i = 0; i < 7; i++) {
    MPI_Datatype dt = get_displ_displ_struct();
    ret = test_get_offs_lens(__func__,"CONTIG", &dt, l[i],r[i], max_size[i], expected_idx[i], expected_l[i], expected_initial_offset[i], (int *)expected_offs[i], (int *)expected_lens[i], expected_data_size[i], expected_metadata_size[i]); // (long long int **)
    if (ret < 0)
      break;
  }
  
  return ret;
}



/*

// 0         1         2         3         4         5         6         7         8         9         
// 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
// ********____****____****____****____*_______________________________________________________________
// ________________________****____****____****____*___________________________________________________
// ____________________________________****____****____****____*_______________________________________
// ________________________________________________****____****____****____*___________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// ____________________________________________________________________________________________________
// *___**__
//I:   12  
//O:   45  
int test1_7() {
  MPI_Datatype dt = get_vect_struct();
  long long int offset_input[]={0,7, 8,11,12,15,16,19,20, 21, 24, 25, 33, 50, 59,  60,  61,  62,  63, 112};
  int  expected_output[] =  {0,7, 8,11,12,15,16,19,20, 21, 24, 25, 33, 50, 59,  60,  61,  62,  63, 112}; 
  return test_sg_mem(__func__,"VECT STRUCT",18, &dt, offset_input, expected_output);
}

*/

int main(int argc, char* argv[]) {
    int ret;
    MPI_Init(&argc, &argv);

    ret = test1_0();
    if (ret >= 0) ret = test1_1();
    if (ret >= 0) ret = test1_2();     
    if (ret >= 0) ret = test1_3();
    if (ret >= 0) ret = test1_4();
    if (ret >= 0) ret = test1_5();
    if (ret >= 0) ret = test1_6();
    /*if (ret >= 0) ret = test1_7();

    */
    if (ret >= 0) printf ("get_offs_len5 tests passed!!\n");

    MPI_Finalize();
    return ret;
}  // main 
