#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "test_datatypes.h"
#include "test_commons.h" 
#include "client_iocontext.h"


#define DUMMY_FILENAME "abc__"
#define SIZE 1024

#define WR 0
#define WRRD 1
#define RD 2

#define DTYPE_BYTE 0
#define DTYPE_BYTE_VECTOR 1

#define DO_NOT_ADD_RANK_TO_DISPL 0
#define ADD_RANK_TO_DISPL 1




int lcm(int x, int y); 

char buf1[SIZE+1];
char buf2[SIZE+1];

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);


static MPI_Datatype get_dtype_displ(int type, int nprocs, int myrank, int size,  int add_rank, MPI_Offset *displ) {
  MPI_Datatype d;
  
  switch(type) {
  case DTYPE_BYTE: {
    d = MPI_BYTE;
    if (add_rank == ADD_RANK_TO_DISPL)
      *displ = *displ + myrank * size;
    break;
  }
  case DTYPE_BYTE_VECTOR: {
    d = get_vec_nstride(nprocs, size);
    if (add_rank == ADD_RANK_TO_DISPL)
      *displ = *displ + myrank;
    break;
  }
  }
  return d;
}

static void setview_exec(int access_type, MPI_Datatype d, MPI_Offset displ, MPI_Info info, int n) {
  int myrank, err;
  MPI_File fh;
  MPI_Status status;
  MPI_Comm intracomm = cls_get_client_intracomm();
  int i;
  
  for (i = 0; i < n; i++)
    buf1[i] = 'a' + i % 17;
  buf1[n] = 0;
  bzero(buf2, n + 1);
  MPI_Comm_rank(intracomm, &myrank);  

  if ((access_type == WRRD) || (access_type == WR)){
    MPI_Barrier(intracomm);
  
    err = MPI_File_open(intracomm, DUMMY_FILENAME, MPI_MODE_CREATE | MPI_MODE_RDWR ,
			info, &fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_open");
    
    err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_set_view");
    
    err = MPI_File_write(fh, buf1, n, MPI_BYTE, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_write");

    printf("Producer process %d wrote:%s\n", myrank, buf1);
  
    err = MPI_File_close(&fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_close");
  }

  if ((access_type == WRRD) || (access_type == RD)){
    MPI_Barrier(intracomm);
    err = MPI_File_open(intracomm, DUMMY_FILENAME, MPI_MODE_RDWR ,
			info, &fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_open");
   
    err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_set_view"); 
    
    err = MPI_File_read_at(fh, 0, buf2, n, MPI_CHAR, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_read");

    printf("Consumer process %d read:%s\n", myrank, buf2);
   
    err = MPI_File_close(&fh);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_close");
    /*   
	 if (strncmp(buf1, buf2, n)) {
	 buf1[n]='\0';
	 buf2[n]='\0';
	 fprintf(stderr, "Test %s File %s: \n\tWritten buf=%s\n\tRead buf=%s\n", 
	 __FILE__, DUMMY_FILENAME, buf1, buf2);
	 handle_error(err,"Read a different value than written");
	 }
    */
 }


}

static void setview_test1(int argc, char **argv, int dtype1, int dtype2, MPI_Offset displ, int add_rank, int nprocs[], int n, int sleep_before_write, int sleep_before_read) {
  int myrank, client_id, lcm_c;
  MPI_Comm intracomm;
  char nr_serv[16];
  
  lcm_c = lcm(nprocs[1], nprocs[2]);
 
  sprintf(nr_serv, "%d", nprocs[0]);
  setenv("CLARISSE_NR_SERVERS", nr_serv, 1 /*overwrite*/);
  cls_set_clients(2, &(nprocs[1]));
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(cls_get_allclients_intracomm(), &myrank);

  if (lcm_c * n > SIZE) {
    if (myrank == 0) {
      fprintf(stderr, "Trying to write %d bytes / proc > SIZE = %d\n", lcm_c * n , SIZE);
    }
    MPI_Finalize();
    exit(1);
  }
  else 
    if (myrank == 0) {
      printf("Client 1: nprocs = %d write %d bytes (%d bytes / proc)\n", nprocs[1],  lcm_c * n, lcm_c * n / nprocs[1] );
      printf("Client 2: nprocs = %d read %d bytes (%d bytes / proc)\n", nprocs[2],  lcm_c * n, lcm_c * n / nprocs[2] );
   }

  intracomm = cls_get_client_intracomm();
  MPI_Comm_rank(intracomm, &myrank);
  
  client_id = cls_get_client_id();
  
  if (client_id == 1) {
    int size1 =  n * (lcm_c / nprocs[1]);
    MPI_Datatype d1 = get_dtype_displ(dtype1, nprocs[1], myrank, size1, add_rank, &displ); 
    sleep(sleep_before_write);
    setview_exec(WR, d1, displ, MPI_INFO_NULL, size1);
    free_datatype(d1);
  }
  if (client_id == 2) {
    int size2 = n * (lcm_c / nprocs[2]);
    MPI_Datatype d2 = get_dtype_displ(dtype2, nprocs[2], myrank, size2, add_rank, &displ); 
    sleep(sleep_before_read);
    setview_exec(RD, d2, displ, MPI_INFO_NULL, size2);
    free_datatype(d2);
  } 
  setenv("CLARISSE_TEST", __FILE__, 1);
  MPI_Finalize();
}

static void test_spc_size(int argc, char **argv, int servers, int producers, int consumers, int size, int dtype1, int dtype2, int sleep_before_write, int sleep_before_read) {
  int nprocs[3];
  nprocs[0] = servers; nprocs[1] = producers; nprocs[2] = consumers;
  setview_test1(argc, argv, dtype1, dtype2, 0, ADD_RANK_TO_DISPL, nprocs, size, sleep_before_write, sleep_before_read); //1
}

int main(int argc, char **argv)
{
  char *env;
 
  setenv("CLARISSE_COUPLENESS", "intercomm", 1 /*overwrite*/);
  setenv("CLARISSE_READ_FUTURE", "enable", 1 /*overwrite*/);
  setenv("CLARISSE_EXTERNAL_BUFFERING", "enable", 1 /*overwrite*/);

  setenv("CLARISSE_FILE_PARTITIONING","workflow", 1 /*overwrite*/);
  //setenv("CLARISSE_COLLECTIVE","vb", 1 /*overwrite*/);
  setenv("CLARISSE_WRITE_TYPE","ondemand", 1 /*overwrite*/);
  setenv("CLARISSE_AGGR_THREAD","thread", 1 /*overwrite*/);
  setenv("CLARISSE_MAX_DCACHE_SIZE", "1", 1 /*overwrite*/);
  env = getenv("ENV_FROM_SCRIPT");
  if (!env || strncmp(env, "true", 4)) { 
    //  setenv("CLARISSE_BUFPOOL_SIZE", "16", 1 /*overwrite*/);
    setenv("CLARISSE_BUFPOOL_SIZE", "2", 1 /*overwrite*/);
    //setenv("CLARISSE_LAZY_EAGER_VIEW", "eager", 1 /*overwrite*/);
    setenv("CLARISSE_BUFFER_SIZE", "16", 1 /*overwrite*/);
    //setenv("CLARISSE_NET_BUF_SIZE", "3", 1 /*overwrite*/);
    setenv("CLARISSE_NET_BUF_SIZE", "16", 1 /*overwrite*/);
  }
  

  // argc, argv, servers, producers, consumers, size, type, sleep_before_write, int sleep_before_read

  //test_spc_size(argc, argv, 1, 1, 1, 1,DTYPE_BYTE,DTYPE_BYTE,1, 0);
  //test_spc_size(argc, argv, 1, 1, 1, 1,DTYPE_BYTE,DTYPE_BYTE,0, 1);

  //test_spc_size(argc, argv, 1, 1, 1, 17,DTYPE_BYTE,DTYPE_BYTE,1, 0);
  //test_spc_size(argc, argv, 1, 1, 1, 17,DTYPE_BYTE,DTYPE_BYTE,0, 1);

  //test_spc_size(argc, argv, 2, 1, 1, 1,DTYPE_BYTE,DTYPE_BYTE,1, 0);
  //test_spc_size(argc, argv, 2, 1, 1, 1,DTYPE_BYTE,DTYPE_BYTE,0, 1);
 
  //test_spc_size(argc, argv, 2, 1, 1, 17,DTYPE_BYTE,DTYPE_BYTE,1, 0);
  //test_spc_size(argc, argv, 2, 1, 1, 17,DTYPE_BYTE,DTYPE_BYTE,0, 1);
 
  //test_spc_size(argc, argv, 3, 1, 1, 49,DTYPE_BYTE,DTYPE_BYTE,1, 0);
  test_spc_size(argc, argv, 3, 1, 1, 49,DTYPE_BYTE,DTYPE_BYTE,0, 1);
 

  return 0;
}



int lcm(int x, int y) {
  int myx, myy;

  myx = x;
  myy = y;
  if (x == 0) {
    return y;
  }
 
  while (y != 0) {
    if (x > y) {
      x = x - y;
    }
    else {
      y = y - x;
    }
  }
 
  return (int)  (((long long int) myx * (long long int) myy) / x) ;
}
