#!/bin/bash
#-x

#export VARS="PAMID_VERBOSE=1:PAMID_ASYNC_PROGRESS=1:PAMID_THREAD_MULTIPLE=1:PAMID_CONTEXT_POST=1:PAMID_CONTEXT_MAX=2"
export VARS=""

export PROJ=PARTS
export ranks=16
export nodes=128
export BENCHMARK=test_aggr
DATE=`date | sed "s/ /_/g" | sed "s/:/_/g"`
export OUTPUT="N${NODES}_R${ranks}_AGGR_${DATE}"

echo "BENCHMARK:"${BENCHMARK}

rm core.*

#qsub -A ${PROJ} -t 10 -n ${nodes} --mode c${ranks} --env ${VARS} ${BENCHMARK} ${PARAMS_BENCH} 
qsub -A ${PROJ} -t 10 -n ${nodes} --mode c${ranks} -O $OUTPUT ${BENCHMARK} ${PARAMS_BENCH} 

