#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "test_datatypes.h"
#include "test_commons.h" 
#include "client_iocontext.h"

// Test setview, write and read N characters (Two blocks). 
// Note: all processes write and read the same data
#define COLLECTIVE_IO 0 
#define INDEPENDENT_IO 1

int coll_indep;
char filename_vb[256];
char filename_tp[256];

void handle_error(int errcode, char *str);
void free_datatype(MPI_Datatype d);

void setview_exec(MPI_Offset off, MPI_Datatype d, MPI_Offset displ, MPI_Info info, char *filename, int n, MPI_Datatype mem_type, int phases) {
  int myrank, err, i, mem_type_size;
  MPI_File fh;
  MPI_Status status;
  MPI_Aint lb, mem_type_extent;
  char *buf1;
  char *buf2;
  MPI_Comm intracomm = cls_get_client_intracomm();

  MPI_Type_get_extent(mem_type, &lb, &mem_type_extent);
  MPI_Type_size(mem_type, &mem_type_size);

  buf1 = malloc(mem_type_extent * n);
  buf2 = malloc(mem_type_extent * n);
  
  for (i = 0; i < mem_type_extent * n; i++){
    buf1[i] = 'a' + i % 16;
    // If gaps in the mem_type initialize the same (o/w bzero, see below)
    if (mem_type_size != mem_type_extent)
      buf2[i] = 'a' + i % 16;
      
  }
  if (mem_type_size == mem_type_extent)
    bzero(buf2, mem_type_extent * n);
  MPI_Comm_rank(intracomm, &myrank);
  if (myrank == 0)
    MPI_File_delete(filename, MPI_INFO_NULL);
  MPI_Barrier(intracomm);
  err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		      info, &fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  
  err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_set_view");
  err = MPI_File_seek(fh, off, MPI_SEEK_SET);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_seek");
  for (i = 0; i < phases; i++) {
    if (coll_indep == INDEPENDENT_IO)
      err = MPI_File_write(fh, buf1, n, mem_type, &status);
    else
      err = MPI_File_write_all(fh, buf1, n, mem_type, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_write_all");
  }
  ////////////////////////
  err = MPI_File_close(&fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");

  MPI_Barrier(intracomm);
  err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
		      info, &fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_open");
  
  err = MPI_File_set_view(fh, displ, MPI_BYTE, d, "native", info);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_set_view");
  ////////////////////////////

  err = MPI_File_seek(fh, off, MPI_SEEK_SET);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_seek");
  for (i = 0; i < phases; i++) {
    if (coll_indep == INDEPENDENT_IO)
      err = MPI_File_read(fh, buf2, n, mem_type, &status);
    else
      err = MPI_File_read_all(fh, buf2, n, mem_type, &status);
    if (err != MPI_SUCCESS)
      handle_error(err,"MPI_File_read_all");
    
    if (strncmp(buf1, buf2, n)) {
      fprintf(stderr, "Phase %d File: %s: \n\tWritten buf=%s\n\tRead buf=%s\n", 
	      i, filename, buf1, buf2);
      handle_error(err,"TEST 7 failed  read a different value than written");
    }

  }
  err = MPI_File_close(&fh);
  if (err != MPI_SUCCESS)
    handle_error(err,"MPI_File_close");
  
  free(buf1);
  free(buf2);
}


extern client_global_state_t client_global_state;

void setview_test(MPI_Offset off, MPI_Datatype d, MPI_Offset displ, int n, MPI_Datatype mem_type, int phases) {
  MPI_Info info;
  
  MPI_Info_create (&info);
   
  //MPI_Info_set(info, "clarisse_collective", "romio");
  client_global_state.clarisse_pars.collective = CLARISSE_ROMIO_COLLECTIVE;
  setview_exec(off, d, displ, info, filename_tp, n, mem_type, phases);
  
  //printf("VB...\n");
  //MPI_Info_set(info, "clarisse_collective", "listio_vb");
  client_global_state.clarisse_pars.collective = CLARISSE_LISTIO_COLLECTIVE;
  setview_exec(off, d, displ, info, filename_vb, n, mem_type, phases);
  MPI_Info_free(&info);

  free_datatype(d);
  free_datatype(mem_type);
}

int main(int argc, char **argv)
{
  int myrank;
  int nprocs;
  MPI_Offset off;
  char *env;

  env = getenv("ENV_FROM_SCRIPT");
  if (!env || strncmp(env, "true", 4)) { 
    //setenv("CLARISSE_BUFPOOL_SIZE", "16", 1 /*overwrite*/);
    setenv("CLARISSE_BUFPOOL_SIZE", "2", 1 /*overwrite*/);
    //setenv("CLARISSE_LAZY_EAGER_VIEW", "eager", 1 /*overwrite*/);
    env = getenv("CLARISSE_COLLECTIVE");
    if (!env || strncmp(env, "vb", 6)) { 
      setenv("CLARISSE_BUFFER_SIZE", "32", 1 /*overwrite*/);
      setenv("CLARISSE_NET_BUF_SIZE", "34", 1 /*overwrite*/);
    }
    else {
      setenv("CLARISSE_BUFFER_SIZE", "4", 1 /*overwrite*/);
      setenv("CLARISSE_NET_BUF_SIZE", "3", 1 /*overwrite*/);     
      //setenv("CLARISSE_BUFFER_SIZE", "128", 1 /*overwrite*/);
      //setenv("CLARISSE_NET_BUF_SIZE", "128", 1 /*overwrite*/);     
    }
  }
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(cls_get_client_intracomm(), &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (argc < 3) {
    if (myrank == 0) {
      fprintf(stderr, "Missing argument, call %s filename collective/independent\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  if (strcmp(argv[2], "collective") && strcmp(argv[2], "independent")){
    if (myrank == 0) {
      fprintf(stderr, "Wrong collective/independent argument, call %s filename collective/independent\n", argv[0]);
    }
    MPI_Finalize();
    exit(1);
  }
  if (!strcmp(argv[2], "collective"))
    coll_indep = COLLECTIVE_IO;
  if (!strcmp(argv[2], "independent"))
    coll_indep = INDEPENDENT_IO;
  sprintf(filename_vb, "%s_vb", argv[1]);
  sprintf(filename_tp, "%s_tp", argv[1]);

  if (cls_is_client()) {
    MPI_Comm intracomm = cls_get_client_intracomm();

    MPI_Comm_rank(intracomm, &myrank);
    MPI_Comm_size(intracomm, &nprocs);
    off = 0;
    //off, file_type, displ, n, mem_type, phases 
    setview_test(off, MPI_FLOAT, 0, 7, MPI_BYTE, 2);
    setview_test(off, MPI_FLOAT, 1, 7, MPI_BYTE, 1);
    setview_test(off, MPI_FLOAT, 10, 7, MPI_BYTE, 1);
    setview_test(off, get_contig(), 0, 7, MPI_BYTE, 1); 
    setview_test(off, get_contig(), 15, 7, MPI_BYTE, 1); 
    setview_test(off, get_lb_contig(), 1, 7, MPI_BYTE, 1); 
    setview_test(off, get_lb_contig(), 10, 7, MPI_BYTE, 1); 
    setview_test(off, get_contig_ub(), 1, 7, MPI_BYTE, 1);
    setview_test(off, get_contig_ub(), 10, 7, MPI_BYTE, 1); 
    setview_test(off, get_simple_vec(), 0, 4, MPI_BYTE, 1); // 1, 7
    setview_test(off, get_displ_simple_vec(), 1, 7, MPI_BYTE, 1);
    setview_test(off, get_displ_simple_vec(), 0, 7, MPI_BYTE, 1);
    setview_test(off, get_simple_struct(), 1, 7, MPI_BYTE, 1);
    setview_test(off, get_displ_displ_struct(), 1, 7, MPI_BYTE, 1);
    setview_test(off, get_vect_struct(), 1, 7, MPI_BYTE, 1);
    
    setview_test(off, get_simple_vec(), 0, 44, MPI_BYTE, 1); 			

    
  // Fails with 5 procs and 5 servers
    setview_test(off, MPI_BYTE, 0, 4, get_simple_vec(), 4);
    
// client_global_state.active_nr_servers = 3
// c_ctxt->clarisse_pars.bufpool_size = 2
// off = 1 : l_f = 1, r_f = 29, b = 4
//                            1             2            3    
//               [0123 4567 8901][2345 6789 0123][4567 8901 2345
//                -abc|defg|hijk |lmno|pabc|defg| hijk|lm--|----
//part_block_len  --------------- 
//domain_len      0000 0000 1111 
//partit 1        ****|****|----| ****|****|----| ****|****
//partit 2        ----|----|****| ----|----|****| ----|----|****|  

// off = 5 : l_f = 1, r_f = 29, b = 4
//                            1             2            3    
//               [0123 4567 8901][2345 6789 0123][4567 8901 2345
//                ----|-abc|defg| hijk|lmno|pabc| defg|hijk|lm--
//part_block_len  --------------- 
//domain_len           0000 0000  1111 
//partit 1             ****|****| ----|****|****|----| ****|****
//partit 2             ----|----| ****|----|----|****| ----|----|****|  



    if (nprocs == 3){
      char *crt_nr_servers;
      crt_nr_servers = getenv("CLARISSE_NR_SERVERS");
      if (crt_nr_servers && (!strcmp(crt_nr_servers,"3"))) {
	//setenv("CLARISSE_NR_SERVERS", "3", 1);
	off = 1;
	//off = 5;
	setview_test(off, MPI_BYTE, 0, 29, MPI_BYTE, 1);
	//if (crt_nr_servers)
	//  setenv("CLARISSE_NR_SERVERS", crt_nr_servers, 1);
      }
    }
  

  /*
  if (nprocs == 1){
    setview_test(get_darray_4procs_1rank(), 0, 16, MPI_BYTE, 1, 1);
    setview_test(get_darray_4procs_1rank(), 1, 16, MPI_BYTE, 1 ,1);
  }
  */
  }
  setenv("CLARISSE_TEST", __FILE__, 1);
  MPI_Finalize();
  return 0; 
}
