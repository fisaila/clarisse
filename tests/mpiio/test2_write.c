#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "clarisse.h"

// Test write 1 character. 
#define SIZE 2

#define COLLECTIVE_IO 0 
#define INDEPENDENT_IO 1

#define TIMES 5
#define CNT 1

int coll_indep;
char filename[256];
char buf[SIZE]="a";

void handle_error(int errcode, char *str);

int main(int argc, char **argv)
{
    int myrank, err;
    MPI_File fh;
    MPI_Status status;
    char *env;

    env = getenv("ENV_FROM_SCRIPT");
    if (!env || strncmp(env, "true", 4)) { 
      setenv("CLARISSE_BUFPOOL_SIZE", "2", 1 /*overwrite*/);
      env = getenv("CLARISSE_COLLECTIVE");
      if (!env || strncmp(env, "vb", 6)) { 
	setenv("CLARISSE_BUFFER_SIZE", "16", 1 /*overwrite*/);
	setenv("CLARISSE_NET_BUF_SIZE","32", 1);
      }
      else {
	setenv("CLARISSE_BUFFER_SIZE", "4", 1 /*overwrite*/);
	setenv("CLARISSE_NET_BUF_SIZE","4", 1);	
      }
      //    setenv("CLARISSE_LAZY_EAGER_VIEW", "eager", 1 /*overwrite*/);
    }
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(cls_get_client_intracomm(), &myrank);
    if (argc < 3) {
      if (myrank == 0) {
	fprintf(stderr, "Missing argument, call %s filename collective/independent\n", argv[0]);
      }
      MPI_Finalize();
      exit(1);
    }
    if (strcmp(argv[2], "collective") && strcmp(argv[2], "independent")){
      if (myrank == 0) {
	fprintf(stderr, "Wrong collective/independent argument argument, call %s filename collective/independent\n", argv[0]);
      }
      MPI_Finalize();
      exit(1);
    }
    if (!strcmp(argv[2], "collective"))
      coll_indep = COLLECTIVE_IO;
    if (!strcmp(argv[2], "independent"))
      coll_indep = INDEPENDENT_IO;
    strcpy(filename, argv[1]);
    if (cls_is_client()){
      MPI_Comm intracomm = cls_get_client_intracomm();
      MPI_Comm_rank(intracomm, &myrank);
      if (myrank == 0)
	MPI_File_delete(filename, MPI_INFO_NULL);
      MPI_Barrier(intracomm);
      err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
			  MPI_INFO_NULL, &fh);
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_open");
      
      int i;
      
      for (i = 0; i < TIMES; i++) {
	if (coll_indep == INDEPENDENT_IO)
	  err = MPI_File_write(fh, buf, CNT, MPI_BYTE, &status);
	else
	  err = MPI_File_write_all(fh, buf, CNT, MPI_BYTE, &status);
	if (err != MPI_SUCCESS)
	  handle_error(err,"MPI_File_write_all");
      }
      
      MPI_Offset size;
      err = MPI_File_get_size(fh, &size);
      if (err != MPI_SUCCESS)
	  handle_error(err,"MPI_File_get_size");

      if (size != TIMES * CNT) {
	fprintf(stderr, "ERROR: incorrect file size, expected=%lld returned=%lld \n", (long long int) TIMES * CNT, (long long int)size);
	MPI_Finalize();
	exit(1);
      }
      err = MPI_File_close(&fh);
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_close");

      
    }
    setenv("CLARISSE_TEST", __FILE__, 1);
    MPI_Finalize();
    return 0; 
}
