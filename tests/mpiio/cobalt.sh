#!/bin/bash 
#-x

export TOTAL_NODES=128 #128 #128
export RANKS_PER_NODE=16 #16 #16 

# full rounds:1 partial rounds with locking: 2 partial rounds no locking: noopen
declare -a tests=(
#"bin/bt.S.4.mpi_io_full"
"../build/main/test8_2_setview_write_read_nproc" 
)
export PROJ=PARTS
export TIME=00:30:00

export ACCESS_SIZE=16777216  #1048576 #4194304 #16777216 #67108864 #1024
export SLEEP_TIME=0
export PHASES=1 #
	#FILENAME="bglockless:/gpfs/vesta-fs0/projects/radix/fisaila/${PROG}_N${NODES}_R${ranks}_VB${vb}"
#FILENAME="/gpfs/vesta-fs0/projects/radix/fisaila/${PROG}_N${NODES}_R${ranks}_${coll"
FILENAME="/gpfs/vesta-fs0/projects/radix/fisaila/test8_2_setview_write_read_nproc"
#FILENAME="/dev/null"		

export NET_BUF_SIZE=16777216 #  65536 #16777216
export BUFFER_SIZE=16777216
export FILE_PARTITIONING="dynamic" #"static" #"dynamic"
export VIEW="lazy" #"eager"
export BUFPOOL_SIZE=1
export MAX_DCACHE_SIZE=1
export WRITE_TYPE=ondemand  # ondemand back onclose
export COLLECTIVE="listio" #"listio vb romio"
export AGGR_THREAD="thread" #"thread process"
export COUPLENESS="intercomm" #coupled intercomm

export BG_NODES_PSET=16 #64 #16  # 4 16(default) 64
export SERVER_MAP=topology_include_bridge  #romio # topology_exclude_bridge    #romio uniform topology_exclude_bridge topology_include_bridge 
export BGQ_AGGREGATOR_SELECTION_STEP=2 #16( 1 2 4 8) 64 (1,2) 
export DEVNULL=disable #enable disable
export GPFS_DEVNULL=0 # 0: disable 1:enable

export HPCTOOL="HPCRUN_EVENT_LIST=WALLCLOCK@5000"
export ENV_VARS="PAMID_VERBOSE=1:CLARISSE_SERVER_MAP=${SERVER_MAP}:CLARISSE_BUFFER_SIZE=${BUFFER_SIZE}:CLARISSE_NET_BUF_SIZE=${NET_BUF_SIZE}:CLARISSE_FILE_PARTITIONING=${FILE_PARTITIONING}:${HPCTOOL}:CLARISSE_LAZY_EAGER_VIEW=${VIEW}:CLARISSE_BUFPOOL_SIZE=${BUFPOOL_SIZE}:CLARISSE_MAX_DCACHE_SIZE=${MAX_DCACHE_SIZE=}:CLARISSE_WRITE_TYPE=${WRITE_TYPE}:CLARISSE_AGGR_THREAD=${AGGR_THREAD}:GPFSMPIO_NAGG_PSET=${BG_NODES_PSET}:CLARISSE_BGQ_BG_NODES_PSET=${BG_NODES_PSET}:CLARISSE_BGQ_AGGREGATOR_SELECTION_STEP=${BGQ_AGGREGATOR_SELECTION_STEP}:CLARISSE_DEVNULL=${DEVNULL}:CLARISSE_COUPLENESS=${COUPLENESS}:GPFSMPIO_DEVNULLIO=${GPFS_DEVNULL}"

rm -f core.* 

#4 16
#256 512
for NODES in  ${TOTAL_NODES}  
do 
    for ranks in ${RANKS_PER_NODE}
    do

	for PROG in "${tests[@]}"
    	do
	    for coll in ${COLLECTIVE} #romio listio vb
	    do
		DATE=`date | sed "s/ /_/g" | sed "s/:/_/g"`
		NPROCS=$((NODES*ranks))
		export VARS="${ENV_VARS}:CLARISSE_COLLECTIVE=${coll}"
		if [ "${coll}" == "romio" ]
    		then 
			OUTPUT="N${NODES}_R${ranks}_${DATE}_${coll}_bnp${BG_NODES_PSET}_P${PHASES}_S${SLEEP_TIME}"
		else
			
			OUTPUT="N${NODES}_R${ranks}_${DATE}_${coll}_bnp${BG_NODES_PSET}_${WRITE_TYPE}${MAX_DCACHE_SIZE}_a${AGGR_THREAD}_FP${FILE_PARTITIONING}_P${PHASES}_S${SLEEP_TIME}"
			if [ "${AGGR_THREAD}" == "thread" ]
			then
				export VARS="PAMID_ASYNC_PROGRESS=1:PAMID_THREAD_MULTIPLE=1:PAMID_CONTEXT_POST=1:PAMID_CONTEXT_MAX=1:${VARS}"	
			fi
		fi
	
		echo "Submitting ${PROG} on ${NODES} nodes"
		echo "VARS=${VARS}"

		active_job=`qstat | grep fisaila`
   		while [[ ! -z "${active_job}" ]]
    		do
        		echo "Active job: sleeping for some time" 
        		sleep 30
        		active_job=`qstat | grep fisaila`
    		done
		qsub -A ${PROJ} -n ${NODES} --mode c${ranks} -t ${TIME} -O $OUTPUT --env ${VARS} ${PROG} ${FILENAME} ${ACCESS_SIZE} ${PHASES} ${SLEEP_TIME}
        	echo "Launched on ${NODES} nodes"
    	    done
	done
    done
done

