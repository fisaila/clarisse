#include "mpi.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "clarisse.h"

// Test write 1 character. 
#define SIZE 2

char filename[256];
char buf[SIZE]="a";

void handle_error(int errcode, char *str);

int main(int argc, char **argv)
{
    int myrank, err;
    MPI_File fh;
    MPI_Status status;
    char *env;

    env = getenv("ENV_FROM_SCRIPT");
    if (!env || strncmp(env, "true", 4)) { 
      setenv("CLARISSE_BUFPOOL_SIZE", "2", 1 /*overwrite*/);
      env = getenv("CLARISSE_COLLECTIVE");
      if (!env || strncmp(env, "vb", 6)) { 
	setenv("CLARISSE_BUFFER_SIZE", "16", 1 /*overwrite*/);
	setenv("CLARISSE_NET_BUF_SIZE","32", 1);
      }
      else {
	setenv("CLARISSE_BUFFER_SIZE", "4", 1 /*overwrite*/);
	setenv("CLARISSE_NET_BUF_SIZE","4", 1);	
      }
      //    setenv("CLARISSE_LAZY_EAGER_VIEW", "eager", 1 /*overwrite*/);
    }
    MPI_Init(&argc,&argv);
    
    if (argc < 2) {
      MPI_Comm_rank(cls_get_client_intracomm(), &myrank);
      if (myrank == 0) {
	fprintf(stderr, "Missing argument, call %s filename\n", argv[0]);
      }
      MPI_Finalize();
      exit(1);
    }
    strcpy(filename, argv[1]);

    if (cls_is_client()){
      MPI_Comm intracomm = cls_get_client_intracomm();
      MPI_Comm_rank(intracomm, &myrank);
      if (myrank == 0)
	MPI_File_delete(filename, MPI_INFO_NULL);
      MPI_Barrier(intracomm);
      err = MPI_File_open(intracomm, filename, MPI_MODE_CREATE | MPI_MODE_RDWR ,
			  MPI_INFO_NULL, &fh);
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_open");
      
      err = MPI_File_write(fh, buf, 1, MPI_BYTE, &status);
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_write_all");
      err = MPI_File_close(&fh);
      if (err != MPI_SUCCESS)
	handle_error(err,"MPI_File_close");
    }
    setenv("CLARISSE_TEST", __FILE__, 1);
    MPI_Finalize();
    return 0; 
}
