#!/bin/bash 
#-x

export TOTAL_NODES=1 #128 #128
export RANKS_PER_NODE=1 #16 #16 
export PROJ=radix-io #PARTS
export TIME=00:10:00

export BENCHMARK=main/test0_empty_mpi
export PARAMS=""

#export LD_LIBRARY_PATH=/bgsys/drivers/toolchain/V1R2M2_base_4.7.2-efix14/gnu-linux-4.7.2-efix014/powerpc64-bgq-linux/lib:/bgsys/drivers/ppcfloor/gnu-linux-4.7.2/powerpc64-bgq-linux/lib/:${HOME}/software/mpich/lib:${HOME}/lib:/opt/ibmcmp/lib64:/lib64:${LD_LIBRARY_PATH}
#export VARS="LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
export VARS=""

echo "BENCHMARK:"${BENCHMARK} ${PARAMS}
#echo "VARS="${VARS}
#echo ${LD_LIBRARY_PATH} 


#qsub -A ${PROJ} -t ${TIME} -n ${TOTAL_NODES}  --mode c${RANKS_PER_NODE} --env ${VARS} ${BENCHMARK} ${PARAMS}
qsub -A ${PROJ} -t ${TIME} -n ${TOTAL_NODES}  --mode c${RANKS_PER_NODE} ${BENCHMARK} ${PARAMS}

