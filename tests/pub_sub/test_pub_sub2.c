#include "util.h"
#ifdef CLARISSE_CONTROL
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <errno.h>
#include "mpi.h"
#include "error.h"
#include "cls_ctrl.h"
#include "clarisse.h"
#include "client_iocontext.h"



void test0(){
  MPI_Comm comms[1];
  clarisse_ctrl_event_props_t eprops;
  clarisse_ctrl_msg_t msg;
  comms[0] = MPI_COMM_WORLD;
  cls_pub_sub_init(1, comms);

  eprops.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
  eprops.handler = NULL;
  cls_subscribe(&eprops);

  msg.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
  cls_publish(&msg, sizeof(int));
  cls_wait_event(CLARISSE_CTRL_MSG_PUBLISH_MEMORY);

  cls_pub_sub_finalize();
}

void * pub_thread(){
  clarisse_ctrl_msg_t msg;
  msg.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
  sleep(1);
  cls_publish(&msg, sizeof(int));
  return NULL;
}

void test1(){
  MPI_Comm comms[1];
  clarisse_ctrl_event_props_t eprops;
  int ret;
  pthread_t tid;
  clarisse_ctrl_event_t *event;

  comms[0] = MPI_COMM_WORLD;
  cls_pub_sub_init(1, comms);

  eprops.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
  eprops.handler = NULL;
  cls_subscribe(&eprops);

  if ((ret = pthread_create(&tid, NULL, pub_thread, NULL)) != 0) {	
    fprintf(stderr,"pthread_create %d",ret); 
    exit(-1); 
  }
  
  event = cls_wait_event(CLARISSE_CTRL_MSG_PUBLISH_MEMORY);
  clarisse_free(event);
  ret = pthread_join(tid, NULL);
  if (ret) { printf("ERROR; return code from pthread_join() is %d\n", ret); exit(-1); }
  cls_pub_sub_finalize();
}




int handler_subscribe_mem(void *eprops, clarisse_ctrl_event_t *event) {
  int err;
  clarisse_ctrl_event_props_t *ep;
  clarisse_ctrl_msg_memory_t *msg_mem;
  static int mem = 0;

  msg_mem = (clarisse_ctrl_msg_memory_t *)&event->msg;
  msg_mem->type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
  msg_mem->memory = mem++;
  event->msg_size = sizeof(clarisse_ctrl_msg_memory_t);
  ep = (clarisse_ctrl_event_props_t *) eprops;
  printf("Sending MEM\n");
  err = MPI_Send(&event->msg, event->msg_size, MPI_BYTE, ep->dest.rank, CLARISSE_CTRL_TAG, ep->dest.comm);
  if (err != MPI_SUCCESS)
    handle_err(err, "MPI_Send error");
  return 0;
}

void test2(){
  MPI_Comm comms[1];
  clarisse_ctrl_event_props_t eprops;
  clarisse_ctrl_msg_t msg;
  int i;

  comms[0] = MPI_COMM_WORLD;
  cls_pub_sub_init(1, comms);
  
  eprops.type = CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY;
  eprops.priority = CLARISSE_EVENT_PRIORITY_LOW;
  eprops.scope = CLARISSE_EVENT_SCOPE_PROCESS;
  MPI_Comm_rank(MPI_COMM_WORLD, &eprops.dest.rank);
  eprops.dest.comm = MPI_COMM_WORLD;
  eprops.dest_type = CLARISSE_EVENT_USE_DESTINATION;
  eprops.handler = handler_subscribe_mem;

  cls_subscribe(&eprops);
  eprops.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
  eprops.handler = NULL;
  cls_subscribe(&eprops);

  msg.type = CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY;
  for (i = 0; i <= 1; i++) {
    cls_publish(&msg, sizeof(int));
    sleep(1);
  }
  for (i = 0; i <= 1; i++) {
    clarisse_ctrl_event_t *event;
    event = cls_wait_event(CLARISSE_CTRL_MSG_PUBLISH_MEMORY);
    clarisse_free(event);
  }
  cls_pub_sub_finalize();
}


void test3(){
  MPI_Comm comms[1];
  clarisse_ctrl_event_props_t eprops;
  clarisse_ctrl_msg_t msg;
  int i, myrank;

  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

  comms[0] = MPI_COMM_WORLD;
  cls_pub_sub_init(1, comms);
  
  if (myrank == 0) {
    eprops.type = CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY;
    eprops.priority = CLARISSE_EVENT_PRIORITY_LOW;
    eprops.scope = CLARISSE_EVENT_SCOPE_PROCESS;
    eprops.dest.rank = 1;
    eprops.dest.comm = MPI_COMM_WORLD;
    eprops.dest_type = CLARISSE_EVENT_USE_DESTINATION;
    eprops.handler = handler_subscribe_mem;

    cls_subscribe(&eprops);
  }
  if (myrank == 1) {
    eprops.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
    eprops.handler = NULL;
    cls_subscribe(&eprops);
  }

  if (myrank == 0) {
    msg.type = CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY;
    for (i = 0; i <= 1; i++) {
      cls_publish(&msg, sizeof(int));
      sleep(1);
    }
  }

  if (myrank == 1)
    for (i = 0; i <= 1; i++) {
      clarisse_ctrl_event_t *event;
      event = cls_wait_event(CLARISSE_CTRL_MSG_PUBLISH_MEMORY);
      clarisse_free(event);
    }
  MPI_Barrier(MPI_COMM_WORLD);
  cls_pub_sub_finalize();
}


int handler_reply_back(void *eprops, clarisse_ctrl_event_t *event) {
  int err;
  printf("%s: Replying msg type = %d\n", __FUNCTION__, event->msg.type);
  err = MPI_Send(&event->msg, event->msg_size, MPI_BYTE, event->source.rank, CLARISSE_CTRL_TAG, event->source.comm);
  if (err != MPI_SUCCESS)
    handle_err(err, "MPI_Send error");

  CLARISSE_UNUSED(eprops);
  return 0;
}

void test4(){
  MPI_Comm comms[1];
  clarisse_ctrl_event_props_t eprops;
  clarisse_ctrl_msg_t msg;
  int i, myrank;

  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

  comms[0] = MPI_COMM_WORLD;
  cls_pub_sub_init(1, comms);
  
  if (myrank == 0) {
    eprops.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
    eprops.priority = CLARISSE_EVENT_PRIORITY_LOW;
    eprops.scope = CLARISSE_EVENT_SCOPE_PROCESS;
    eprops.dest_type = CLARISSE_EVENT_REPLY_TO_SENDER;
    eprops.handler = handler_reply_back;
    cls_subscribe(&eprops);  
  }
  
  if (myrank == 1) {  
    eprops.type = CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY;
    eprops.priority = CLARISSE_EVENT_PRIORITY_LOW;
    eprops.scope = CLARISSE_EVENT_SCOPE_PROCESS;
    eprops.dest.rank = 0;
    eprops.dest.comm = MPI_COMM_WORLD;
    eprops.dest_type = CLARISSE_EVENT_USE_DESTINATION;
    eprops.handler = handler_subscribe_mem;
    cls_subscribe(&eprops);
    
    eprops.type = CLARISSE_CTRL_MSG_PUBLISH_MEMORY;
    eprops.handler = NULL;
    cls_subscribe(&eprops);

    msg.type = CLARISSE_CTRL_MSG_SUBSCRIBE_MEMORY;
    for (i = 0; i <= 1; i++) {
      cls_publish(&msg, sizeof(int));
      sleep(1);
    }
    for (i = 0; i <= 1; i++) {
      clarisse_ctrl_event_t *event;
      event = cls_wait_event(CLARISSE_CTRL_MSG_PUBLISH_MEMORY);
      clarisse_free(event);
    }
  }
  sleep(2);
  MPI_Barrier(MPI_COMM_WORLD);
  cls_pub_sub_finalize();
}
#endif


int main(int argc, char **argv){
#ifdef CLARISSE_CONTROL  
  int provided;
  PMPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  test4();
  PMPI_Finalize();
#else
  CLARISSE_UNUSED(argc);
  CLARISSE_UNUSED(argv);
#endif
  return 0;
}


