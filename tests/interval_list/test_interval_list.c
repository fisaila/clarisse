#include "interval_list.h"
#include <stdio.h>

static void print_test_results(dllist *il, long long int l, long long int r){
  printf("__________________________\n");
  if (interval_list_covers(il, l, r))
    printf("Test passed: (%lld, %lld) covers ", l, r);
  else {
    printf("Test failed: (%lld, %lld) should have covered ", l, r);
    interval_list_print(il);
    exit(1);
  }
  interval_list_print(il);
  printf("__________________________\n");
}

static void interval_list_insert_print(dllist *il, long long int l, long long int r){
  interval_list_insert(il, l, r);
  interval_list_print(il);
}

void test1(){
  dllist il = DLLIST_INITIALIZER;
  interval_list_insert_print(&il, 0, 0);
  print_test_results(&il, 0, 0);
  interval_list_free(&il);
}

void test2(){
  dllist il = DLLIST_INITIALIZER;
  interval_list_insert_print(&il, 0, 0);
  interval_list_insert_print(&il, 1, 1);
  print_test_results(&il, 0, 0);
  interval_list_free(&il);
}

void test3(){
  dllist il = DLLIST_INITIALIZER;
  interval_list_insert_print(&il, 0, 0);
  interval_list_insert_print(&il, 2, 2);
  interval_list_insert_print(&il, 1, 1);
  print_test_results(&il, 0, 0);
  interval_list_free(&il);
}

void test4(){
  dllist il = DLLIST_INITIALIZER;
  interval_list_insert_print(&il, 0, 0);
  interval_list_insert_print(&il, 2, 2);
  interval_list_insert_print(&il, 4, 4);
  interval_list_insert_print(&il, 1, 3);
  print_test_results(&il, 0, 0);
  interval_list_free(&il);
}

void test5(){
  dllist il = DLLIST_INITIALIZER;
  interval_list_insert_print(&il, 800, 879);
  interval_list_insert_print(&il, 1584, 1911);
  interval_list_insert_print(&il, 136, 679);
  interval_list_insert_print(&il, 680, 799);
  interval_list_insert_print(&il, 880, 919);
  interval_list_insert_print(&il, 526472, 526799);
  interval_list_insert_print(&il, 920, 1463);
  interval_list_insert_print(&il, 1464, 1583);
  interval_list_insert_print(&il, 1912, 2183);
  interval_list_insert_print(&il, 96, 135);
  interval_list_insert_print(&il, 0, 95);
  
  print_test_results(&il, 0, 0);
  interval_list_free(&il);
}

//vpicio
void test6(){
  dllist il = DLLIST_INITIALIZER;
  interval_list_insert_print(&il, 2184, 4194303);
  interval_list_insert_print(&il, 800, 879);
  interval_list_insert_print(&il, 1584, 1911);
  interval_list_insert_print(&il, 136, 679);
  interval_list_print(&il);
  interval_list_insert_print(&il, 680, 799);
  
  interval_list_insert_print(&il, 880, 919);
  
  interval_list_insert_print(&il, 920, 1463);
  
  interval_list_insert_print(&il, 1464, 1583);
  
  interval_list_insert_print(&il, 1912, 2183);
  
  interval_list_insert_print(&il, 96, 135);
  
  interval_list_insert_print(&il, 0, 95);
  

  print_test_results(&il, 1912, 2423);
  interval_list_free(&il);
}

//vorpalio
void test7(){
  dllist il = DLLIST_INITIALIZER;

  interval_list_insert_print(&il, 4168, 18567);
  interval_list_insert_print(&il,21936, 36335 );
  interval_list_insert_print(&il,800,839);
  interval_list_insert_print(&il,2536, 2863);
  interval_list_insert_print(&il,840, 1383);
  interval_list_insert_print(&il,1384, 1503);
  interval_list_insert_print(&il,1832, 1871);
  interval_list_insert_print(&il,3568, 3895);
  interval_list_insert_print(&il,1872, 2415);
  interval_list_insert_print(&il,2416, 2535);
  interval_list_insert_print(&il,2864, 2903);
  interval_list_insert_print(&il,18568, 18895);
  interval_list_insert_print(&il,2904, 3447);
  interval_list_insert_print(&il,3448, 3567);
  interval_list_insert_print(&il, 3896, 4167 );
  interval_list_insert_print(&il, 96, 135);
  interval_list_insert_print(&il, 1504, 1831);
  interval_list_insert_print(&il, 136, 679);
  interval_list_insert_print(&il, 680, 799);
  interval_list_insert_print(&il, 18896, 18935);
  interval_list_insert_print(&il, 20304, 20631);
  interval_list_insert_print(&il, 18936, 19479);
  interval_list_insert_print(&il, 19480, 19599);
  interval_list_insert_print(&il, 19600, 19639);
  interval_list_insert_print(&il, 21336, 21663);
  interval_list_insert_print(&il, 19640, 20183);
  interval_list_insert_print(&il, 20184, 20303);
  interval_list_insert_print(&il, 20632, 20671);
  interval_list_insert_print(&il, 36336, 36663);
  interval_list_insert_print(&il, 20672, 21215);
  interval_list_insert_print(&il, 21216, 21335);
  interval_list_insert_print(&il, 21664, 21935);
  interval_list_insert_print(&il, 0, 95);

  print_test_results(&il, 3896, 4407);
  interval_list_free(&il);
}

int main(){

  test1();
  test2();
  test3();
  test4();
  test5();
  test6();
  test7();
  return 0;
}
