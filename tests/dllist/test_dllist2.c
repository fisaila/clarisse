#include <string.h>
#include <stdio.h>
#include "list.h"


struct el{ 
  struct dllist_link link0;
  struct dllist_link link1;
  int i;
};

void print_list0(struct dllist *dll, char *buf)
{
  struct dllist_link *aux;
  printf("%s:\t", buf);
  for(aux = dll->head; aux != NULL; aux = aux->next) {
    printf("%d\t", (DLLIST_ELEMENT(aux, struct el, link0))->i);
  }
  printf("\n");
}

void print_list1(struct dllist *dll, char *buf)
{
  struct dllist_link *aux;
  printf("%s:\t", buf);
  for(aux = dll->head; aux != NULL; aux = aux->next) {
    printf("%d\t", (DLLIST_ELEMENT(aux, struct el, link1))->i);
  }
  printf("\n");
}



struct el * remove_int_elem(struct dllist *dll, int i) {
  struct dllist_link * aux;
  for(aux = dll->head; aux != NULL; aux = aux->next) {
    struct el *e = DLLIST_ELEMENT(aux, struct el, link1);
    if (e->i == i) {
      dllist_rem(dll, aux);
      return e;
    }
  }
  return NULL;
}


    

int main()
{
  char c;
  int i=0;
  struct dllist dll[2];

  dllist_init(&dll[0]);
  dllist_init(&dll[1]);

  printf("e=enqueue d=dequeue i=insert a= append l to l p=print ?=is empty\n");
  scanf("%c",&c);
  printf("c=%c\n",c);
  while(c!='n')
    {
      switch(c)
	{
	case 'e': {
	  struct el *e=(struct el *) malloc(sizeof(struct el));
	  int l;
	  printf("Enqueue in list 0 (list 0) 1 (list 1) 2 (for both 0 and 1)="); fflush(stdout);
	  scanf("%d",&l);
	  
	  e->i=i++;
	  if ((l==0) || (l==2))
	    dllist_iat(&dll[0],&e->link0);
	  if ((l==1) || (l==2))
	    dllist_iat(&dll[1],&e->link1);

	  print_list0(&dll[0], "List 0");
	  print_list1(&dll[1], "List 1");
	  break;
	}
	case 'i': {
	  struct el *e=(struct el *) malloc(sizeof(struct el));
	  int l;
	  printf("Insert in front into list 0 (list 0) 1 (list 1) 2 (for both 0 and 1) ="); fflush(stdout);
	  scanf("%d",&l);	  
	  e->i=i++;
	  if ((l==0) || (l==2))
	    dllist_iah(&dll[0], &e->link0);
	  if ((l==1) || (l==2))
	    dllist_iah(&dll[1], &e->link1);

	  print_list0(&dll[0], "List 0");
	  print_list1(&dll[1], "List 1");
	  break;
	}
	case 'p': 
	  print_list0(&dll[0], "List 0");
	  print_list1(&dll[1], "List 1");
	  break;
	case 'd': {
	  int l;
	  printf("Dequeue from list="); fflush(stdout);
	  scanf("%d",&l);	  
	  dllist_rem_tail(&dll[l]);
	  print_list0(&dll[0], "List 0");
	  print_list1(&dll[1], "List 1");
	  break;
	}
	case 'a':
	  /*
	  init_list(&l2);

	  i_p=(struct i*)malloc(sizeof(struct i));
	  i_p->i=10;
	  add_in_front(i_p,&l2);
	  
	  i_p=(struct i*)malloc(sizeof(struct i));
	  i_p->i=11;
	  add_in_front(i_p,&l2);

	  l_p=list_append(&l,&l2);
	  printf(dump_list(l_p));
	  */
	  printf("Append not implemented\n");
	  break;
	  
	case 'l':
	  printf("length not implemented\n");
	  break;
	  
	case 'r': {
	  int l;
	  printf("Remove from list="); fflush(stdout);
	  scanf("%d",&l);	  
	  printf("Remove element with i="); fflush(stdout);
	  scanf("%d",&i);
	  printf("i=%d\n",i); fflush(stdout);

	  remove_int_elem(&dll[l], i);
	  print_list0(&dll[0], "List 0");
	  print_list1(&dll[1], "List 1");
	  break; 
	}
	case '?': {
	  int l;
	  printf("Empty check for list="); fflush(stdout);
	  scanf("%d",&l);	  	  
	  if(dllist_is_empty(&dll[l]))
	    printf("Empty\n");
	  else
	    printf("Not Empty\n");
	}
	}
      printf("e=enqueue d=dequeue i=insert p=print r=remove ?=is empty\n");
      scanf("\n%c",&c);
      printf("c=%c\n",c);
    }
  return 0;
}
